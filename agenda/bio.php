<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

require("../classes/database.class.php");
$database = new database();


$id = $_GET['id'];
$nome = $_GET['nome'];
// select apresentacoes.id, group_concat(apresentacoes_oradores.nome separator ', ') from apresentacoes, apresentacoes_oradores where apresentacoes.id = apresentacoes_oradores.id_apresentacao group by 'all'

// $strting_sql = "SELECT ".$database->array_tables[4].".id,".$database->array_tables[4].".flag_passado, ".$database->array_tables[4].".resumo,".$database->array_tables[4].".flag_activo,".$database->array_tables[4].".titulo, group_concat(".$database->array_tables[6].".nome separator ', ') as oradores, ".$database->array_tables[5].".titulo as titulo_grupo, ".$database->array_tables[5].".ordem as ordem_grupo FROM ".$database->array_tables[4].", ".$database->array_tables[6].", ".$database->array_tables[5]." WHERE ".$database->array_tables[4].".id = ".$database->array_tables[6].".id_apresentacao AND ".$database->array_tables[4].".id_grupo = ".$database->array_tables[5].".id AND ".$database->array_tables[4].".id = ? GROUP BY ".$database->array_tables[4].".id ORDER BY ".$database->array_tables[5].".ordem ASC, ".$database->array_tables[4].".ordem ASC";

$strting_sql = "SELECT ".$database->array_tables[6].".bio FROM ".$database->array_tables[6]." WHERE  ".$database->array_tables[6].".id = ? AND  ".$database->array_tables[6].".nome = ? ";

$res_agenda = $database->query_simple_prepare($strting_sql,array($id,$nome),"is");


?>
<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/agenda.css">
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <p class="titulo_agenda color_red">
        <?php 
        
        if (strpos($nome, 'Moderado por') !== false) {
            echo str_replace("Moderado por","",$nome);
        }
        else{
            echo $nome;
        }
        ?>
        </p>
        <div style="clear:left;"></div>
        <p class="resumo_titulo">BIO</p>
        <div style="clear:left;"></div>
        <p class="resumo_text"><?php echo nl2br($res_agenda[0]['bio'])?></p>
        <div style="clear:left;"></div>


		



        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="../js/plugins.js"></script>
        <script src="../js/main_new.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-12913211-65', 'auto');
        ga('send', 'pageview');

        </script>
    </body>
</html>