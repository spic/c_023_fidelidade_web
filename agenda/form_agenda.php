<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

require("../classes/database.class.php");
$database = new database();

$email = $_GET['email'];
$id = $_GET['id'];
$nome = $_GET['nome'];

?>
<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/agenda.css">
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

       <form action="">
            <div class="form_agenda_input">
                <div>
                    ASSUNTO
                </div>
                <div>
                    <input type="text" placeholder="assunto" value="<?php echo $nome; ?>">
                </div>
                
            </div>
            <div class="form_agenda_textarea">
                <div>
                    DÚVIDAS
                </div>
                <textarea name="" id="">
                </textarea>
            </div>

            <div class="form_agenda_button_holder">
                <div class='button saber_mais' onclick="javascript: window.history.back();">VOLTAR</div>
                <div class='button saber_mais'>SUBMETER</div>
            </div>
           
       </form>

		



        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="../js/plugins.js"></script>
        <script src="../js/main_new.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-12913211-65', 'auto');
        ga('send', 'pageview');

        </script>
    </body>
</html>