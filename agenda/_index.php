<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

require("../classes/database.class.php");
$database = new database();

$email = $_GET['email'];
// select apresentacoes.id, group_concat(apresentacoes_oradores.nome separator ', ') from apresentacoes, apresentacoes_oradores where apresentacoes.id = apresentacoes_oradores.id_apresentacao group by 'all'

$strting_sql = "SELECT ".$database->array_tables[4].".id,".$database->array_tables[4].".flag_passado,".$database->array_tables[4].".flag_activo,".$database->array_tables[4].".titulo, group_concat(".$database->array_tables[6].".nome ORDER BY ".$database->array_tables[6].".ordem ASC separator ', ') as oradores, ".$database->array_tables[5].".titulo as titulo_grupo, ".$database->array_tables[5].".ordem as ordem_grupo FROM ".$database->array_tables[4].", ".$database->array_tables[6].", ".$database->array_tables[5]." WHERE ".$database->array_tables[4].".id = ".$database->array_tables[6].".id_apresentacao AND ".$database->array_tables[4].".id_grupo = ".$database->array_tables[5].".id GROUP BY ".$database->array_tables[4].".id ORDER BY ".$database->array_tables[5].".ordem ASC, ".$database->array_tables[4].".ordem ASC";

$res_agenda = $database->query_simple_prepare($strting_sql,array(),"");




?>
<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        <link rel="stylesheet" href="../css/agenda.css">
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <p class="titulo_agenda">REUNIÃO<br />DE TRABALHO</p>

		<?php
            $i = -1;
            $old_grupo = "";
            foreach($res_agenda as $key => $value){
                
                if($old_grupo != $value['titulo_grupo']){
                    $old_grupo = $value['titulo_grupo'];
                    $titulo_grupo = $value['titulo_grupo'];
                    $i++;
                }
                else{
                    $titulo_grupo = "";
                }

                if($i % 2 == 0) $string_back_class = "cinza_back";
                else $string_back_class = "";

                if($value['flag_activo'] == 1) $string_activo_class = "red_back";
                else{ $string_activo_class = "";}

                if($value['flag_passado'] == 1){ $string_passado_class = "cinza_text"; $string_terminado = "(TERMINADO)";}
                else{ $string_passado_class = "";$string_terminado = "";}

                echo "<div class='apres_holder ".$string_back_class." ".$string_activo_class." ".$string_passado_class."'>";
                echo "<p class='titulo_grupo'>".$titulo_grupo."</p>";

                if($value['flag_activo'] == 1) {
                    echo "<div style='float:left;position:relative;'>
                        <div style='float:left;position:relative;margin-right:10px'>
                            <img src='img/icon_activo.png' width='25' />
                        </div>
                        <div style='float:left;position:relative'>";
                        if($value['titulo']){
                            echo "<p class='titulo_apres'>".$value['titulo'].", ".$string_terminado."</p>";
                        }
                            
                            echo "<p class='oradores'>".$value['oradores']."</p>
                        </div>
                    </div>";
                }
                else{
                    if($value['titulo']){
                        echo "<p class='titulo_apres'>".$value['titulo'].", ".$string_terminado."</p>";
                    }
                    echo "<p class='oradores'>".$value['oradores']."</p>";
                }
                echo "<div style='clear:left;'></div>";
                echo "<a href='ficha.php?email=".$email."&id=".$value['id']."'><div class='button saber_mais'>SABER MAIS</div></a>";
                echo "</div>";

                
            }
        
        ?>
		
        
		



        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="../js/plugins.js"></script>
        <script src="../js/main_new.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-12913211-65', 'auto');
        ga('send', 'pageview');

        </script>
    </body>
</html>