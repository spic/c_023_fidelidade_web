<?php
	session_start();
	error_reporting(E_ALL|E_STRICT);
	error_reporting(E_ALL^E_NOTICE);
	ini_set("display_errors","off");
	include "base.php";
	$base = new base();

	
//	echo md5("madmia"."aquinaoentrasze");

	
	$loginErradoVar = "";
	
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		$user = $_POST['user'];
		$pass = $_POST['password'];	
		$password = md5($user.$pass);
//		echo "<br />".$password;
		$sql = "SELECT lastlogin,id FROM users_cms WHERE user='$user' AND pass='$password'";
//		echo "<br />".$sql;
		$res = $base->query_simple($sql);
//		echo "<br />".mysql_num_rows($res);
		if(mysql_num_rows($res) == 0)
		{
//			$msgErroLogin = "
//				<div class='alert-box alert'>
//				 Username ou Password errados!
//				  <a href='' class='close'>&times;</a>
//				</div>
//
//				";
				$loginErradoVar = '<div class="col-lg-12">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Login</strong> errado!
			</div>	
		</div>
		<div style="clear:left">&nbsp;</div>';
		}
		else
		{
			require_once("valida.php");
            $_SESSION['sessao'] = 1;
			$_SESSION['userId'] = mysql_result($res,0,'id');
			$_SESSION['lastlogin'] = mysql_result($res,0,'lastlogin');
            $sql = "UPDATE users_cms SET lastlogin = '".date("Y-m-d H:i")."' WHERE id=".$_SESSION['userId'];
			$res = $base->query_simple($sql);
			
            header("Location:home.php");
		}
	}
//	echo md5("admin"."admin");
?>
<!DOCTYPE html>
<html lang="en" ng-app="cmsLoginApp" ng-cloak>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS</title> 

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="background-color:#222222" >

    <div ng-controller="loginController" class="col-md-4 col-md-offset-4" style="background:#FFFFFF;padding: 20px 20px 20px 20px;border-radius:5px;">

      <form class="form-signin" role="form" method="post" action=""  novalidate>
        <h2 class="form-signin-heading">CMS</h2>
        <div style='clear:left'>&nbsp;</div>
        <?php echo $loginErradoVar?>
        <div class="input-group col-md-12">
		  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		  <input type="text" class="form-control" id="user" name="user" required placeholder="Username">
		</div>
		<div style='clear:left'>&nbsp;</div>
		<div class="input-group col-md-12">
		  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		  <input type="password" id="password" name="password" class="form-control" required placeholder="Password">
		</div>	
		<div style='clear:left'>&nbsp;</div>
		<div style='clear:left'>&nbsp;</div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
      </form>

    </div>
	
	<div ng-controller="registoController" class="col-md-8 col-md-offset-2" style="background:#FFFFFF;padding: 20px 20px 20px 20px;border-radius:5px;margin-top:20px;">

      <form class="form-signin" role="form" method="post" action="" id="registoForm" name="registoForm"  novalidate>
        <h2 class="form-signin-heading">Registo de Utilizador</h2>
        <div style='clear:left'>&nbsp;</div>
			
			<div class="form-group col-md-6">
				<label for="nomeInput">Nome</label>
				<span class="label label-danger" data-ng-show="submitted && registoForm.nomeInput.$error.required">Obrigatório!</span>
				<input type="text" class="form-control" id="nomeInput" required ng-model="new_user.nome" name="nomeInput" placeholder="Nome">
			</div>
			
			<div class="form-group col-md-6">
				<label for="apelidoInput">Apelido</label>
				<span class="label label-danger" data-ng-show="submitted && registoForm.apelidoInput.$error.required">Obrigatório!</span>
				<input type="text" class="form-control" id="apelidoInput" required ng-model="new_user.apelido" name="apelidoInput" placeholder="Apelido">
			</div>

			<div class="form-group col-md-6">
				<label for="emailInput">Email</label>
				<span class="label label-danger" data-ng-show="submitted && registoForm.emailInput.$error.required">Obrigatório!</span>
				<span class="label label-danger" data-ng-show="submitted && registoForm.emailInput.$error.emailregisto">{{emailError}}</span>
				<input type="text" class="form-control" id="emailInput" required ng-model="new_user.email" emailregisto name="emailInput" placeholder="Email">
			</div>

			<div class="form-group col-md-6">
				<label for="userInpput">Username</label>
				<span class="label label-danger" data-ng-show="submitted && registoForm.userInpput.$error.required">Obrigatório!</span>
				<span class="label label-danger" data-ng-show="submitted && registoForm.userInpput.$error.username">{{userError}}</span>
				<input type="text" class="form-control" id="userInpput" required ng-model="new_user.username" username name="userInpput" placeholder="Username">
			</div>
	
			<div style='clear:left'>&nbsp;</div>
			<div style='clear:left'>&nbsp;</div>
        	<button class="btn btn-lg btn-primary btn-block" ng-click="register(registoForm)" type="submit">Registar</button>
      </form>

    </div>


     <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="js/angular/angular.min.js"></script>
    <script src="js/global_services.js"></script>
    <script src="js/global_directives.js"></script>
    <script src="js/appLogin.js"></script>


  </body>
</html>
