<?php
	session_start();
	error_reporting(E_ALL|E_STRICT);
	error_reporting(E_ALL^E_NOTICE);
	ini_set("display_errors","off");
	include "base.php";
	$base = new base();

	
	$loginErradoVar = "";
	
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		$user = $_POST['user'];
		$pass = $_POST['password'];	
		$loginFrase = '';

		$res = $base->query_totalrows("SELECT id FROM users_cms WHERE user='".$user."' AND activo=1");	
		if ($res == 0) {
			$loginFrase = "Username descohecido!";
			$loginErradoVar = '<div class="col-lg-12" style="padding:0px;">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>'.$loginFrase.'</strong>
				</div>	
			</div>
		<div style="clear:left">&nbsp;</div>';
		}
		else{
			$res = $base->query_simple("SELECT last_login,email,password,id,nome FROM users_cms WHERE user='".$user."' AND activo=1");	
			$array_rows = $res->fetch_assoc();	
			$last_login = $array_rows['last_login'];
			$id_user = $array_rows['id'];
			$nome_user = $array_rows['nome'];
			$email_user = $array_rows['email'];

			$pass_code = md5($user.$array_rows['email'].$pass);

			$res_login = $base->query_totalrows("SELECT id FROM users_cms WHERE user='".$user."' AND email='".$array_rows['email']."' AND password='".$pass_code."' AND activo=1");

			if ($res_login == 0) {
				$loginFrase = "Username ou password errados!";
				$loginErradoVar = '<div class="col-lg-12" style="padding:0px;">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>'.$loginFrase.'</strong>
					</div>	
				</div>
				<div style="clear:left">&nbsp;</div>';
			}	
			else{
				$loginFrase = $base->flag_success;
				require_once("content/valida.php");
            $_SESSION['sessao'] = 1;
				$_SESSION['userId'] = $id_user;
				$_SESSION['lastlogin'] = $last_login;
				$_SESSION['nome'] = $nome_user;
				$_SESSION['email_user'] = $email_user;
				$_SESSION['username'] = $user;
				$res_last_login = $base->query_totalrows("UPDATE users_cms SET last_login = '".date("Y-m-d ").(date('H')-1).date(":i:s")."' WHERE id=".$_SESSION['userId']);
            header("Location: content/");
			}

			
		}
		// $password = md5($user.$pass);
//		echo "<br />".$password;
		// $sql = "SELECT lastlogin,id FROM users WHERE user='$user' AND pass='$password'";
//		echo "<br />".$sql;
		// $res = $base->query_simple($sql);
//		echo "<br />".mysql_num_rows($res);
		// if(mysql_num_rows($res) == 0)
		// {

		
		// }
		// else
		// {
		// 	
		// }
	}
//	echo md5("admin"."admin");
?>
<!DOCTYPE html>
<html lang="en" ng-app="cmsLoginApp" ng-cloak>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS</title> 

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="background-color:#d2d6de" >

    <div ng-controller="loginController"  class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1" style="background:#FFFFFF;padding: 20px 20px 20px 20px;border-radius:5px;">

      <form class="form-signin" ng-show="recuperar_flag == 0" role="form" method="post" action="" id="loginForm" name="loginForm" novalidate>
      	<h2 class="form-signin-heading">CMS</h2>
        	<div style='clear:left'>&nbsp;</div>
        	<div class="col-lg-12" ng-show="msg_login" style="padding:0px;">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>{{msg_login}}</strong>
				</div>	
			</div>
			<div style="clear:left">&nbsp;</div>
			<?php echo $loginErradoVar;?>
			<label for="titulo_input">Username</label>
        <span class="label label-danger" data-ng-show="submitted && loginForm.user.$error.required">Obrigatório!</span>
        <div class="input-group col-md-12">
			  	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			  	<input type="text" class="form-control" id="user" name="user" ng-model="login_user.username" required placeholder="Username">
			</div>
			<div style='clear:left'>&nbsp;</div>
			<label for="titulo_input">Password</label>
			<span class="label label-danger" data-ng-show="submitted && loginForm.password.$error.required">Obrigatório!</span>
			<div class="input-group col-md-12">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			  <input type="password" id="password" name="password" class="form-control" ng-model="login_user.password" required placeholder="Password">
			</div>	
			<div style='clear:left'>&nbsp;</div>
			<div style='clear:left'>&nbsp;</div>
        	<button class="btn btn-lg btn-primary btn-block" ng-click="login(loginForm)" type="submit">Login</button>
      </form>

      <p style="margin-top:20px;margin-bottom:0px;cursor:pointer;padding:0px" ng-show="recuperar_flag == 0" ng-click="recuperar_flag=1" class="btn btn-link">Recuperar Password</p>

      <form class="form-signin" ng-show="recuperar_flag == 1" role="form" method="post" action="" id="recuperarForm" name="recuperarForm" novalidate>
      	<h2 class="form-signin-heading">Recuperar Password</h2>
        	<div style='clear:left'>&nbsp;</div>
        	<div class="col-lg-12" ng-show="msg_recuperar" style="padding:0px;">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>{{msg_recuperar}}</strong>
				</div>	
			</div>
			<div style="clear:left">&nbsp;</div>
			<label for="titulo_input">Email</label>
        <span class="label label-danger" data-ng-show="submitted_recuperar && recuperarForm.email_input.$error.required">Obrigatório!</span>
        <span class="label label-danger" data-ng-show="submitted_recuperar && recuperarForm.email_input.$error.email">email Inválido!</span>
        <div class="input-group col-md-12">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span>
			  <input type="text" class="form-control" id="email_input" name="email_input" email ng-model="email_recuperar" required placeholder="Email">
			</div>
			
			<div style='clear:left'>&nbsp;</div>
        	<button class="btn btn-lg btn-primary btn-block" ng-click="recuperar(recuperarForm)" type="submit">Recuperar Password</button>
      </form>

      <p style="margin-top:20px;margin-bottom:0px;cursor:pointer;padding:0px" ng-show="recuperar_flag == 1" ng-click="recuperar_flag=0" class="btn btn-link">Voltar ao login</p>

    </div>
	
	


     <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

	<script src="js/vendor/jquery-1.11.1.min.js"></script>
   <script src="js/angular/angular.min.js"></script>
   <script src="js/global_services.js"></script>
   <script src="js/global_directives.js"></script>
   <script src="js/appLogin.js"></script>


  </body>
</html>
