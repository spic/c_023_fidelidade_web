<?php
session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 600)) {
   // last request was more than 30 minates ago
   session_destroy();   // destroy session data in storage
   session_unset();     // unset $_SESSION variable for the runtime
	echo "out";
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
// echo "out";
?>