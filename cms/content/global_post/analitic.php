<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");

$array_dias = array();
$array_visitas_dias = array();
$array_paises = array();
$array_visitas_paises = array();
$array_browser = array();
$array_visitas_browser = array();
$array_paginas = array();
$array_visitas_paginas = array();
$array_source = array();
$array_visitas_source = array();
$array_cities = array();
$array_visitas_cities = array();

$array_source_global = array();
$array_visitas_source_global = array();

$array_tipo_dispositivo = array();
$array_visitas_tipo_dispositivo = array();


require 'gapi.class.php';
$ga = new gapi('spicsolutions.com@gmail.com','spicspic123');
$id_property = '98529331';

// id do loule criativo: 98529331
// $ga->requestReportData('98529331',array('browser','browserVersion'),array('pageviews','visits'));
// $ga->requestReportData('98529331', array('browser'), array('pageviews', 'visits'), null, null, date("Y-"+(date('m')-1)+"-d"), date("Y-m-d"));

    $data_inicio = $_GET['data_inicio'];
    $data_fim = $_GET['data_fim'];

    if ($data_inicio != '' && $data_fim != '') {
        $inicio = $data_inicio; 
        $fim = $data_fim;    
    }
    else{
        $inicio = date('Y-m-d', strtotime('-1 month')); 
        $fim = date('Y-m-d');    
    }
    
    
    $ga->requestReportData($id_property, array('day','month'), array('pageviews', 'visits', 'users','visitors','newUsers','newVisits','avgSessionDuration'), array('month','day'), null, $inicio, $fim, null, null);
    $contador_visitas = 0;
    $contador_pageviews = 0;

    foreach ($ga->getResults() as $dados) {
    	$contador_visitas= $contador_visitas+$dados->getVisits();
        $contador_pageviews = $contador_pageviews+$dados->getPageviews();

        $array_dias[] = $dados->getDay()."/".$dados->getMonth();
        $array_visitas_dias[] = $dados->getVisits();

        // $tipo = $dados->getDevicecategory();
        // if($tipo== 'desktop')
        // {
        //     $array_tipo_dispositivo['desktop'] = $array_tipo_dispositivo['desktop']+1;
        // }
        // elseif($tipo == 'tablet')
        // {
        //     $array_tipo_dispositivo['tablet'] = $array_tipo_dispositivo['tablet']+1;
        // }
        // elseif($tipo == 'mobile')
        // {
        //     $array_tipo_dispositivo['mobile'] = $array_tipo_dispositivo['mobile']+1;
        // }
    }

    $contador_visitas = $ga->getVisits();
    $contador_pageviews = $ga->getPageviews();
    $utilizadores = $ga->getUsers();
    $utilizadores_unicos = $ga->getVisitors();
    $utilizadores_novos = $ga->getNewusers();
    $novas_Visitas = $ga->getNewvisits();
    $media_tempo_visitas = floor($ga->getAvgsessionduration()/60)." min ".round(($ga->getAvgsessionduration()-(floor($ga->getAvgsessionduration()/60)*60)))." seg";
    


    $ga->requestReportData($id_property, array('country'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {

        $array_paises[] = $dados->getCountry();
        $array_visitas_paises[] = $dados->getVisits();
    }

    $ga->requestReportData($id_property, array('city','country'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {

        $array_cities[] = $dados->getCity()." / ".$dados->getCountry();
        $array_visitas_cities[] = $dados->getVisits();
    }

// browserVersion
    $ga->requestReportData($id_property, array('browser'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {
        $array_browser[] = $dados->getBrowser();
        $array_visitas_browser[] = $dados->getVisits();
    }

    $ga->requestReportData($id_property, array('pageTitle'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {
        $array_paginas[] = $dados->getPagetitle();
        $array_visitas_paginas[] = $dados->getVisits();
    }

    $ga->requestReportData($id_property, array('source','medium'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {
        $array_source[] = $dados->getSource().' / '.$dados->getMedium();
        $array_visitas_source[] = $dados->getVisits();
    }

    $ga->requestReportData($id_property, array('medium'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {
        $array_source_global[] = $dados->getMedium();
        $array_visitas_source_global[] = $dados->getVisits();
    }

    $ga->requestReportData($id_property, array('deviceCategory'), array('visits'), array('-visits'), null, $inicio, $fim, null, null);

    foreach ($ga->getResults() as $dados) {
        $array_tipo_dispositivo[] = $dados->getDevicecategory();
        $array_visitas_tipo_dispositivo[] = $dados->getVisits();
    }

   $array_total = array(
        'data_inicio'=>$inicio,
        'data_fim'=>$fim,
        'total_visitas'=>$contador_visitas,
        'total_pageviews'=>$contador_pageviews,
        'total_users'=>$utilizadores,
        'total_visitantes'=>$utilizadores_unicos,
        'total_novos'=>$utilizadores_novos,
        'novas_visitas'=>$novas_Visitas,
        'media_tempo'=>$media_tempo_visitas, 
        'dias'=>$array_dias, 'visitas_dias'=>$array_visitas_dias, 
        'paises'=>$array_paises,'visitas_paises'=>$array_visitas_paises, 
        'cidades' => $array_cities, 'visitas_cidades' => $array_visitas_cities, 
        'browsers'=>$array_browser, 'vistas_browser'=>$array_visitas_browser, 
        'paginas'=>$array_paginas, 'visitas_paginas'=>$array_visitas_paginas, 
        'sources'=>$array_source, 'visitas_sources'=>$array_visitas_source,
        'sources_global'=>$array_source_global, 'visitas_sources_global'=>$array_visitas_source_global,
        'tipo_maquina'=>$array_tipo_dispositivo, 'visitas_tipo_maquina'=>$array_visitas_tipo_dispositivo
    );

   echo json_encode($array_total);


?>