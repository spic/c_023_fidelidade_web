<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");

require '../../base.php';
$base = new base();

$new_item = json_decode(file_get_contents("php://input"));

$flag = $new_item->flag;

$res = $base->return_id($flag);

echo json_encode($res);
?>