<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");


include "../../base.php";
$base = new base();

$new_item = json_decode(file_get_contents("php://input"));
$flag = $new_item->flag;
$id = $new_item->id;

// var_dump($new_item);


$res = $base->getItem_by_id($flag,$id);

if ($flag == 5) {
	
	$res_idiomas = $base->get_list(2);
	$res['idiomas'] = array();
	foreach ($res_idiomas as $key => $value) {
		$array_return_item = array();
		$array_return_item['id_idioma'] = $value['id'];
		$array_return_item['nome_idioma'] = $value['idioma'];
		$array_return_item['sigla'] = $value['sigla'];
		$array_return_item['principal'] = $value['principal'];
		$res_nome = $base->query_simple("SELECT tag,id FROM ".$base->array_tables[6]." WHERE id_idioma=".$value['id']." AND id_tag = ".$id);
		$res_data = $res_nome->fetch_assoc();
		$array_return_item['titulo']	= $res_data['tag'];
		$array_return_item['id']	= (int)$res_data['id'];
		$res['idiomas'][] = $array_return_item;
	}

	echo json_encode($res);	
}
else if ($flag == 3) {
	
	$res_idiomas = $base->get_list(2);
	$res['idiomas'] = array();
	foreach ($res_idiomas as $key => $value) {
		$array_return_item = array();
		$array_return_item['id_idioma'] = $value['id'];
		$array_return_item['nome_idioma'] = $value['idioma'];
		$array_return_item['sigla'] = $value['sigla'];
		$array_return_item['principal'] = $value['principal'];
		$res_nome = $base->query_simple("SELECT titulo,subtitulo,texto1,texto2,id FROM ".$base->array_tables[4]." WHERE id_idioma=".$value['id']." AND id_post = ".$id);
		$res_data = $res_nome->fetch_assoc();
		$array_return_item['titulo']	= $res_data['titulo'];
		$array_return_item['texto1']	= $res_data['texto1'];
		$array_return_item['texto2']	= $res_data['texto2'];
		$array_return_item['subtitulo']	= $res_data['subtitulo'];
		$array_return_item['id']	= (int)$res_data['id'];
		$res['idiomas'][] = $array_return_item;
	}

	$res_tags = $base->query_simple("SELECT id_tag, id FROM ".$base->array_tables[7]." WHERE id_post = ".$id);
	$res['tags'] = array();
	foreach ($res_tags as $key => $value) {
		$array_return_item = array();
		$array_return_item['id'] = (int)$value['id'];
		$array_return_item['id_tag'] = (int)$value['id_tag'];
		$array_return_item['deleted'] = 0;
		$res['tags'][] = $array_return_item;
	}

	$res_media = $base->query_simple("SELECT id_tipo, media, id FROM ".$base->array_tables[9]." WHERE id_post = ".$id);
	$res['media'] = array();
	foreach ($res_media as $key => $value) {
		$array_return_item = array();
		$array_return_item['id'] = (int)$value['id'];
		$array_return_item['tipo'] = (int)$value['id_tipo'];
		$array_return_item['valor'] = $value['media'];
		$array_return_item['deleted'] = 0;
		$res['media'][] = $array_return_item;
	}



	echo json_encode($res);	
}
else{
	$i=0;
	$j=0;
	foreach ($res as $key => $value) {
		if (strpos($key,'imagem') !== false) {
		   $res['imagens_old'][$i] = $value;
		   $i++;
		}
		if (strpos($key,'file') !== false) {
		   $res['ficheiros_old'][$j] = $value;
		   $j++;
		}
		$res[$key] =  str_replace('<br />',"", $value);
	}
	// var_dump($res);
	echo json_encode($res);
}



?>