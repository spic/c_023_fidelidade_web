<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");

require '../../base.php';
$base = new base();

$new_item = json_decode(file_get_contents("php://input"));

$flag = $new_item->flag;
$id = $new_item->id;

$res = $base->get_list_id($flag,$id);

echo json_encode($res);
?>