<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");


include "../../base.php";
$base = new base();

$new_item = json_decode(file_get_contents("php://input"));

$new_item = (array) $new_item;



$item_final = array();
$flag = $new_item['flag'];

//if($flag==3)

$i=0;

if($flag == 5){
	$item_final['area'] = 0;
	$next_id = $base->return_id($flag);
	$res = $base->global_db_insert($flag,$item_final);
	if ($res == 'ok') {
		foreach ($new_item['idiomas'] as $key => $value) {
			$array_item_final = array();
			$array_item_final['id_idioma'] = $value->id_idioma;
			$array_item_final['id_tag'] = $next_id;
			$array_item_final['tag'] = $value->titulo;	
			$res_item = $base->global_db_insert(6,$array_item_final);
		}	
	}
	
}
else if ($flag == 3) {
	$next_id = $base->return_id($flag);
	// $item_final['data'] = date('Y-m-d h:i:s');
	$item_final['template'] = $new_item['template'];
	$item_final['data'] = $new_item['data'];
	foreach ($new_item['idiomas'] as $key => $value) {
		if ($value->principal == 1) {
			$nome_item_lang_principal = $value->titulo;
		}
	}
	$item_final['url_post'] = $next_id."-".$base->generateFriendlyName($nome_item_lang_principal, 100);
	$res = $base->global_db_insert($flag,$item_final);

	if ($res == 'ok') {
		foreach ($new_item['idiomas'] as $key => $value) {
			$array_item_final = array();
			$array_item_final['id_idioma'] = $value->id_idioma;
			$array_item_final['id_post'] = $next_id;
			$array_item_final['titulo'] = $value->titulo;	
			$array_item_final['subtitulo'] = $value->subtitulo;	
			$array_item_final['texto1'] = $value->texto1;
			$array_item_final['texto2'] = $value->texto2;	
			$res_item = $base->global_db_insert(4,$array_item_final);
		}	

		foreach ($new_item['tags'] as $key => $value) {
			$array_item_final = array();
			$array_item_final['id_tag'] = $value->id_tag;
			$array_item_final['id_post'] = $next_id;
			if ($value->id_tag != '') {
				$res_item = $base->global_db_insert(7,$array_item_final);	
			}
			
		}

		foreach ($new_item['media'] as $key => $value) {
			$array_item_final = array();
			$array_item_final['id_post'] = $next_id;
			$array_item_final['id_tipo'] = $value->tipo;
			$array_item_final['media'] = $value->valor;
			if($value->tipo != 0 && $value->valor != ''){
				$res_item = $base->global_db_insert(9,$array_item_final);
			}
		}
	}
}
else{

foreach ($new_item as $key => $value) {
	if ($key != 'flag' || $key != 'especificacoes') {
		if ($key == 'images' && $flag != 2 && $flag !=10 & $flag !=11) {
			$j=1;
			for ($i=0; $i < count($new_item['images']); $i++) { 
				$item_final['logo'] = $new_item['images'][$i]->image;
				$j++;
			}
		}
		else if ($key == 'images'){
			$array_final_images = array();
			foreach ($value as $key2 => $value2) {
				// var_dump($value2);
						
				$item_final_images = array();
				$item_final_images['id_propriedade'] = $base->return_id(2);
				$item_final_images['imagem'] = $value2->image;


				$array_final_images[] = $item_final_images;

				
			}
			$j=1;
			for ($i=0; $i < count($new_item['images']); $i++) {
				$item_final['imagem'.$j] = $new_item['images'][$i]->image;
				$j++;
			
			}
		}
		else if ($key == 'files') {
			$j=1;
			for ($i=0; $i < count($new_item['files']); $i++) {
				$item_final['file'.$j] = $new_item['files'][$i]->file;
				$j++;
			
			}
		}
		else{
			$item_final[$key] = $value;
		}

	}
}

if ($flag == 2) {
	$item_final['url_pt'] = $base->return_id($flag)."-".$base->generateFriendlyName($item_final['nome_pt'], 100);	
	$item_final['url_en'] = $base->return_id($flag)."-".$base->generateFriendlyName($item_final['nome_en'], 100);	
	$item_final['url_fr'] = $base->return_id($flag)."-".$base->generateFriendlyName($item_final['nome_fr'], 100);	
}
if ($flag == 9) {
	$item_final['url_pt'] = $base->return_id($flag)."-".$base->generateFriendlyName($item_final['nome_pt'], 100);	
	$item_final['url_en'] = $base->return_id($flag)."-".$base->generateFriendlyName($item_final['nome_en'], 100);	
	$item_final['url_fr'] = $base->return_id($flag)."-".$base->generateFriendlyName($item_final['nome_fr'], 100);	
}
	
	//var_dump($item_final);
	$next_id = $base->return_id($flag);
	$res = $base->global_db_insert($flag,$item_final);

	if ($res == 'ok') {
		if ($flag ==2) {
			foreach ($new_item['especificacoes'] as $key => $value) {
				$value = (array) $value;
				if (!($value['nome_pt'] == '' && $value['nome_fr'] == '' && $value['nome_en'] == '')) {
					$value['id_propriedade'] = $next_id;
					$res2 = $base->global_db_insert(6,$value);
				}
				
			}

			foreach ($new_item['infos'] as $key => $value) {
				$value = (array) $value;
				if (!($value['nome_pt'] == '' && $value['nome_fr'] == '' && $value['nome_en'] == '')) {
					$value['id_propriedade'] = $next_id;
					$res2 = $base->global_db_insert(7,$value);
				}
				
			}
			// var_dump($array_final_images);
			foreach ($array_final_images as $key => $value) {
				$value = (array) $value;
				$res2 = $base->global_db_insert(8,$value);
			}
		}
	}
}

	//echo $res;




echo $res;


?>