<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");

require '../../base.php';
$base = new base();

$new_item = json_decode(file_get_contents("php://input"));

$flag = $new_item->flag;

$res = $base->get_list($flag);

// var_dump($res);
if ($flag == 5) {
	$array_return = array();
	$res_idiomas = $base->query_simple("SELECT id FROM ".$base->array_tables[2]." WHERE principal=1");
	$id_idioma_principal = $res_idiomas->fetch_assoc()['id'];
	foreach ($res as $key => $value) {
		$array_return_item = array();
		$array_return_item = $value;
		$res_nome = $base->query_simple("SELECT tag FROM ".$base->array_tables[6]." WHERE id_idioma=".$id_idioma_principal." AND id_tag = ".$value['id']);
		$array_return_item['nome']	= $res_nome->fetch_assoc()['tag'];
		$array_return[] = $array_return_item;
	}

	echo json_encode($array_return);	
}
else if ($flag == 3) {
	$array_return = array();
	$res_idiomas = $base->query_simple("SELECT id FROM ".$base->array_tables[2]." WHERE principal=1");
	$id_idioma_principal = $res_idiomas->fetch_assoc()['id'];
	foreach ($res as $key => $value) {
		$array_return_item = array();
		$array_return_item = $value;
		$res_nome = $base->query_simple("SELECT titulo FROM ".$base->array_tables[4]." WHERE id_idioma=".$id_idioma_principal." AND id_post = ".$value['id']);
		$array_return_item['titulo']	= $res_nome->fetch_assoc()['titulo'];
		$array_return[] = $array_return_item;
	}

	echo json_encode($array_return);	
}
else{
	echo json_encode($res);	
}


?>