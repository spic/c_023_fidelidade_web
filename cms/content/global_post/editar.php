<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");


include "../../base.php";
$base = new base();

$new_item = json_decode(file_get_contents("php://input"));
$new_item = (array) $new_item;



$item_final = array();
$flag = $new_item['flag'];
$i=0;


if ($flag == 5) {
	foreach ($new_item as $key => $value) {
		if ($key != 'idiomas') {
			$item_final[$key] = $value;
		}
	}
	$res = $base->global_db_update($flag,$item_final);	

	

	foreach ($new_item['idiomas'] as $key => $value) {
		$array_item_final = array();
		$array_item_final['id_idioma'] = $value->id_idioma;
		$array_item_final['id_tag'] = $new_item['id'];
		$array_item_final['tag'] = $value->titulo;	
		if ($value->id) {
			$array_item_final['id'] = $value->id;
			$res_item = $base->global_db_update(6,$array_item_final);	
		}
		else{
			$res_item = $base->global_db_insert(6,$array_item_final);
		}
		
	}
}
else if($flag == 3){
	foreach ($new_item as $key => $value) {
		if ($key != 'idiomas') {
			$item_final[$key] = $value;
		}
	}
	foreach ($new_item['idiomas'] as $key => $value) {
		if ($value->principal == 1) {
			$nome_item_lang_principal = $value->titulo;
		}
	}
	$item_final['url_post'] = $new_item['id']."-".$base->generateFriendlyName($nome_item_lang_principal, 100);
	$res = $base->global_db_update($flag,$item_final);	

	if ($res == 'ok') {
		foreach ($new_item['idiomas'] as $key => $value) {
			$array_item_final = array();
			$array_item_final['id_idioma'] = $value->id_idioma;
			$array_item_final['id_post'] = $new_item['id'];
			$array_item_final['titulo'] = $value->titulo;	
			$array_item_final['subtitulo'] = $value->subtitulo;	
			$array_item_final['texto1'] = $value->texto1;
			$array_item_final['texto2'] = $value->texto2;
			
			if ($value->id) {
				$array_item_final['id'] = $value->id;	
				$res_item = $base->global_db_update(4,$array_item_final);
			}
			else{
				$res_item = $base->global_db_insert(4,$array_item_final);
			}
			
		}	

		foreach ($new_item['tags'] as $key => $value) {
			if ($value->deleted == 1) {
				if ($value->id) {
					$res_item = $base->global_delete(7,$value->id);
				}
			}
			else{
				$array_item_final = array();
				$array_item_final['id_tag'] = $value->id_tag;
				$array_item_final['id_post'] = $new_item['id'];
				
				if ($value->id_tag != '') {
					if ($value->id) {
						$array_item_final['id'] = $value->id;		
						$res_item = $base->global_db_update(7,$array_item_final);	
					}
					else{
						$res_item = $base->global_db_insert(7,$array_item_final);	
					}
					
				}
			}
			
			
			
		}


		foreach ($new_item['media'] as $key => $value) {
			if ($value->deleted == 1) {
				if ($value->id) {
					$res_item = $base->global_delete(9,$value->id);
				}
			}
			else{
				$array_item_final = array();
				$array_item_final['id_post'] = $new_item['id'];
				$array_item_final['id_tipo'] = $value->tipo;
				$array_item_final['media'] = $value->valor;

				if ($value->tipo!=0 && $value->valor!=''){
					
					if ($value->id) {
						$array_item_final['id'] = $value->id;	
						$res_item = $base->global_db_update(9,$array_item_final);
					}
					else{
						$res_item = $base->global_db_insert(9,$array_item_final);
					}
				}
			}
		}
	}

	
}
else{
	foreach ($new_item as $key => $value) {
			if ($key != 'flag') {
				if ($key == 'images' && $flag != 2 && $flag !=10 && $flag !=11) {
					$j=1;
					for ($i=0; $i < count($new_item['images']); $i++) { 
							$item_final['logo'] = $new_item['images'][$i]->image;
						$j++;
					}
				}
				else if ($key == 'images'){
					$array_final_images = array();
					foreach ($value as $key2 => $value2) {
						// var_dump($value2);
								
						$item_final_images = array();
						$item_final_images['id_propriedade'] = $new_item['id'];
						$item_final_images['imagem'] = $value2->image;


						$array_final_images[] = $item_final_images;

						
					}
					$j=1;
					for ($i=0; $i < count($new_item['images']); $i++) {
						$item_final['imagem'.$j] = $new_item['images'][$i]->image;
						$j++;
					
					}
				}
				else if ($key == 'files') {
					$j=1;
					for ($i=0; $i < count($new_item['files']); $i++) { 
						$item_final['file'.$j] = $new_item['files'][$i]->file;
						$j++;
					}
				}
				else{
					$item_final[$key] = nl2br($value);
				}

			}
		}

		$res = $base->global_db_update($flag,$item_final);	
}
	







echo $res;


?>