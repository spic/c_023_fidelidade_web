'use strict';


var global_services = angular.module('global_services', []);

global_services.service('global_service', function ($http,$q,$location,$window) {
    this.check_user = function(value,flag){
        var deferred = $q.defer();
        $http.post('../global_post/check_user.php?data='+(Math.random()),{'value':value,'flag':flag},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.editar_user = function(array_user){
        var deferred = $q.defer();
        $http.post('global_post/edit_user.php?data='+(Math.random()),JSON.stringify(array_user),{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.get_analitics = function(data_ini,data_fim){
        var deferred = $q.defer();
        $http.get('global_post/analitic.php?data_inicio='+data_ini+'&data_fim='+data_fim+'&data='+(Math.random()),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.updateTime = function(){
        var deferred = $q.defer();
        $http.get('global_post/updateTime.php?data='+(Math.random()),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }


    this.getUser = function(){
        var deferred = $q.defer();
        $http.get('global_post/getUser.php?data='+(Math.random()),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.getAreas = function(){
        var deferred = $q.defer();
        $http.get('global_post/getAreas.php?data='+(Math.random()),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.insert_item = function(new_item){
        var deferred = $q.defer();
        $http.post('global_post/insert.php?data='+(Math.random()),JSON.stringify(new_item),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.insertAllItens = function(new_item,tipos,legislacao,cae){
        var deferred = $q.defer();
        $http.post('global_post/insertAll.php?data='+(Math.random()),JSON.stringify(new_item),JSON.stringify(tipos),JSON.stringify(legislacao),JSON.stringify(cae),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.editar_item = function(new_item){
        var deferred = $q.defer();
        $http.post('global_post/editar.php?data='+(Math.random()),JSON.stringify(new_item),{ cache: false}).success(function(data, status)
        {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.get_list = function(flag){
        var deferred = $q.defer();
        $http.post('global_post/getList.php?data='+(Math.random()),{'flag':flag},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }




	this.getItens = function(atribute, attr_value, table){
    	var deferred = $q.defer();
        $http.post('global_post/getItens.php?data='+(Math.random()),{'atribute':atribute,'attr_value':attr_value,'table':table},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
	}

    this.getDistinct = function(atribute,atribute2,table){
        var deferred = $q.defer();
        $http.post('global_post/getDistinct.php?data='+(Math.random()),{'atribute':atribute,'atribute2':atribute2,'table':table},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.getDistinctItens = function(field,field2,table,atribute1,attr_value1,atribute2,attr_value2){
        var deferred = $q.defer();
        $http.post('global_post/getDistinctItens.php?data='+(Math.random()),{'field':field,'field2':field2,'table':table,'atribute1':atribute1,'attr_value1':attr_value1,'atribute2':atribute2,'attr_value2':attr_value2},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.get_item = function(flag, id){
        var deferred = $q.defer();
        $http.post('global_post/getItem.php?data='+(Math.random()),{'flag':flag,'id':id},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.activar_item = function(flag,id){
        var deferred = $q.defer();
        $http.post('global_post/activarItem.php?data='+(Math.random()),{'flag':flag,'id':id},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.destacar_item = function(flag,id){
        var deferred = $q.defer();
        $http.post('global_post/destacarItem.php?data='+(Math.random()),{'flag':flag,'id':id},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.delete_item = function(flag,id){
        var deferred = $q.defer();
        $http.post('global_post/deleteItem.php?data='+(Math.random()),{'flag':flag,'id':id},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.delete_itens = function(atribute, attr_value, table){
        var deferred = $q.defer();
        $http.post('global_post/deleteItens.php?data='+(Math.random()),{'atribute':atribute,'attr_value':attr_value,'table':table},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

	this.getNumItens = function(atribute, attr_value, table){
    	var deferred = $q.defer();
        $http.post('global_post/getNumItens.php?data='+(Math.random()),{'atribute':atribute,'attr_value':attr_value,'table':table},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
	}

    this.get_list_id = function(flag,id){
        var deferred = $q.defer();
        $http.post('global_post/getListId.php?data='+(Math.random()),{'flag':flag,'id':id},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

     this.get_next_id = function(flag){
            var deferred = $q.defer();
            $http.post('global_post/getNextID.php?data='+(Math.random()),{'flag':flag},{ cache: false}).success(function(data, status) {
                // Some extra manipulation on data if you want...
                deferred.resolve(data);
            }).error(function(data, status) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
});