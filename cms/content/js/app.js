'use strict';

/* App Module */

var cmsApp = angular.module('cmsApp', [
  'ngRoute',
  // 'ngAnimate',
  'angularFileUpload',
  'global_services',
  'global_directives',
  'chart.js'
], function($rootScopeProvider) {
  $rootScopeProvider.digestTtl(100);
});


cmsApp.config(['$routeProvider',
  function($routeProvider,$routeParams) {
    $routeProvider.
    when('/:area/listar', {
      templateUrl: function(params) {
        return 'listar/'+params.area + '_listar.html';
      },
      controller: 'global_controller'
    }).
    when('/:area/inserir', {
      templateUrl: function(params) {
        return 'inserir/'+params.area + '_inserir.html';
      },
      controller: 'global_controller'
    }).
    when('/:area/editar/:id', {
      templateUrl: function(params) {
        return 'editar/'+params.area + '_editar.html';
      },
      controller: 'global_controller'
    }).
    when('/stats', {
      templateUrl: 'stats.html',
      controller: 'stats_controller'
    }).
    when('/user', {
      templateUrl: 'user.html',
      controller: 'user_controller'
    }).
    otherwise({
      redirectTo: '/stats/'
    });
}]);


cmsApp.run(function($rootScope, $location, global_service) {
  $rootScope.loading = 1;
  $rootScope.sucess_form = false;
  $rootScope.areas_cms = [];
    var promise = global_service.getAreas();  
    promise.then(
      function(response){
        $rootScope.areas_cms = response;

        var location_array = $location.path().split('/');
        $rootScope.area_selected = location_array[1];

        var result = $.grep($rootScope.areas_cms, function(e){ return e.url == $rootScope.area_selected; });
        if (result.length) {
           $rootScope.area_selected_titulo = result[0].nome;
           $rootScope.area_selected_flag = result[0].flag;
        } 
        $rootScope.loading = 0;
      }
      ,function(reason){console.log('Failed: ' + reason);}
    );

    var promise = global_service.getUser();  
     promise.then(
        function(response){
          $rootScope.user_array = response;
        }
        ,function(reason){console.log('Failed: ' + reason);}
     );

    $rootScope.$on('$routeChangeStart', function() {
      console.clear();
      $rootScope.loading = 1;
      var location_array = $location.path().split('/');
      $rootScope.area_selected = location_array[1];
      $rootScope.area_selected_mode = location_array[2];

      var result = $.grep($rootScope.areas_cms, function(e){ return e.url == $rootScope.area_selected; });
      if (result.length) {
         $rootScope.area_selected_titulo = result[0].nome;
         $rootScope.area_selected_flag = result[0].flag;
      } 

      var promise = global_service.updateTime();  
      promise.then(
        function(response){
          if(response == 'out'){
            window.location = "index.php";
          }
        }
        ,function(reason){console.log('Failed: ' + reason);}
      );
    });
    
    $rootScope.$on('$viewContentLoaded', function() {
      var wait_hover;
      clearInterval(wait_hover);
      if ($rootScope.sucess_form == true) {
        wait_hover = setTimeout(function(){
          $rootScope.sucess_form = false;
          $rootScope.$apply();
        },2000); 
      };

      var location_array = $location.path().split('/');
      $rootScope.area_selected = location_array[1];
      $rootScope.area_selected_mode = location_array[2];

      if ($rootScope.areas_cms.length == 0) {
        console.info('aqui','voltei');
        var promise = global_service.getAreas();  
        promise.then(
          function(response){
            $rootScope.areas_cms = response;
            $location.path( '/stats/' );
          }
          ,function(reason){console.log('Failed: ' + reason);}
        );
        
      };

      var result = $.grep($rootScope.areas_cms, function(e){ return e.url == $rootScope.area_selected; });
      if (result.length) {
         $rootScope.area_selected_titulo = result[0].nome;
         $rootScope.area_selected_flag = result[0].flag;
      }
      $rootScope.loading = 0;
      
   });
});

cmsApp.filter('myFilter', function () {
    return function (items, keyword) {
        var listToReturn = [];
        if (keyword === undefined) return items;
        if (keyword === '') return items;
        keyword = keyword.toLowerCase();
        
        angular.forEach(items, function (item) {
          var re_data = /^\d{4}-\d{1,2}-\d{1,2}$/;
          var INTEGER_REGEXP = /^\-?\d+$/;

          if(INTEGER_REGEXP.test(keyword)){
            if (item.id == keyword) {
                listToReturn.push(item);
            } 
          }
          else if(re_data.test(keyword)){
            if(item.data){
              if(item.data.toLowerCase().indexOf(keyword) >= 0) {
                listToReturn.push(item);
              }
            }
          }
          else{
            if(item.nome){
              if(item.nome.toLowerCase().indexOf(keyword) >= 0) {
                listToReturn.push(item);
              }
            }
            if(item.titulo){
              if(item.titulo.toLowerCase().indexOf(keyword) >= 0) {
                listToReturn.push(item);
              }
            }
            
                
          }

            
            
            
        });
        
        return listToReturn;
    };
});

cmsApp.filter('range', function(){
  return function(n) {
    var res = [];
    for (var i = 0; i < n; i++) {
      res.push(i);
    }
    return res;
  };
});

cmsApp.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
          // console.log(scope.$parent);
          //alert('ola');
          var scope2 = scope.$parent;
          //console.log(scope2.from);
            $(function(){
              console.log(scope2);
                element.datepicker({
                    dateFormat:'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '-100:+2',
                    //minDate: scope2.minDate,
                    onSelect:function (date) {

                      var date2 = date;
                        if (element.attr('name') == 'from') {
                          var dateString = date2.split('-');
                          scope2.minDate = new Date( dateString[0], dateString[1]-1, dateString[2] );
                        };
                        if (element.attr('name') == 'to') {
                          var dateString = date2.split('-');
                          scope2.maxDate = new Date( dateString[0], dateString[1]-1, dateString[2] );  ;
                        };


                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    },
                    beforeShow:function(){
                      if (element.attr('name') == 'from') {
                        if(scope2.maxDate){
                          element.datepicker( "option", "maxDate", new Date(scope2.maxDate) );
                        }
                      };
                      if (element.attr('name') == 'to') {
                        if(scope2.minDate){
                          element.datepicker( "option", "minDate", new Date(scope2.minDate) );
                        }
                      };
                    }
                });
            });
        }
    }
});

  
  

