'use strict';

cmsApp.controller('user_controller', function stats_controller($scope,$rootScope,global_service,$fileUploader,$location){
  $scope.changeFlag = function(){
      $rootScope.sucess_form = false;
    }

  var promise = global_service.getUser();  
   promise.then(
      function(response){
        $rootScope.user_array = response;
        $scope.email_origin = $rootScope.user_array.email_user;
        $scope.user_origin = $rootScope.user_array.username_user;
      }
      ,function(reason){console.log('Failed: ' + reason);}
   );
   $scope.submitted = false;
   $scope.flag_pass = 0;
   $scope.submit_reg = function(form){
    $scope.flag_pass = 0;
    $scope.submitted = true;
    if (form.$invalid) {
      return;
    }
    else{
      if($rootScope.user_array.password.length < 6 && $rootScope.user_array.password.length > 0){
        return;
      }
      else if($rootScope.user_array.password.length > 0 && ($rootScope.user_array.password != $scope.user_array.password2)){
        return;
      }
      else{
        $rootScope.loading = 1;
      
        var promise = global_service.editar_user($rootScope.user_array);  
          promise.then(
            function(response){
              $rootScope.loading = 0;
              if (response == 'ok') {
                $rootScope.sucess_form = true;
                $location.path( '/stats' );
            }
            else{
              $rootScope.loading = 0;
              $scope.erro_form = true;
              if(response != 'error'){
                $scope.flag_pass = 1;
              }
            }
            }
            ,function(reason){console.log('Failed: ' + reason);}
          );  
      }
      
      
      
    }
  }


});

cmsApp.controller('stats_controller', function stats_controller($scope,$rootScope,global_service,$fileUploader,$location){
  $scope.changeFlag = function(){
      $rootScope.sucess_form = false;
    }
    
  var promise = global_service.getUser();  
   promise.then(
      function(response){
        $rootScope.user_array = response;
      }
      ,function(reason){console.log('Failed: ' + reason);}
   );

   $scope.loading_dados = 1;

  $scope.datas = {
    data_inicio:'',
    data_fim:''
  };

  $scope.areas_stats = $rootScope.areas_cms;
  
  var i=0;
  var promises = [];

  angular.forEach($scope.areas_stats, function(item, key) {
      promises.push(global_service.get_list(item.flag));  
     promises[i].then(
        function(response){
          $scope.areas_stats[key].total_items = response.length;
        $scope.areas_stats[key].total_items_active = $.grep(response, function(e){ return e.activo == 1; }).length;
        
        }
        ,function(reason){console.log('Failed: ' + reason);}
     );
     i++;
  });

  
});


cmsApp.controller('global_controller', function global_controller($scope,$rootScope,global_service,$fileUploader,$location,$routeParams){
      
  
    $scope.init = function(flag, total_images, total_images_ob, total_files, total_files_ob, mode)
    {




      /*INSERIR*/
      if (mode == 'inserir') {
          $scope.new_item ={};
           $scope.new_item.flag = flag;
           $scope.total_images = total_images;
           $scope.total_images_ob = total_images_ob;
           $scope.total_files = total_files;
           $scope.total_files_ob = total_files_ob;
           $scope.new_item.images = [];
          for(var i=0; i<$scope.total_images; i++){
            $scope.new_item.images.push({
              image: '',
            });
          }

          $scope.new_item.files = [];
          for(var i=0; i<$scope.total_files; i++){
            $scope.new_item.files.push({
              file: '',
            });
          } 

          var promise_idiomas = global_service.get_list(2);
          promise_idiomas.then(
            function(response){
              console.log(response);
              $scope.idiomas = response;  
              $scope.new_item.idiomas = [];
              angular.forEach($scope.idiomas, function(lang, key) {
                  $scope.new_item.idiomas.push({
                    id_idioma: lang.id,
                    nome_idioma:lang.idioma,
                    sigla:lang.sigla,
                    principal: lang.principal
                  }); 
              });
              console.log($scope.new_item);
            }
            ,function(reason){console.log('Failed: ' + reason);}
          );
       
      };




      /*EDITAR*/
      
      if (mode == 'editar') {
        $scope.new_item = {};
        $scope.id_item = $routeParams.id;
        // $scope.new_item = [];
        var promise = global_service.get_item($rootScope.area_selected_flag,$scope.id_item);  
       promise.then(
          function(response){
            $scope.new_item =response;
            console.log($scope.new_item);


           $scope.new_item.flag = flag;
           $scope.total_images = total_images;
           $scope.total_images_ob = total_images_ob;
           $scope.total_files = total_files;
           $scope.total_files_ob = total_files_ob;
           
          $scope.new_item.images = [];
          for(var i=0; i<$scope.total_images; i++){
            if ($scope.new_item.imagens_old[i]) {
              $scope.new_item.images.push({
                image: $scope.new_item.imagens_old[i],
              }); 
            }
            else{
              $scope.new_item.images.push({
                image: '',
              }); 
            }
        
          }
          
           

          $scope.new_item.files = [];
          for(var i=0; i<$scope.total_files; i++){

            if ($scope.new_item.ficheiros_old[i]) {
              $scope.new_item.files.push({
                file: $scope.new_item.ficheiros_old[i],
              });
            }
            else{
              $scope.new_item.files.push({
                file: '',
              });
            }
          }

        



        }
          ,function(reason){console.log('Failed: ' + reason);}
       );
              
    };



      /*LISTAGEM*/
      if (mode == 'listar') {
        console.log($rootScope.area_selected);
        $scope.sentido= 0;
        $scope.orderList='id';
        

          
        var promise = global_service.get_list($rootScope.area_selected_flag);  
         promise.then(
            function(response){
              console.info('oi',response);
              $scope.items = response;
              $scope.total_items = $scope.items.length;
              $scope.total_items_show_initial = 12;
              $scope.total_items_show = $scope.total_items_show_initial;
              $scope.total_items_show_negative = $scope.total_items_show*(-1);
              $scope.page_actual = 1;
              $scope.total_pages = Math.ceil($scope.total_items/$scope.total_items_show_initial);
              $scope.pages = {};
              for (var i = 1; i <= $scope.total_pages; i++) {
                $scope.pages[i-1] =i;
              };

            }
            ,function(reason){console.log('Failed: ' + reason);}
         );

         $scope.setPage = function(page){
          $scope.page_actual = page;
          if ($scope.page_actual < 1) {$scope.page_actual = 1;};
          if ($scope.page_actual > $scope.total_pages) {$scope.page_actual = $scope.total_pages;};
          $scope.total_items_show = $scope.total_items_show_initial*$scope.page_actual;
          if ($scope.page_actual == $scope.total_pages) {
            $scope.total_items_show_negative = (-1)*($scope.total_items_show_initial-($scope.total_items_show-$scope.total_items));
          }
          else{
            $scope.total_items_show_negative = $scope.total_items_show_initial*(-1);
          }
          

         }

         $scope.activar = function(id){
          $scope.flagativando = id;
          var promise = global_service.activar_item($rootScope.area_selected_flag,id);  
           promise.then(
              function(response){
                var result = $.grep($scope.items, function(e){ return e.id == id; });
              if (result.length) {
                if (result[0].activo == 1)
                  result[0].activo = 0;
                 else
                  result[0].activo = 1;
              }
                $scope.flagativando = false;
              }
              ,function(reason){console.log('Failed: ' + reason);}
           );
         }

         $scope.destacar = function(id){
          var promise = global_service.destacar_item($rootScope.area_selected_flag,id);  
           promise.then(
              function(response){
                var result = $.grep($scope.items, function(e){ return e.id == id; });
              if (result.length) {
                if (result[0].destaque == 1)
                  result[0].destaque = 0;
                 else
                  result[0].destaque = 1;
              }
              }
              ,function(reason){console.log('Failed: ' + reason);}
           );
         }
         

         

         $scope.search = function(){
          $scope.getSize = function () {
            return {
              'size': $scope.filtered.length
               };
           };

           $scope.$watch($scope.getSize, function(){
            $scope.total_items = $scope.filtered.length;
              $scope.total_items_show = $scope.total_items_show_initial;
              $scope.total_items_show_negative = $scope.total_items_show*(-1);
              $scope.page_actual = 1;
              $scope.total_pages = Math.ceil($scope.total_items/$scope.total_items_show_initial);
              $scope.pages = {};
              for (var i = 1; i <= $scope.total_pages; i++) {
                $scope.pages[i-1] =i;
              };
           } ,true);
         }




      };

      if (flag == 3) {
        if (mode == "inserir") {
          $scope.new_item.tags = [];
          $scope.new_item.tags.push({
               id_tag: '',
               deleted: 0            
           });

          $scope.new_item.media = [];
        }
        var tags_promise = global_service.get_list(5);
          tags_promise.then(
            function(response){
              $scope.tags = response;
            }
            ,function(reason){console.log('Failed: ' + reason);}
         );
 
        $scope.new_tag = function(){
           $scope.new_item.tags.push({
               id_tag: '',
               deleted: 0            
           });
        };

        

        $scope.new_media = function(){
          $scope.new_item.media.push({
            tipo: '',
            valor: '',
            deleted: 0
          });
        }

        if (mode == "inserir") {
          $scope.delete_tag = function(item){
            $scope.new_item.tags.splice($scope.new_item.tags.indexOf(item), 1);
          }

          $scope.delete_media = function(item){
            $scope.new_item.media.splice($scope.new_item.media.indexOf(item), 1);
          }
        }

        if (mode == "editar") {
          $scope.delete_tag = function(item){
            item.deleted = 1;
          }

          $scope.delete_media = function(item){
            item.deleted = 1;
          }
        }

       
      }
/*
      if (flag == 2) {
          $scope.categorias_prop = [];
          var promise_cat_prop = global_service.get_list(3);
          promise_cat_prop.then(
            function(response){
              $scope.categorias_prop = response;

            }
            ,function(reason){console.log('Failed: ' + reason);}
         );

          $scope.tipos_prop = [];
          var promise_tipos_prop = global_service.get_list(4);
          promise_tipos_prop.then(
            function(response){
              $scope.tipos_prop = response;

            }
            ,function(reason){console.log('Failed: ' + reason);}
         );

          $scope.localidades = [];
          $scope.localidades2 = [];
          var localidades_prop = global_service.get_list(5);
          localidades_prop.then(
            function(response){
              $scope.localidades = response;
              angular.forEach($scope.localidades, function (item) {
                var arrayTemp = {'label':item.nome,'value':item.nome, 'desc':item.nome };
                $scope.localidades2.push(arrayTemp);

                
                
              });

            }
            ,function(reason){console.log('Failed: ' + reason);}
         );


        if (mode == "editar" || mode == "inserir") {
          if (mode == "inserir") {
            $scope.new_item.especificacoes = [];   
            $scope.new_item.infos = []; 
          };
          
          $scope.new_spec = function(){
            $scope.new_item.especificacoes.push({
              id: $scope.new_item.especificacoes.length,
              nome_pt: '',
              nome_en: '',
              nome_fr: '',
            });        
          }

          $scope.remove_spec = function(spec){
            $scope.new_item.especificacoes.splice($scope.new_item.especificacoes.indexOf(spec), 1);
          }

            
          $scope.new_info = function(){
            $scope.new_item.infos.push({
              id: $scope.new_item.infos.length,
              nome_pt: '',
              nome_en: '',
              nome_fr: '',
              texto_pt: '',
              texto_en: '',
              texto_fr: '',
            });        
          }
          $scope.remove_info = function(spec){
            $scope.new_item.infos.splice($scope.new_item.infos.indexOf(spec), 1);
          }
        }

        $scope.new_imagem = function(){
          $scope.new_item.images.push({
              image: '',
            });
        }
        



      };

      if (flag == 9) {
        $scope.new_imagem = function(){
          $scope.new_item.images.push({
              image: '',
            });
        }
      }

      if (flag == 10) {
        $scope.portfolios = [];
        var promise_portfolio = global_service.get_list(9);
        promise_portfolio.then(
          function(response){
            $scope.portfolios = response;
          },function(reason){console.log('Failed: ' + reason);}
       );
        
      }

      if (flag == 11) {
        $scope.portfolios = [];
        var promise_portfolio = global_service.get_list(9);
        promise_portfolio.then(
          function(response){
            $scope.portfolios = response;
          },function(reason){console.log('Failed: ' + reason);}
       );

        $scope.imoveis = [];
        var promise_imoveis = global_service.get_list(2);
        promise_imoveis.then(
          function(response){
            $scope.imoveis = response;
          },function(reason){console.log('Failed: ' + reason);}
       );
        
      }*/
    
    };

    $scope.changeFlag = function(){
      $rootScope.sucess_form = false;
    }

    $scope.numItens = 0;
    $scope.item_remove = '';
  $scope.remove_activar = function(id){
  //***********************CASO ESPECIFICO TABELAS COM CAMPOS ASSOCIADOS*****************************//
  //verifica se existe legislacoes associadas ao programa que se pretende apagar
    /*if($rootScope.area_selected == 'programa'){
      var promise = global_service.getNumItens("id_programa",id,2);  
      promise.then(
        function(response){
            if(response > 0){
              $scope.msgm ="Este programa tem "+response+" concursos associadas. Se continuar com esta operação todas serão apagadas.";
              $scope.numItens=response;
            } else {
              $scope.msgm ="";
            }
        },function(reason){console.log('Failed: ' + reason);}
     );

    }*/

    $('#myModal').modal({
      keyboard: false,
      backdrop: 'static'
    });
    $scope.item_remove = id;
    console.log($scope.item_remove);

  }
  $scope.remove_voltar = function(){
    $('#myModal').modal('hide');
    $scope.item_remove = "";
  }
    
  $scope.remove = function(id){
    console.log($scope.numItens);


    var promise = global_service.delete_item($rootScope.area_selected_flag,id);  
     promise.then(
        function(response){
          var result = $.grep($scope.items, function(e){ return e.id == id; });
        if (result.length) {
          $scope.items.splice($scope.items.indexOf(result[0]), 1);
          $scope.filtered.splice($scope.filtered.indexOf(result[0]), 1);
          $scope.total_items = $scope.filtered.length;
            $scope.total_pages = Math.ceil($scope.total_items/$scope.total_items_show_initial);
            if($scope.page_actual>$scope.total_pages){$scope.page_actual=$scope.total_pages}
            $scope.pages = {};
            for (var i = 1; i <= $scope.total_pages; i++) {
              $scope.pages[i-1] =i;
            };
            $scope.setPage($scope.page_actual);
            $rootScope.sucess_form = true;
          } 
        }
        ,function(reason){console.log('Failed: ' + reason);}
     );
  }

  $scope.erro_form = false;

  // Creates a uploader
    var uploader = $scope.uploader = $fileUploader.create({
      scope: $scope,
      // removeAfterUpload: true,
      url: 'global_post/upload.php'
    });

  // REGISTER HANDLERS
    uploader.bind('afteraddingfile', function (event, item, items) {
      if (item.alias == 'image') {
        $('#myModal').modal({
          keyboard: false,
          backdrop: 'static'
        });
        $('#myModal .close').css('display','block');  
      };
      
      item.formData.push(item.formData2);
      item.formData.push({'id':item.item_id});
      console.info('After adding a file', item);
      
      if ($rootScope.area_selected_flag == 3) {
        $scope.new_item.media[item.id].valor = item.file.name;
      }
      else{
        if(item.alias == "image")
          $scope.new_item.images[item.id].image = item.file.name;
        if (item.alias == "file"){ 
          $scope.new_item.files[item.id].file = item.file.name;  
      }
      
        
        console.log($scope.new_item);
        console.log($scope.legilacoes);
      }

      
      var i=0;
      var old_i='null';
      angular.forEach(uploader.queue, function(item_array, key) {
        if(item_array.id == item.id && item_array.alias == item.alias){
          
            if( old_i=='null'){
              old_i=i;
            }
            else{
              uploader.queue.splice(old_i, 1);
            }
          
          
        }
        i++;
      });


    
    
    });

    uploader.bind('whenaddingfilefailed', function (event, item) {
      console.info('When adding a file failed', item);
    });

    uploader.bind('afteraddingall', function (event, items) {
      console.info('After adding all files', items);
    });

    uploader.bind('beforeupload', function (event, item) {
      $('#voltarescolher').css('display','none');
      $("#guardar_imagem").button('loading');
      $('#myModal .close').css('display','none');
      console.info('Before upload', item);
    });

    uploader.bind('progress', function (event, item, progress) {
      console.info('Progress: ' + progress, item);
    });

    uploader.bind('success', function (event, xhr, item, response) {
      $('#myModal').modal('hide');
      if ($rootScope.area_selected_flag == 3) {
        $scope.new_item.media[item.id].valor = response.answer;
      }
      else{
        if (item.alias == "image") {
          $scope.new_item.images[item.id].image = response.answer;  
        }
        else if(item.alias == "file"){
          
          $scope.new_item.files[item.id].file = response.answer;
          
          console.log($scope.new_item);
          
        }     
      }
      
      
      
      console.info('Success', xhr, item, response);
    });

    uploader.bind('cancel', function (event, xhr, item) {
      console.info('Cancel', xhr, item);
    });

    uploader.bind('error', function (event, xhr, item, response) {
      console.info('Error', xhr, item, response);
    });

    uploader.bind('complete', function (event, xhr, item, response) {
      console.info('Complete', xhr, item, response);
    });

    uploader.bind('progressall', function (event, progress) {
      console.info('Total progress: ' + progress);
    });

    uploader.bind('completeall', function (event, items) {
      console.info('Complete all', items);
      // alert("oi");
      if ($rootScope.area_selected_mode == 'inserir') {
        console.log($scope.new_item);
        var promise = global_service.insert_item($scope.new_item);  
        promise.then(
          function(response){
            console.log(response);
              if (response == 'ok') {
                  $rootScope.sucess_form = true;
                  $location.path( '/'+$rootScope.area_selected+'/listar' );
            }
            else{
              $rootScope.loading = 0;
              $scope.erro_form = true;
            }
          }
          ,function(reason){console.log('Failed: ' + reason);}
        );
      };

      if ($rootScope.area_selected_mode == 'editar') {
        var promise = global_service.editar_item($scope.new_item);  
        promise.then(
          function(response){
            console.log(response);
              if (response == 'ok') {
                $rootScope.sucess_form = true;
                $location.path( '/'+$rootScope.area_selected+'/listar' );
            }
            else{
              $rootScope.loading = 0;
              $scope.erro_form = true;
            }

        }
          ,function(reason){console.log('Failed: ' + reason);}
        );
      };
});


  $scope.click_upload = function (element) {
    $("#"+element).click();
  }


  // SUBMIT
  $scope.submitted = false;
  $scope.submit_insert = function(form){
      $scope.success= false;
      $scope.submitted = true;
      if (form.$invalid) {
        return;
      }
      else{
        $rootScope.loading = 1;                  
        if (uploader.queue.length >0) {
          uploader.uploadAll(); 
        }
        else{

            var promise = global_service.insert_item($scope.new_item);  
              promise.then(
                function(response){
                  console.log(response);
                  if (response == 'ok') {
                      $rootScope.sucess_form = true;
                      $location.path( '/'+$rootScope.area_selected+'/listar' );
                  }
                  else{
                    $rootScope.loading = 0;
                    $scope.erro_form = true;
                  }
               }
             ,function(reason){console.log('Failed: ' + reason);});
     }
    }
  }

  $scope.submit_edit = function(form){
    var registo=0;
    $scope.submitted = true;
    if (form.$invalid) {
      return;
    }
    else{
      $rootScope.loading = 1;
      if (uploader.queue.length >0) {
        uploader.uploadAll(); 
      }
      else{
        console.log($scope.new_item);
        var promise = global_service.editar_item($scope.new_item);  
            promise.then(
              function(response){
                console.log(response);
                if (response == 'ok') {
                    $rootScope.sucess_form = true;
                    $location.path( '/'+$rootScope.area_selected+'/listar' );
                }
                else{
                  $rootScope.loading = 0;
                  $scope.erro_form = true;
                }
             }
           ,function(reason){console.log('Failed: ' + reason);});
       }
      }
    }



});
