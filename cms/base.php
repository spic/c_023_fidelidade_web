<?php
error_reporting(E_ALL|E_STRICT);
error_reporting(E_ALL^E_NOTICE);
ini_set("display_errors","off");

class base{
	
	var $database_name;
	var $database_user;
	var $database_pass;
	var $database_host;    
	var $mysqli;
	var $mysqli_schema;

	var $email_smtp;
	var $pass_email_smtp;
	var $clientname;

	var $all_tables;
	var $array_tables;
	var $array_areas;
	var $array_inserts;

	var $desenvolvimento_flag;
	var $email_developer;
	var $flag_error;
	var $flag_success;




	function base()
	{
		/*definir a zona horaria*/
   	date_default_timezone_set('GMT');
	  include("changeable_data.php");
	  
	  $this->database_user = $database_user;
	  $this->database_pass = $database_pass;
	  $this->database_host = $database_host;
	  $this->database_name = $database_name;

	  $this->email_smtp = $email_smtp;
	  $this->pass_email_smtp = $pass_email_smtp;
	  $this->clienturl = $clienturl;
	  $this->database_name = $database_name;


	  //$this->all_tables=array();
	  
	  /*$res=$this->query_simple("SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database_name."'");
	  
	  
	  while($row = $res->fetch_assoc()){
	  	echo $row['TABLE_NAME']."<br />";
	  }*/

	 // tabelas da BD
    $this->array_tables = array(
        "1" => "users_cms",
        "2" => "idiomas",
        "3" => "posts",
        "4" => "posts_idiomas",
        "5" => "tags",
        "6" => "tags_idiomas",
        "7" => "tags_post",
        "8" => "tipo_media",
        "9" => "media_posts",
      );
   	 $array_res = array();
//echo sizeof($this->array_tables);
      for($i=1;$i<=sizeof($this->array_tables);$i++){
	      $res=$this->query_simple("SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database_name."' AND TABLE_NAME='".$this->array_tables[$i]."'");

	      while($row = $res->fetch_assoc()){
		  		$row['coluna'] =$row['COLUMN_NAME'];
		  		
		  		switch ($row['DATA_TYPE'])
		  		{
		  			case 'int':
		  			case 'tinyint':
		  				$row['tipo'] ='i';
		  				break;
		  			case 'float':
		  			case 'double':
		  			case 'decimal':
			  			$row['tipo'] ='d';
			  			break;
		  			case 'varchar':
		  			case 'text':
		  			case 'date':
		  			case 'datetime':
			  			$row['tipo'] ='s';
			  			break;

		  		}
		  		//$row['tipo'] =$row['DATA_TYPE'];
		  		$row['flag']=$i;
		  		$array_res[] = $row;
		  		//var_dump($array_res);
		  }
	   }
	   //var_dump($array_res);

	  $this->array_areas = array(
	  		array("nome" => "Tags", "url" => 'tags', "flag" =>5),
	  		array("nome" => "Posts", "url" => 'posts', "flag" =>3),
	  	);

	  // SELECTS à BD
		  for($f=1;$f<=sizeof($this->array_tables);$f++){
		  	$this->array_selects[$f]="SELECT * FROM ".$this->array_tables[$f];
		  }
		  //var_dump($this->array_selects);
    	/*$this->array_selects = array(
        "1" => "SELECT id,nome,email,activo FROM ".$this->array_tables[1],//." WHERE activo =1",
       	);*/

     // selects à BD por ID
		for($f=1;$f<=sizeof($this->array_tables);$f++){
		  	$this->array_selects_id[$f]="SELECT * FROM ".$this->array_tables[$f]." WHERE id=";
		  }
		  //var_dump($this->array_selects_id);
	    /*$this->array_selects_id = array(
	        "1" => "SELECT * FROM ".$this->array_tables[1]." WHERE id=",
	    );*/

		for($f=1;$f<=sizeof($this->array_tables);$f++){
		  	$this->array_selects_grupoBy[$f]="SELECT * FROM ".$this->array_tables[$f]." GROUP BY ";
		  }


		for($f=1;$f<=sizeof($this->array_tables);$f++){
		  	$this->array_selects_grupo[$f]="SELECT * FROM ".$this->array_tables[$f]." WHERE grupo=";
		  }

		  
	  for($f=1;$f<=sizeof($this->array_tables);$f++){ //percorre as tabelas pela flag
	  	$i=0;
	  	$string="";
	  	$colunas="";
	  	$values="";
	  	$tipos="";
	  	$colunas_editaveis=array();
	 
	  	$string="INSERT INTO ".$this->array_tables[$f]."(";
	  	for($c=0;$c<=sizeof($array_res);$c++)
	  	{
	  		if($array_res[$c]['flag'] == $f)
	  		{
	  			$colunas.= $array_res[$c]['coluna'].',';

	  			if($array_res[$c]['coluna']=='id'){
	  				$values.="null,";
	  			}
	  			else if($array_res[$c]['coluna']=='activo'){
	  				$values.= "0,";

	  			} else if($array_res[$c]['coluna']=='nivel'){
	  				$values.= "1,";
	  			}
	  			else {
	  				$values.= "?,";
	  				$colunas_editaveis[$i]=''.$array_res[$c]['coluna'].'';
	  				$tipos.= $array_res[$c]['tipo'];
	  				$i++;
	  			}
	  			//$values.= "";
	  	
	  		}
	  	}
	  	$colunas=substr($colunas,0,-1);
	  	$values=substr($values,0,-1);
	  	//$colunas_editaveis=substr($colunas_editaveis,0,-1);
	  	$string.=$colunas.") VALUES(".$values.")";
		$this->array_inserts[$f]=array();
		$this->array_inserts[$f][0]=$string;
		$this->array_inserts[$f][1]=''.$tipos.'';
		//$this->array_inserts[$f][2]=array();
		$this->array_inserts[$f][2]=$colunas_editaveis;
	  }
	  //var_dump($this->array_inserts);
/*
	  $this->array_inserts = array(
	  		"1" => array("INSERT INTO ".$this->array_tables[1]."(id,activo,nivel,nome,user,password,email) VALUES(null,0,1,?,?,?,?)",'ssss',array('nome','user','password','email')),
	  );*/

	  
	 for($f=1;$f<=sizeof($this->array_tables);$f++){ //percorre as tabelas pela flag
	  	$i=0;
	  	$string="";
	  	$colunas="";
	  	$values="";
	  	$tipos="";
	  	$colunas_editaveis=array();
	 
	  	$string="UPDATE ".$this->array_tables[$f]." SET ";
	  	for($c=0;$c<=sizeof($array_res);$c++)
	  	{
	  		if($array_res[$c]['flag'] == $f)
	  		{
	  			if($array_res[$c]['coluna']!='id' && $array_res[$c]['coluna']!='activo' && $array_res[$c]['coluna']!='principal' && $array_res[$c]['coluna']!='destaque'){

	  				$colunas.= $array_res[$c]['coluna'].' = ?,';
	  				$colunas_editaveis[$i]=''.$array_res[$c]['coluna'].'';
	  				$tipos.= $array_res[$c]['tipo'];
	  				$i++;
	  			}
	  			//$values.= "";
	  	
	  		}
	  	}

	  	$colunas_editaveis[$i]="id";
	  	$colunas=substr($colunas,0,-1);
	  	//$colunas_editaveis=substr($colunas_editaveis,0,-1);
	  	$string.=$colunas." WHERE id = ?";
		$this->array_updates[$f]=array();
		$this->array_updates[$f][0]=$string;
		$this->array_updates[$f][1]=''.$tipos.'i';
		//$this->array_inserts[$f][2]=array();
		$this->array_updates[$f][2]=$colunas_editaveis;
	  }

//var_dump($this->array_updates);
/*$this->array_updates = array(
	  		"1" => array("UPDATE ".$this->array_tables[1]." SET nome = ?,user = ?,password = ?,email = ? WHERE id = ?",'ssssi',array('nome','user','password','email','id')),
	  );*/

		


	  $this->desenvolvimento_flag = 1;
	  $this->email_developer = "joao@spic.pt";
	  $this->flag_error = "error";
	  $this->flag_success = "ok";
	}

	function get_areas(){
		return $this->array_areas;
	}

	function error_report($error_string){
		if ($this->desenvolvimento_flag == 1) {
			echo $error_string;	
		}
		else{
			$this->send_email($this->email_developer,$this->email_smtp,'admin',$this->email_smtp,$error_string,"Erro ".$this->clienturl,2);
		}
	}

	function connect()
	{
		
		$this->mysqli = new mysqli($this->database_host, $this->database_user, $this->database_pass, $this->database_name);
		if ($this->mysqli->connect_errno) {
			$this->error_report("Failed to connect to MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error);
			$result = $this->flag_error;
		}
		else{
			$result = $this->flag_success;
		}

		return $result;
	}

   function disconnect()
   {
    	$this->mysqli->close(); 
   }

   function query_simple($qry)
	{

		$flag = $this->connect();

		if ($flag == $this->flag_success) {

			if(!$result = $this->mysqli->query($qry)){

				$this->error_report("MySQL error: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
				$result = $this->flag_error;

			}	

			$this->disconnect();
		}
		else{
			$result = $this->flag_error;
		}
		
		return $result;
	}



	function query_multiple($qry)
	{
		$flag = $this->connect();
		
		if ($flag == $this->flag_success) {

			if(!$result = $this->mysqli->multi_query($qry)){

				$this->error_report("MySQL error: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
				$result = $this->flag_error;

			}
			else{
				$result = array();
				$i = 0;
				while ($this->mysqli->more_results()) {
					$this->mysqli->next_result();
					if($result_one = $this->mysqli->store_result())
					{
						$result_one_array = array();
						while ($row = $result_one->fetch_assoc()) {
							$result_one_array[] = $row;
						}

						$result[$i] = $result_one_array;
	            	$result_one->free();	
					}
					else{
						$result[$i] = $this->flag_error;
					}	
					
	            $i++;

				}
			}	

			$this->disconnect();
		}
		else{
			$result = $this->flag_error;
		}
		
		return $result;
	}

	function query_totalrows($qry)
	{
		$result = $this->query_simple($qry);
		if ($result != $this->flag_error) {
			$result = $result->num_rows;
		}
		return $result;
	}  

	function query_multiple_totalrows($qry)
	{
		$result_array = $this->query_multiple($qry);
		if ($result_array != $this->flag_error) {
			$result = array();
			for ($i=0; $i < count($result_array); $i++) { 
				$result[$i] = count($result_array[$i]);
			}
		}
		return $result;
	}



	//devolve o numero de items encontrados com aquele campo
	function get_NumItems($atribute,$attr_value,$table)
	{
		$result = $this->query_simple("SELECT * FROM ".$this->array_tables[$table]." WHERE $atribute=$attr_value");
		if ($result != $this->flag_error) {
			$result = $result->num_rows;
		}
		return $result;
	}   

	function query_insert($qry){
		$flag = $this->connect();
		
		if ($flag == $this->flag_success) {

			if(!$result = $this->mysqli->query($qry)){

				$this->error_report("MySQL error: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
				$result = $this->flag_error;

			}	
			else{
				$result = $this->flag_success;
			}
			$this->disconnect();
		}
		else{
			$result = $this->flag_error;
		}
		
		return $result;
	} 

	function global_activar($table,$id){
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET activo = !activo WHERE id=".$id);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}
	
	function global_activar_grupo($table,$grupo){
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET activo = !activo WHERE grupo=".$grupo);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function global_preferir($table,$id,$grupo_parent){
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET principal = 0 WHERE grupo_produto=".$grupo_parent);
		$res_activo = $this->query_simple("SELECT activo FROM ".$this->array_tables[$table]." WHERE id=".$id);
		if ($res_activo->fetch_assoc()->activo == 0) {
			$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET activo = 1 WHERE id=".$id);	
		}
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET principal = 1 WHERE id=".$id);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}
	
	function global_destacar($table,$id){
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET destaque = !destaque WHERE id=".$id);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function global_destacar_grupo($table,$id){
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET destaque = !destaque WHERE grupo=".$id);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function global_delete($table,$id){
		$result = $this->query_simple("DELETE FROM ".$this->array_tables[$table]." WHERE id=".$id);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function global_delete_grupo($table,$grupo){
		$result = $this->query_simple("DELETE FROM ".$this->array_tables[$table]." WHERE grupo=".$grupo);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function delete_all($table){
		$result = $this->query_simple("DELETE FROM ".$this->array_tables[$table]);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function deleteItens($atribute,$attr_value,$table){
		$result = $this->query_simple("DELETE FROM ".$this->array_tables[$table]." WHERE $atribute=$attr_value");
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}

	function delete_encomenda($table,$id){
		$result = $this->query_simple("UPDATE ".$this->array_tables[$table]." SET apagado=1 WHERE id=".$id);
		if ($result != $this->flag_error) {
			$result = $this->flag_success;
		}
		return $result;
	}



	function getItem_by_id($table,$id){
		$flag = $this->connect();
		
		if ($flag == $this->flag_success) {
			$stmt = $this->mysqli->stmt_init();
			if (!($stmt->prepare("SELECT * FROM  ".$this->array_tables[$table]." WHERE id=?"))) {
			   $this->error_report("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
			   $result = $this->flag_error;
			}
			else{
				if (!$stmt->bind_param("i", $id)) {
					$this->error_report("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
					$result = $this->flag_error;
				}
				else{
					if (!$stmt->execute()) {
						$this->error_report("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
						$result = $this->flag_error;
					}
					else{
						$result = array();
						$metaResults = $stmt->result_metadata();
    					$fields = $metaResults->fetch_fields();
    					$statementParams='';
				      //build the bind_results statement dynamically so I can get the results in an array
				     	foreach($fields as $field){
				         if(empty($statementParams)){
				             $statementParams.="\$column['".$field->name."']";
				         }else{
				             $statementParams.=", \$column['".$field->name."']";
				         }
				    	}
				    	$statment="\$stmt->bind_result($statementParams);";
				    	eval($statment);

						// loop through all result rows
						while ($stmt->fetch()) {
						   foreach($fields as $field){
					         $result[$field->name] = $column[$field->name];
					    	}
						}

						/* free result */
					   $stmt->free_result();

					   /* close statement */
					   $stmt->close();
					}
				}
			}


			$this->disconnect();
		}
		else{
			$result = $this->flag_error;
		}
		
		return $result;
	}

	
	function return_id_lang($lang){
		$return = $this->query_simple("SELECT id FROM ".$this->array_tables[17]." WHERE sigla='".$lang."'");
		return $return->fetch_assoc()['id'];
	}
	     

	function return_id($table)
	{
		$result = $this->query_simple("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '".$this->array_tables[$table]."' AND TABLE_SCHEMA = '".$this->database_name."'");
		$id = $result->fetch_object()->AUTO_INCREMENT;
		return $id;
	}


	function global_db_insert($table,$array_items)
	{
		$flag = $this->connect();
		// var_dump($this->array_inserts);
		// echo "oi ".$table." - ".$this->array_inserts[$table][0];	
		if ($flag == $this->flag_success) {
			$stmt = $this->mysqli->stmt_init();
			if (!($stmt->prepare($this->array_inserts[$table][0]))) { 
			   $this->error_report("Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
			   $result = $this->flag_error;
			}
			else{
				
				$prepare_statement_params='';
				$i=0;
				$array_order_fields = $this->array_inserts[$table][2];
				while ($i< count($array_order_fields)) {
					if(empty($prepare_statement_params)){
		            if ($array_items[$array_order_fields[$i]]) {
		         		$prepare_statement_params.="\$array_items['".$array_order_fields[$i]."']";
		         	}
		         	else{
		         		$array_items[$array_order_fields[$i]]="";	
		         		$prepare_statement_params.="\$array_items['".$array_order_fields[$i]."']";
		         	}
		         }else{
		         	if ($array_items[$array_order_fields[$i]]) {
		         		$prepare_statement_params.=", \$array_items['".$array_order_fields[$i]."']";
		         	}
		         	else{
		         		$array_items[$array_order_fields[$i]]="";	
		         		$prepare_statement_params.=", \$array_items['".$array_order_fields[$i]."']";
		         	} 
		         }
					$i++;
				}

				$prepare_statement = "\$stmt->bind_param('".$this->array_inserts[$table][1]."',".$prepare_statement_params.");";
				eval($prepare_statement);
		    	
				if (!$stmt->execute()) {
					$this->error_report("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
					$result = $this->flag_error;
				}
				else{
					$result = $this->flag_success;
				}
			}


			$this->disconnect();
		}
		else{
			$result = $this->flag_error;
		}
		
		return $result;
	}

	function global_db_update($table,$array_items)
	{
		$flag = $this->connect();
		
		if ($flag == $this->flag_success) {
			$stmt = $this->mysqli->stmt_init();
			if (!($stmt->prepare($this->array_updates[$table][0]))) { 
			   $this->error_report("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
			   $result = $this->flag_error;
			}
			else{
				
				$prepare_statement_params='';
				$i=0;
				$array_order_fields = $this->array_updates[$table][2];
				while ($i< count($array_order_fields)) {
					if(empty($prepare_statement_params)){
		            if ($array_items[$array_order_fields[$i]]) {
		         		$prepare_statement_params.="\$array_items['".$array_order_fields[$i]."']";
		         	}
		         	else{
		         		$array_items[$array_order_fields[$i]]="";	
		         		$prepare_statement_params.="\$array_items['".$array_order_fields[$i]."']";
		         	}
		         }else{
		         	if ($array_items[$array_order_fields[$i]]) {
		         		$prepare_statement_params.=", \$array_items['".$array_order_fields[$i]."']";
		         	}
		         	else{
		         		$array_items[$array_order_fields[$i]]="";	
		         		$prepare_statement_params.=", \$array_items['".$array_order_fields[$i]."']";
		         	} 
		         }
					$i++;
				}

				$prepare_statement = "\$stmt->bind_param('".$this->array_updates[$table][1]."',".$prepare_statement_params.");";

				
				eval($prepare_statement);
		    	
				if (!$stmt->execute()) {
					$this->error_report("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
					$result = $this->flag_error;
				}
				else{
					$result = $this->flag_success;
				}
			}


			$this->disconnect();
		}
		else{
			$result = $this->flag_error;
		}
		
		return $result;
	}

	function get_list($table){
    // echo $this->array_selects[$table];
    $res = $this->query_simple($this->array_selects[$table]);
    $array_res = array();
    while ($row = $res->fetch_assoc()) {
      $row['id'] = (int)$row['id'];
      $row['activo'] = (int)$row['activo'];
      
      $array_res[] = $row;
    }
    return $array_res;
  }

  function get_list_id($table,$id_parent){
    // if($table == 12)
     // echo $this->array_selects_id[$table].$id_parent;
    $res = $this->query_simple($this->array_selects_id[$table].$id_parent);
    $array_res = array();
    while ($row = $res->fetch_assoc()) {
      $row['id'] = (int)$row['id'];
      $row['activo'] = (int)$row['activo'];
		$array_res[] = $row;
    }
    // var_dump($array_res);
    return $array_res;
  }

  function get_list_groupBy($table,$campo){
    // echo $this->array_selects[$table];
    $res = $this->query_simple($this->array_selects_grupoBy[$table].$campo);
    $array_res = array();
    while ($row = $res->fetch_assoc()) {
      $row['id'] = (int)$row['id'];
      $row['activo'] = (int)$row['activo'];
      
      $array_res[] = $row;
    }
    return $array_res;
  }

   function get_list_grupo($table,$grupo){
    // echo $this->array_selects[$table];
    $res = $this->query_simple($this->array_selects_grupo[$table].$grupo);
    $array_res = array();
    while ($row = $res->fetch_assoc()) {
      $row['id'] = (int)$row['id'];
      $row['activo'] = (int)$row['activo'];
      
      $array_res[] = $row;
    }
    return $array_res;
  }

   function get_list_atributos($table,$atrr,$atrr_value,$campo){
    // echo $this->array_selects[$table];
    $res = $this->query_simple($this->array_selects[$table]." WHERE ".$atrr."=".$atrr_value." GROUP BY ".$campo);
    $array_res = array();
    while ($row = $res->fetch_assoc()) {
      $row['id'] = (int)$row['id'];
      $row['activo'] = (int)$row['activo'];
      
      $array_res[] = $row;
    }
    return $array_res;
  }


	//devolve um array com todos os itens encontrados com um determinado atributo
	function get_Itens($atribute,$attr_value,$table)
	{
		$res = $this->query_simple("SELECT * FROM ".$this->array_tables[$table]." WHERE $atribute=$attr_value");
		$array_res = array();
   		while ($row = $res->fetch_assoc()) {
      		$row['id'] = (int)$row['id'];
      		$row['activo'] = (int)$row['activo'];
			$array_res[] = $row;
   		 }
    	// var_dump($array_res);
    	return $array_res;
	}  

	function get_Itens_groupby($atribute,$attr_value,$table,$group_by)
	{
		$res = $this->query_simple("SELECT * FROM ".$this->array_tables[$table]." WHERE $atribute=$attr_value GROUP BY ".$group_by);
		$array_res = array();
   		while ($row = $res->fetch_assoc()) {
      		$row['id'] = (int)$row['id'];
      		$row['activo'] = (int)$row['activo'];
			$array_res[] = $row;
   		 }
    	// var_dump($array_res);
    	return $array_res;
	}  

	function get_grupo($table)
	{
		//echo $this->array_tables[$table];
		$result = $this->query_simple("SELECT grupo FROM ".$this->array_tables[$table]." ORDER BY grupo DESC LIMIT 0,1");
		$grupo=$result->fetch_object()->grupo;
		if(empty($grupo))
			$grupo = 1;
		else
			$grupo++;
    	
    	return $grupo;
	}



	function get_distinct($atribute,$atribute2,$lang,$grupo,$table)
	{
		if($table==3){
			//echo "SELECT DISTINCT $atribute FROM ".$this->array_tables[$table]." WHERE id_lang=$lang AND grupo_atributo=$grupo";
			$res = $this->query_simple("SELECT DISTINCT $atribute FROM ".$this->array_tables[$table]." WHERE grupo_atributo=$grupo");
		}else{
			 if($atribute2 != "")
				$res = $this->query_simple("SELECT DISTINCT $atribute,$atribute2 FROM ".$this->array_tables[$table]);
			else
				$res = $this->query_simple("SELECT DISTINCT $atribute FROM ".$this->array_tables[$table]);
		}
		$array_res = array();
	    while ($row = $res->fetch_assoc()) {
				//$array_res[] = array_map('utf8_encode', $row);
	    	//$row['id'] = (int)$row['id'];

			$array_res[] = $row;
	    }
    	return $array_res;
 
	}
  


	function generate_pass($total = 8)
	{
		$new_pass = '';
		$arrayNumbers = array(48,57);
		$arrayMin = array(65,90);
		$arrayCaps = array(97,122);
		$arrayInterval = array($arrayNumbers,$arrayMin,$arrayCaps);
		for ($i=0; $i < $total; $i++) {
			$codeLength = rand(0,2); 
			$code = rand( $arrayInterval[$codeLength][0] , $arrayInterval[$codeLength][1] );
			$char = chr($code);	
			$new_pass .= $char; 
		}
		return $new_pass;
	}
    
  
	function striptags($string,$ordem)
	{
		if($ordem == 1)
		{
			$string = addslashes($string);
		}
		else
		{
			$string = stripslashes($string);
		}
		return $string;
	}
	
	function generateFriendlyName($phrase, $maxLength)
	{
		$result = preg_replace('/ã/', 'a', $phrase);
		$result = preg_replace('/Ã/', 'a', $result);
		$result = preg_replace('/á/', 'a', $result);
		$result = preg_replace('/Á/', 'a', $result);
		$result = preg_replace('/à/', 'a', $result);
		$result = preg_replace('/À/', 'a', $result);
		$result = preg_replace('/Â/', 'a', $result);
		$result = preg_replace('/â/', 'a', $result);
		
		$result = preg_replace('/Ç/', 'c', $result);
		$result = preg_replace('/ç/', 'c', $result);
		
		$result = preg_replace('/õ/', 'o', $result);
		$result = preg_replace('/Õ/', 'o', $result);
		$result = preg_replace('/ó/','o',$result);
		$result = preg_replace('/Ó/','o',$result);

		$result = preg_replace('/é/','e',$result);
		$result = preg_replace('/É/','e',$result);
		$result = preg_replace('/ê/','e',$result);
		$result = preg_replace('/Ê/','e',$result);



		$result = preg_replace('/í/','i',$result);
		$result = preg_replace('/Í/','i',$result);		
		
		$result = preg_replace('/ú/','u',$result);
		$result = preg_replace('/Ú/','u',$result);


		$result = strtolower($result);

		$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
		$result = trim(preg_replace("/[\s-]+/", " ", $result));
		$result = trim(substr($result, 0, $maxLength));
		$result = preg_replace("/\s/", "-", $result);

		return $result;
	}


	/*
		method = 1 -> smtp
		method = 2 -> mail
	*/
	function send_email($to,$from,$name_from,$replyto,$body,$subject,$method){
		if ($method == 1)
         {
            $smtpinfo["host"] = "localhost";
            $smtpinfo["port"] = "25";
            $smtpinfo["auth"] = true;
            $smtpinfo["username"] = $this->email_smtp;
            $smtpinfo["password"] = $this->pass_email_smtp;

            $mID = md5(uniqid(time()));

            $headers = array (
            	'MIME-Version'=> '1.0', 
            	'From' => $name_from.' <'.$from.'>', 
					'To' => $to, 
					'Subject' => $subject, 
					'Reply-To' => $replyto, 
					'Return-Path' => $replyto, 
					'Organization' => $this->clienturl, 
					'Content-type' => 'text/html; charset=iso-8859-1',
					'X-Sender' => $from ,
					'X-Mailer'=> 'PHP/'.phpversion(),
					"Message-ID" => $mID . "@".$this->clienturl,
					"date" => date("D, d M Y H:i:s"), 
					"X-Priority" => "1",
					"X-MSmail-Priority"=> "High", 
					"X-MimeOLE" => "Produced By ".$this->clienturl
				);

            $mail_object = Mail::factory("smtp", $smtpinfo);
            $mail = $mail_object->send($to, $headers, $body);

            if (PEAR::isError($mail)) {
                $res = "mail ERROR: ".$mail->getMessage();
            } else {
                $res = 0;
            }
        }
        else
        {
            $headers  = "From: ".$name_from." <".$from."> \n";
            $headers .= "Reply-To: ".$replyto." \n";
            $headers .= "MIME-Version: 1.0 \n";
            $headers .= "Return-Path: ".$replyto." \n";
            $headers .= "Organization: ".$this->clienturl." \n";
            $headers .= "X-Sender: <$from> \n";
            $headers .= "X-Sender-IP: ".$_SERVER['SERVER_ADDR']." \r\n";
            $headers .= "Message-ID: <" . md5(uniqid(time())) . "@".$this->clienturl."> \n";
            $headers .= "Sender: <".$from."> \n";
            $headers .= "Date:".date("D, d M Y H:i:s"). "\n";
            $headers .= "X-Priority: 1 ";
            $headers .= "X-MSmail-Priority: High \n";
            $headers .= "X-MimeOLE: Produced By ".$this->clienturl." \n";
            $headers .= "X-Mailer: PHP/" . phpversion()."\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1 \n";
            $mailparams = "-f$from";
            mail($to, $subject, $body, $headers);
            $res=0;
        }
	}
	
		
	/**
	
	5000000
	**/
	function CroppedThumbnail($file,$imgSrc,$thumbnail_width,$thumbnail_height,$name,$sharpen=true,$max_size=50000000) 
	{ 

	$result = "";

	if(!empty($file))
	{

		if ($file["error"] == 0 && $file["size"] < $max_size)
		{
			//echo $file['name'];
			$newname = $imgSrc.$file['name'];
			//if (!file_exists($newname)) 
			//{
			move_uploaded_file($file['tmp_name'], $imgSrc.$file['name']);
				//if(move_uploaded_file($file['tmp_name'], $imgSrc.$file['name']))
				//{
			
					//$imgSrc is a FILE - Returns an image resource.
	    			//getting the image dimensions 
	    			list($width_orig, $height_orig) = getimagesize($imgSrc.$file['name']);  
	    			
	    			$fileType = $file['type'];
	    			// GIF
					if($fileType == "image/gif")
					    $myImage = imagecreatefromgif( $imgSrc.$file['name'] );
					// JPG
					if($fileType == "image/jpeg")
					    $myImage = imagecreatefromjpeg( $imgSrc.$file['name'] );
					if($fileType == "image/pjpeg"){
					    $myImage = imagecreatefromjpeg( $imgSrc.$file['name'] );
					}
					// PNG
					if($fileType == "image/x-png")
					    $myImage = imagecreatefrompng($imgSrc.$file['name']);
					if($fileType == "image/png"){
					    $myImage = imagecreatefrompng($imgSrc.$file['name'] );}
					
					if($width_orig < $thumbnail_width){
						$thumbnail_width = $width_orig;
					}
	    			if($height_orig < $thumbnail_height){
						$thumbnail_height = $height_orig;
					}
					
	    			//$myImage = imagecreatefromjpeg($imgSrc);
						$ratio_orig = $width_orig/$height_orig;
		    			if ($thumbnail_width/$thumbnail_height > $ratio_orig) 
		    			{
		    			   $new_height = $thumbnail_width/$ratio_orig;
		    			   $new_width = $thumbnail_width;
		    			} 
		    			else 
		    			{
		    			   $new_width = $thumbnail_height*$ratio_orig;
		    			   $new_height = $thumbnail_height;
		    			}
					
	    			$x_mid = $new_width/2;  //horizontal middle
	    			$y_mid = $new_height/2; //vertical middle
	    			
	    			
	//    			if($sharpen)
	//    			{
	//    			
	//    				$sharpenMatrix = array(
	//				      array(-1,-1,-1),
	//				      array(-1,16,-1),
	//				      array(-1,-1,-1)
	//				     );
	////0	1	0	    	-1	-1	-1	    	1	-2	1
	////1	-4	1	    	-1	8	-1	    	-2	4	-2
	////0	1	0	    	-1	-1	-1	    	1	-2	1
	//
	//					$divisor = array_sum(array_map('array_sum', $sharpenMatrix));
	//					$offset = 0;
	//					imageconvolution($thumb, $sharpenMatrix, $divisor, $offset);
	//    			}
	    			
	    		
	    			 // JPG
					if($fileType == "image/jpeg" || $fileType == "image/pjpeg" )
					{
						$process = imagecreatetruecolor(round($new_width), round($new_height));		
		    			imagecopyresampled($process, $myImage, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
		    			$thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
		    			imagecopyresampled($thumb, $process, 0, 0, ($x_mid-($thumbnail_width/2)), ($y_mid-($thumbnail_height/2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);
		    			imagedestroy($process);
		    			imagedestroy($myImage);
						imagejpeg( $thumb, $imgSrc.$this->generateFriendlyName($name,200).".jpg" );
						$result = $this->generateFriendlyName($name,200).".jpg";
					}
				
					// GIF
					if($fileType == "image/gif")
					{
						$process = imagecreatetruecolor(round($new_width), round($new_height));			
		    			imagecopyresampled($process, $myImage, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
		    			$thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
		    			imagecopyresampled($thumb, $process, 0, 0, ($x_mid-($thumbnail_width/2)), ($y_mid-($thumbnail_height/2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);
		    			imagedestroy($process);
		    			imagedestroy($myImage);
						imagegif( $thumb, $imgSrc.$this->generateFriendlyName($name,200).".gif" );
						$result = $this->generateFriendlyName($name,200).".gif";
					}
					   
					// PNG
					if($fileType == "image/png" || $fileType == "image/x-png")
					{
						$tmp=imagecreatetruecolor($new_width,$new_height);
		                $src=imagecreatefrompng($imgSrc.$file['name']);
		                imagealphablending($tmp, false);
		                imagesavealpha($tmp,true);
		                $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
		                imagefilledrectangle($tmp, 0, 0, $new_width, $new_height, $transparent); 
		                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
						$thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
						imagealphablending($thumb, false);
		                imagesavealpha($thumb,true);
		                $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
		                imagefilledrectangle($thumb, 0, 0, $new_width, $new_height, $transparent);
		    			imagecopyresampled($thumb, $tmp, 0, 0, ($x_mid-($thumbnail_width/2)), ($y_mid-($thumbnail_height/2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);
		                $con=imagepng($thumb, $imgSrc.$this->generateFriendlyName($name,200).".png");
					    $result = $this->generateFriendlyName($name,200).".png";
					}
					    
					
						
				
			//	}
			//}
			
			
		}
		else{
			$result = 1;
		}
	}
	else{
		$result = 2;
	}	
		
	return $result;
	}	

    //SPECIFIC 

	function login($user, $pass)
	{
		$mysql_query = "SELECT id,nome,email FROM users_cms WHERE user='".$user."' AND pass='".$pass."' AND activo=1";
		$result = $this->query_simple($mysql_query);
		if ($this->query_totalrows($mysql_query) == 0) {
			return false;
		}
		else{
			$user_data = array();
			$user_data['id'] = mysql_result($result, 0, 'id');
			$user_data['nome'] = mysql_result($result, 0, 'nome');
			$user_data['email'] = mysql_result($result, 0, 'email');
			$res_update_lastlogin = $this->iquery("UPDATE users_cms SET lastlogin = '".date("Y-m-d H:i:s")."' WHERE id = ".$user_data['id']);
			return $user_data;
		}
	}

   function registo_user($nome,$apelido,$email,$username)
	{

		$gen_pass = $this->generate_pass();
		$password = md5($username.$email.$gen_pass);
		$nome = $this->striptags($nome,1);
		$apelido = $this->striptags($apelido,1);
		$email = $this->striptags($email,1);
		$username = $this->striptags($username,1);
		
		

		$array_valores = array('nome' => $nome.' '.$apelido, 'user' => $username, 'password' => $password, 'email' => $email );

		$res = $this->global_db_insert(1,$array_valores);

		$body_email = "Foi feito um registo no Backoffice ".$this->clienturl." em nome de ".$nome.' '.$apelido.", username - '".$username."'.<br /> A password gerada para esta conta foi: ".$gen_pass."<br />
		Poderá alterar a password em qualquer altura, na área de utilizador, em www.projectotasashop.com.<br /><br /><br />
		Para activar a sua conta clique <a href='".$this->clienturl."activaConta.php?user=".$username."'>aqui.</a>";
		
		$this->send_email($email,$this->email_smtp,$this->clienturl,$this->email_smtp,utf8_decode($body_email),"Registo de utilizador - ".$this->clienturl,$this->clienturl,2);


		
		return $res;


	}

	function update_user($array_user)
	{
		if($array_user['password_act'] == '')
		{
			return $this->flag_error;
		}
		else{
			$query = "SELECT email,user FROM users_cms WHERE id=".$array_user['id_user'];
			$result = $this->query_simple($query);
			$array_rows = $result->fetch_assoc();	
			$emailBD = $array_rows['email'];
			$usernameBD = $array_rows['user'];
			$password_act = md5($usernameBD.$emailBD.$array_user['password_act']);

			$query = "SELECT id FROM users_cms WHERE password='".$password_act."' AND user='".$usernameBD."'";
			$result = $this->query_totalrows($query);
			if($result == 0)
			{
				return "Password errada";
			}
			else{
				if($array_user['password'] != $array_user['password2'] || (strlen($array_user['password']) < 6 && strlen($array_user['password']) >0))
				{
					return $this->flag_error;
				}
				else if($array_user['username_user'] == '' || $array_user['email_user'] == '' || $array_user['nome_user'] == ''){
					return $this->flag_error;
				}
				else{
					$mysql_query1 = "SELECT id FROM users_cms WHERE user='".$array_user['username_user']."'";
					$result_username = $this->query_simple($mysql_query1);
					$array_rows_username = $result_username->fetch_assoc();	

					$mysql_query2 = "SELECT id FROM users_cms WHERE email='".$array_user['email_user']."'";
					$result_email = $this->query_simple($mysql_query2);
					$array_rows_email = $result_email->fetch_assoc();	

					if(($result_username->num_rows > 0 && $array_rows_username['id']!= $array_user['id_user']) || ($result_email->num_rows > 0 && $array_rows_email['id']!= $array_user['id_user'])){
						return $this->flag_error;
					}
					else{
						if($array_user['password'] == '')
						{
							$password = md5($array_user['username_user'].$array_user['email_user'].$array_user['password_act']);
						}
						else{
							$password = md5($array_user['username_user'].$array_user['email_user'].$array_user['password']);		
						}
						
						$nome = $this->striptags($array_user['nome_user'],1);
						
						$email = $this->striptags($array_user['email_user'],1);
						$username = $this->striptags($array_user['username_user'],1);
						
						

						$array_valores = array('nome' => $nome, 'user' => $username, 'password' => $password, 'email' => $email , 'id' => $array_user['id_user'] );

						$res = $this->global_db_update(1,$array_valores);

						$body_email = "Foi feito um registo no Backoffice ".$this->clienturl." em nome de ".$nome.' '.$apelido.", username - '".$username."'.<br /> A password gerada para esta conta foi: ".$gen_pass."<br />
						Poderá alterar a password em qualquer altura, na área de utilizador do backoffice.<br /><br /><br />
						Para activar a sua conta clique <a href='".$this->clienturl."activaConta.php?user=".$username."'>aqui.</a>";
						// $this->send_email($email,$this->email_smtp,$this->clienturl,$this->email_smtp,$body_email,"Registo de utilizador - ".$this->clienturl,$this->clienturl,2);


						
						return $res;
					}
					
				}
			}


		}

		
		
	}
    
   function recuperarPass($email){
   	$query = "SELECT id FROM users_cms WHERE email='".$email."' AND activo=1";
		$result = $this->query_totalrows($query);

		if($result == 1){
			$query = "SELECT email,user,id,nome FROM users_cms WHERE email='".$email."'";
			$result = $this->query_simple($query);
			$array_rows = $result->fetch_assoc();	
			$emailBD = $array_rows['email'];
			$usernameBD = $array_rows['user'];
			$nomeBD = $array_rows['nome'];
			$idBD = $array_rows['id'];

			$gen_pass = $this->generate_pass();
			$password = md5($usernameBD.$emailBD.$gen_pass);

			$array_valores = array('nome' => $nomeBD, 'user' => $usernameBD, 'password' => $password, 'email' => $emailBD , 'id' => $idBD );
			$res = $this->global_db_update(1,$array_valores);
			
			if($res == $this->flag_success){
				$body_email = "Foi feito um pedido de nova password para a conta ligada a este email. <br />
				A nova password é: ".$gen_pass."<br />
				E o username é: ".$usernameBD."<br /><br />
				Se não fez nenhum pedido peço que descarte este email.";

				$this->send_email($email,$this->email_smtp,$this->clienturl,$this->email_smtp,utf8_decode($body_email),"Nova password - ".$this->clienturl,$this->clienturl,2);	
			}
			

			return $res;

		}
		else{
			return $this->flag_error;
		}
   }
    
    
	
}

?>
