'use strict';


var global_directives = angular.module('global_directives', []);

var USER_LENGTH = 4;
global_directives.directive('username', ['global_service', function(global_service) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      var return_var = '';
      ctrl.$parsers.unshift(function(viewValue) {
        if (viewValue) {

          if (viewValue.length >= USER_LENGTH) {
            
            // it is valid
            var promise = global_service.check_user(viewValue,1); 
            promise.then(
              function(response){
                if (response == 1) {
                  if(scope.user_origin == viewValue)
                  {
                    scope.userError = "";
                    ctrl.$setValidity('username', true);
                    return_var = viewValue;
                  }
                  else{
                    scope.userError = "Este username já existe!";
                    // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('username', false);
                    return_var = viewValue;  
                  }
                }
                else{
                  scope.userError = "";
                  ctrl.$setValidity('username', true);
                  return_var = viewValue; 
                }
              },
              function(reason){
                console.log('Failed: ' + reason);
              }
            ); 
            return viewValue;
          } else {
            scope.userError = "No minino "+USER_LENGTH+" digitos";
            // it is invalid, return undefined (no model update)
            ctrl.$setValidity('username', false);
            return viewValue;
          }
        }
        else{
          ctrl.$setValidity('username', false);
          return undefined;
        }
        
      });
    }
  };
}]);  

var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
global_directives.directive('emailregisto', ['global_service', function(global_service) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      var return_var = '';
      ctrl.$parsers.unshift(function(viewValue) {
        if (EMAIL_REGEXP.test(viewValue)) {
          // it is valid
          var promise = global_service.check_user(viewValue,2); 
          promise.then(
            function(response){
              if (response == 1) {
                if (scope.email_origin == viewValue) {
                  scope.emailError = "";
                  ctrl.$setValidity('emailregisto', true);
                  return_var = viewValue; 
                }
                else{
                  scope.emailError = "Este email já existe!";
                  // it is invalid, return undefined (no model update)
                  ctrl.$setValidity('emailregisto', false);
                  return_var = viewValue;  
                }
                
              }
              else{
                scope.emailError = "";
                ctrl.$setValidity('emailregisto', true);
                return_var = viewValue; 
              }
            },
            function(reason){
              console.log('Failed: ' + reason);
            }
          );
          return viewValue;
        } else {

          scope.emailError = "Este email não é valido!";
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('emailregisto', false);
          return undefined;
        }
      });
    }
  };
}]);   

var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
global_directives.directive('email', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (EMAIL_REGEXP.test(viewValue)) {
          // it is valid
          ctrl.$setValidity('email', true);
          return viewValue;
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('email', false);
          return undefined;
        }
      });
    }
  };
});  

var URL_REGEXP = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
global_directives.directive('url', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (viewValue == '') {
          ctrl.$setValidity('url', true);
          return viewValue;
        }
        else{
          if (URL_REGEXP.test(viewValue)) {
            // it is valid
            ctrl.$setValidity('url', true);
            return viewValue;
          } else {
            // it is invalid, return undefined (no model update)
            ctrl.$setValidity('url', false);
            return undefined;
          }  
        }
        
      });
    }
  };
});  

var INTEGER_REGEXP = /^\-?\d+$/;
cmsApp.directive('integer', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (INTEGER_REGEXP.test(viewValue)) {
          // it is valid
          ctrl.$setValidity('integer', true);
          return viewValue;
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('integer', false);
          return undefined;
        }
      });
    }
  };
});

var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
cmsApp.directive('smartFloat', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (FLOAT_REGEXP.test(viewValue)) {
          ctrl.$setValidity('float', true);
          return parseFloat(viewValue.replace(',', '.'));
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});

/**
* The ng-thumb directive
* @author: nerv
* @version: 0.1.2, 2014-01-09
*/
cmsApp.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);


cmsApp.directive('table', function() {
    return {
        link : function (scope, element, attrs) {
          // console.log(scope.$parent);
          //alert('ola');
          var scope2 = scope.$parent;
          //console.log(scope2.from);
            // $(function(){
              console.log(element);
              jQuery(element).dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true
              });
            // });
        }
    }
});



cmsApp.directive('simpledatepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
          // console.log(ngModelCtrl);
          var flag_define = 0;
          var flag_change_limits = 0;
          // console.log(scope);
          // console.log(attrs);
          //alert('ola');
          var scope2 = scope.$parent;
          //console.log(scope2.from);
            $(function(){
              // console.log(scope2);
                element.datepicker({
                  format: "yyyy-mm-dd",
                  multidate: false,
                  autoclose: true,
                  todayHighlight: true,
                  language: 'pt-BR',

                }).on('show', function(e){
                  if (flag_define==0) {
                    $(this).datepicker('setDate',ngModelCtrl.$modelValue);
                    flag_define=1;
                  };
                    
                  if (flag_change_limits == 0) {
                    if (attrs.ngModel.indexOf("data_inicio") != -1) {
                      if (scope.data) {
                        $(this).datepicker('setEndDate',scope.data.data_fim);
                      };
                      if (scope.new_item) {
                         $(this).datepicker('setEndDate',scope.new_item.data_fim);
                      };
                    }
                    else if (attrs.ngModel.indexOf("data_fim") != -1) {
                      
                      if (scope.data) {
                        $(this).datepicker('setStartDate',scope.data.data_inicio);
                      };
                      if (scope.new_item) {
                         $(this).datepicker('setStartDate',scope.new_item.data_inicio);
                      };   
                   }; 
                   flag_change_limits = 1;
                  };

                  

                }).on('changeDate', function(e){
                    if (attrs.ngModel.indexOf("data_inicio") != -1) {
                      if (scope.data) {
                        if (scope.data.data_inicio > scope.data.data_fim && scope.data.data_fim!='') {
                          scope.data.data_inicio = scope.data.data_fim;
                          $(this).datepicker('setDate',scope.data.data_fim);
                        };  
                      };
                      if (scope.new_item) {
                        if (scope.new_item.data_inicio > scope.new_item.data_fim && scope.new_item.data_fim!='') {
                          scope.new_item.data_inicio = scope.new_item.data_fim;
                          $(this).datepicker('setDate',scope.new_item.data_fim);
                        };
                      };
                    }
                    else if (attrs.ngModel.indexOf("data_fim") != -1) {
                      if (scope.data) {
                        if (scope.data.data_fim < scope.data.data_inicio && scope.data.data_inicio !='') {
                          scope.data.data_fim = scope.data.data_inicio;
                          $(this).datepicker('setDate',scope.data.data_inicio);
                        }  
                      }
                      if (scope.new_item) {
                        if (scope.new_item.data_fim < scope.new_item.data_inicio && scope.new_item.data_inicio !='') {
                          scope.new_item.data_fim = scope.new_item.data_inicio;
                          $(this).datepicker('setDate',scope.new_item.data_inicio);
                        }
                      };
                      
                    };
                    flag_change_limits = 0;
                });

            });
        }
    }
});