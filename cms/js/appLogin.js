'use strict';

/* App Module */

var cmsLoginApp = angular.module('cmsLoginApp', [
  'global_services',
  'global_directives'
]);


cmsLoginApp.controller('loginController', function loginController($scope,global_service,$http){
  $scope.submitted = false;
  $scope.msg_login = '';
  $scope.recuperar_flag = 0;
  $scope.submitted_recuperar = false;
  $scope.msg_recuperar='';

  $scope.login = function(form){
    $scope.submitted = true;
    if (form.$invalid) {
      return;
    }
    else{
      var promise = global_service.login($scope.login_user); 
      promise.then(
        function(response){
          if (response == 'ok') {
            $scope.msg_login = '';

            $("#loginForm").submit();
          }
          else{
            $scope.msg_login = response;  
          }
          
        },
        function(reason){
          console.log('Failed: ' + reason);
        }
      ); 

    }
  }

  $scope.recuperar = function(form){
    $scope.submitted_recuperar = true;
    if (form.$invalid) {
      return;
    }
    else{
      var promise = global_service.recuperar($scope.email_recuperar); 
      promise.then(
        function(response){
          if (response == 'ok') {
            $scope.msg_recuperar = 'A nova password foi enviada para o seu email. Por favor verifique o email, e volte a tentar fazer login.';
            
          }
          else{
            $scope.msg_recuperar = "Email não existe na base de dados!";  
          }
          
        },
        function(reason){
          console.log('Failed: ' + reason);
        }
      ); 

    }
  }
}); 

cmsLoginApp.controller('registoController', function registoController($scope,global_service,$http){
  $scope.submitted = false;

  $scope.register = function(form){
    $scope.submitted = true;
    if (form.$invalid) {
      return;
    }
    else{
      var promise = global_service.registo($scope.new_user); 
      promise.then(
        function(response){
          console.log(response);
        },
        function(reason){
          console.log('Failed: ' + reason);
        }
      ); 

    }
  }
  
}); 
