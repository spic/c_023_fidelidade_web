'use strict';


var global_services = angular.module('global_services', []);

global_services.service('global_service', function ($http,$q,$location,$window) {
    
    this.recuperar = function(email){
        var deferred = $q.defer();
        $http.post('global_post/recuperarPass.php?data='+(Math.random()),{'email':email},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

	this.getItem = function(atribute, attr_value, table){
    	var deferred = $q.defer();
        $http.post('global_post/getItem.php?data='+(Math.random()),{'atribute':atribute,'attr_value':attr_value,'table':table},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
	}

    this.check_user = function(value,flag){
        var deferred = $q.defer();
        $http.post('global_post/check_user.php?data='+(Math.random()),{'value':value,'flag':flag},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

	this.getNumItens = function(atribute, attr_value, table){
    	var deferred = $q.defer();
        $http.post('global_post/getNumItens.php?data='+(Math.random()),{'atribute':atribute,'attr_value':attr_value,'table':table},{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
	}

    this.registo = function(new_user){
        var deferred = $q.defer();
        $http.post('global_post/registo.php?data='+(Math.random()),JSON.stringify(new_user),{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.login = function(login_user){
        var deferred = $q.defer();
        $http.post('global_post/login.php?data='+(Math.random()),JSON.stringify(login_user),{ cache: false}).success(function(data, status) {
            // Some extra manipulation on data if you want...
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
});