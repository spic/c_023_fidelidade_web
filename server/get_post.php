<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");

require_once('../classes/database.class.php');

$dados = json_decode(file_get_contents("php://input"));

$url_post = $dados->url_post;
$lang = $dados->lang;

$database = new database();

$id_lang = $database->return_id_lang($lang);
if (count($id_lang) > 1 ) {
	$lang = $id_lang[2];
	$id_lang = $id_lang[1];
}

$array_arguments = array($url_post,$id_lang);
$string_args = 'si';

$where_string_tags = "";
$table_tags = "";


$string_sql = "SELECT 
	".$database->array_tables[3].".id AS id, 
	".$database->array_tables[3].".data as data, 
	".$database->array_tables[3].".url_post as url_post, 
	".$database->array_tables[3].".template as template,
	".$database->array_tables[4].".titulo as titulo,
	".$database->array_tables[4].".subtitulo as subtitulo, 
	".$database->array_tables[4].".texto1 as texto1,
	".$database->array_tables[4].".texto2 as texto2
	FROM ".$database->array_tables[3].",".$database->array_tables[4].$table_tags."
	WHERE 
	activo =1 AND ".$database->array_tables[3].".url_post = ? AND 
	".$database->array_tables[3].".id = ".$database->array_tables[4].".id_post AND
	".$database->array_tables[4].".id_idioma = ?";

	// echo $string_sql;

$res = $database->query_simple_prepare($string_sql, $array_arguments, $string_args);

/*VERIFICAR TOTAL*/
$total_posts = 1;


$array_return_items = array();
foreach ($res as $key => $value) {
	$array_return_item = array();
	foreach ($value as $key2 => $value2) {
		if ($key2 == 'data') {
			$data_array_string = explode(' ',$value2);
			$data_array_string = explode('-',$data_array_string[0]);
			$array_return_item[$key2] = $data_array_string[2].".".$data_array_string[1].".".$data_array_string[0];	
		}
		else{
			$array_return_item[$key2] = $value2;	
		}
		
	}	

	/*GET MEDIA POR POST*/
	$res_media = $database->query_simple_prepare("SELECT id_tipo, media FROM ".$database->array_tables[9]." WHERE id_post=?",array($value['id']),'i');
	$array_return_item['media'] = $res_media;

	$res_tag_media = $database->search($array_return_item['media'],'id_tipo',1);
	$array_return_item['total_media_tipo1'] = count($res_tag_media);
	if (count($res_tag_media) > 0) {
		$array_return_item['tag_media'] = 1;
	}
	else{
		$res_tag_media = $database->search($array_return_item['media'],'id_tipo',2);
		
		if (count($res_tag_media) > 0) {
			$array_return_item['tag_media'] = 2;
		}
		else{
			$array_return_item['tag_media'] = 3;
		}
	}

	if ($array_return_item['template'] == 3) {
		$array_return_item['tag_media'] = 3;
	}

	if ($array_return_item['template'] == 4) {
		$array_return_item['tag_media'] = 2;
	}

	if ($array_return_item['template'] == 2) {
		$array_return_item['tag_media'] = 2;
	}

	$res_tag_media_img = $database->search($array_return_item['media'],'id_tipo',2);
	$array_return_item['total_media_tipo2'] = count($res_tag_media_img);

	/*GET TAGS POR POST*/
	$res_tags = $database->query_simple_prepare("SELECT ".$database->array_tables[6].".tag as tag_nome, ".$database->array_tables[7].".id_tag as id_tag FROM ".$database->array_tables[6].",".$database->array_tables[7]." WHERE ".$database->array_tables[7].".id_post=? AND ".$database->array_tables[6].".id_tag = ".$database->array_tables[7].".id_tag AND ".$database->array_tables[6].".id_idioma = ?",array($value['id'],$id_lang),'ii');
	$array_return_item['tags'] = $res_tags;

	$string_tags = '';
	foreach ($array_return_item['tags'] as $key2 => $value2) {
		$string_tags .= ' '.$value2['tag_nome'].',';
	}
	$array_return_item['tags_string'] = substr($string_tags, 0, -1);

	$array_return_item['texto1'] = $database->gera_links($array_return_item['texto1']);
	$array_return_item['texto2'] = $database->gera_links($array_return_item['texto2']);

	
	$array_return_item['text_divide'] = array($array_return_item['texto1'],$array_return_item['texto2']);

	$array_return_items[] = $array_return_item;
}

$array_return = array('total_posts' => $total_posts, 'posts' => $array_return_items);

echo json_encode($array_return);


?>