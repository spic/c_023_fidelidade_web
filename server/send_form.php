<?php


require_once('../classes/mail.class.php');

$dados = json_decode(file_get_contents("php://input"));

$form_item = $dados->form_item;
$lang = $dados->lang;

$mail = new mail();

if ($lang == 'en') {
	$body ="<span style='font:12px arial;color:#000000'>";
	$body .="<strong>Name:</strong> ".$form_item->nome."<br />";
	$body .="<strong>Email:</strong> ".$form_item->email."<br />";
	$body .="<strong>Phone:</strong> ".$form_item->telefone."<br />";
	$body .="<strong>Company:</strong> ".$form_item->company."<br />";
	$body .="<strong>Professional title:</strong> ".$form_item->professional."<br />";
	$body .="<strong>Subject:</strong> ".$form_item->assunto."<br />";
	$body .="<strong>Who are you?:</strong> ".$form_item->who."<br />";
	$body .="<strong>Where are you from?:</strong> ".$form_item->where."<br />";
	$body .="<strong>What’s your budget?:</strong> ".$form_item->howmuch."<br />";
	$body .="<strong>Message:</strong> ".$form_item->msg;
	$body .= "</span>";

	$body = utf8_decode($body);
	$assunto = utf8_decode($form_item->assunto);

	$assunto_copia = "Copy of message sent - ".$assunto;
}

$res_user = $mail->send_email($form_item->email,'noreply@fivethousandmiles.com','5TMiles website',$form_item->email,$body,$assunto_copia,2);
$res = $mail->send_email('info@fivethousandmiles.com','noreply@fivethousandmiles.com','5TMiles website',$form_item->email,$body,$assunto,2);
$res_teste = $mail->send_email('joao@spic.pt','noreply@fivethousandmiles.com','5TMiles website',$form_item->email,$body,$assunto,2);

echo $res;


?>