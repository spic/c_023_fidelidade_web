<?php


require_once('../classes/database.class.php');
require_once('../classes/mail.class.php');

$dados = json_decode(file_get_contents("php://input"));

$form_item = $dados->form_item;
$lang = $dados->lang;

$database = new database();

$res = $database->query_simple_prepare("INSERT INTO ".$database->array_tables[10]."(email) VALUES(?)",array($form_item->email),'s');

if (count($res) == 0) {
	if ($lang == 'en') {
		$body ="<span style='font:12px arial;color:#000000'>";
		$body .="Successfully made application<br />";
		$body .="<strong>Email:</strong> ".$form_item->email."<br />";
		$body .= "</span>";

		$body = utf8_decode($body);
		$assunto = utf8_decode("Newsletter application");

		$assunto_copia = "Copy of message sent - ".$assunto;
	}
	$mail = new mail();
	$res_user = $mail->send_email($form_item->email,'noreply@5tmiles.com','5TMiles website',$form_item->email,$body,$assunto,2);
	echo 0;
}
else{
	echo $database->flag_error;
}








?>