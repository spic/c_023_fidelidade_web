<?php
  include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Agenda  </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" href="../css/agenda.css">

     <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <div class="background"></div>
    
     <div class="col-md-6 col-xs-12 pd0">
      <div class="col-xs-12 pd0 holder_segundo_esquerda">
         <div class="col-xs-12 pd0">
            <img src="../img/logo_novo.png" alt="Pensar Maior" width="173"/>
        </div>
         <p class="titulo_agenda_css">REUNIÃO<br />DE TRABALHO</p>
      </div>
       
      
      <div class="col-xs-12 pd0 holder_palestra">
        <p class="titulo_apres">ABERTURA</p>
        <p class="oradores">Boas Vindas, <br> Jorge Magalhães Correia</p>
        <div class='button saber_mais'>SABER MAIS</div>
      </div>

      <div class="col-xs-12 pd0 red_back holder_palestra">
        <p class="titulo_apres">ABERTURA</p>
       <div style='float:left;position:relative;margin-right:10px'>
            <img src='../agenda/img/icon_activo.png' width='25' />
        </div>
        <div style='float:left;position:relative'>
           
            <p class='oradores'>Boas Vindas, <br> Jorge Magalhães Correia</p>
        </div>
        <div style="clear:left;"></div>
        <div class='button saber_mais'>SABER MAIS</div>
      </div>
      
    </div>

    <div class="col-md-6 col-xs-12 pd0 holder_segundo_direita" style="padding-top: 50px; min-height: 100vh;">
        <div class="col-xs-12 holder_voltar_agenda" onclick="javascript: window.history.back();">
            <div><img src="../agenda/img/setavoltar.svg" alt="" width="25"></div>
            <div >VOLTAR</div>
        </div>
        <p class="titulo_agenda color_red">Paulo Macedo (CGD)</p>
        <div style="clear:left;"></div>
        <p class="resumo_titulo">BIO</p>
        <div style="clear:left;"></div>
        <p class="resumo_text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
        <div style="clear:left;"></div>


    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script>
      $(document).ready(function(){

        $("#inicio").click(function(){
          $(".fundocinza").css("display","none");
          $("#inicio").css("display","none");
        });

      });
    </script>
    
  </body>
</html>