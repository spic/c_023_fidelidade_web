<?php
  include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Agenda  </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" href="../css/agenda.css">
     <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <div class="background"></div>
    
    <div class="col-md-6 col-xs-12 pd0">
      <div class="col-xs-12 pd0 holder_segundo_esquerda">
        <div class="col-xs-12 pd0">
            <img src="../img/logo_novo.png" alt="Pensar Maior" width="173"/>
        </div>
         <p class="titulo_agenda_css">REUNIÃO<br />DE TRABALHO</p>
      </div>
       
      
      <div class="col-xs-12 pd0 holder_palestra">
        <p class="titulo_apres">ABERTURA</p>
        <p class="oradores">Boas Vindas, <br> Jorge Magalhães Correia</p>
        <div class='button saber_mais'>SABER MAIS</div>
      </div>

      <div class="col-xs-12 pd0 red_back holder_palestra">
        <p class="titulo_apres">ABERTURA</p>
       <div style='float:left;position:relative;margin-right:10px'>
            <img src='../agenda/img/icon_activo.png' width='25' />
        </div>
        <div style='float:left;position:relative'>
           
            <p class='oradores'>Boas Vindas, <br> Jorge Magalhães Correia</p>
        </div>
        <div style="clear:left;"></div>
        <div class='button saber_mais'>SABER MAIS</div>
      </div>
      
    </div>

    <div class="col-md-6 col-xs-12 pd0 holder_segundo_direita">

      <p class="titulo_agenda color_red">TECNOLOGIA</p>
      <div style="clear:left;"></div>
      <p class="subtitulo_agenda">Ameaças e Oportunidades</p>
      <div style="clear:left;"></div>
      <p class="suvsubtitulo_agenda">Paulo Macedo (CGD)</p>
      <div style="clear:left;"></div>
      <p class="resumo_titulo"> RESUMO</p>
      <div style="clear:left;"></div>
      <p class="resumo_text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>  
      <div style="clear:left;"></div>
      <div class='button_orador_holder'>
        <a href='bio.php'>
          <div class='button_orador'>
              <div style='padding-top:10px'><img src='../agenda/img/pessoa.svg' alt='pessoa' width='20'></div>
              <div class='button_orador_nome'>CV Paulo Macedo</div>
          </div>
        </a>
      </div>
       <div class='button_orador_holder'>
        <a href=''>
          <div class='button_orador'>
              <div style='padding-top:10px'><img src='../agenda/img/pessoa.svg' alt='pessoa' width='20'></div>
              <div class='button_orador_nome'>CV Paulo Macedo</div>
          </div>
        </a>
      </div>
       <div class='button_orador_holder'>
        <a href=''>
          <div class='button_orador'>
              <div style='padding-top:10px'><img src='../agenda/img/pessoa.svg' alt='pessoa' width='20'></div>
              <div class='button_orador_nome'>CV Paulo Macedo</div>
          </div>
        </a>
      </div>

      <div class="col-xs-12 pd0" style="padding-left: 16px; padding-right: 16px;">
        
           <div class="holder_button_duvidas" style="cursor:pointer; width: 100%; margin-left: 0%;">
                <div class="button_duvidas_img">
                    <img src="../agenda/img/duvidas.svg" alt="" width="30">
                </div>
                <div>
                    DÚVIDAS
                </div>
            </div>
        
      </div>

    </div>


    <div class="col-xs-12 pd0 holder_popup_mensagem ">
      <div class="col-xs-6 col-xs-offset-3 pd0 holder_popup" >
            <div class="close_popup"><img src="../agenda/img/close.svg" alt="" width="25"></div>
            <div class="form_agenda_input">
                <div>
                    ASSUNTO
                </div>
                <div>
                    <input type="text" placeholder="phpaqui" value="">
                </div>
                
            </div>

            <div class="form_agenda_input" style="margin-top:4px">
                <div>
                    EMAIL
                </div>
                <div>
                    <input type="text" value="">
                </div>
                
            </div>

            <div class="form_agenda_textarea">
                <div>
                    DÚVIDAS
                </div>
                <textarea name="" id="">
                </textarea>
            </div>

            <div class="form_agenda_button_holder">
                <div class='button saber_mais'>SUBMETER</div>
            </div>
      </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script>
      $(document).ready(function(){
        $("#inicio").click(function(){
          $(".fundocinza").css("display","none");
          $("#inicio").css("display","none");
        });

        $(".holder_button_duvidas").click(function(){
          $(".holder_popup_mensagem").addClass("holder_popup_mensagem_ativo");
        });

         $(".close_popup").click(function(){
          $(".holder_popup_mensagem").removeClass("holder_popup_mensagem_ativo");
        });


        $(".enviar_email").click(function(){
          $(".holder_popup_mensagem_email").addClass("holder_popup_mensagem_ativo");
        });

        $(".close_popup").click(function(){
          $(".holder_popup_mensagem_email").removeClass("holder_popup_mensagem_ativo");
        });

      });
    </script>
    
  </body>
</html>