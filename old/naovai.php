<?php

    error_reporting(E_ALL|E_STRICT);
    ini_set("display_errors","off");
    ini_set('error_log','my_file.log');

    require("classes/fidelidade.class.php");
	$fidelidade = new fidelidade();


    $string_error = "";

    $email_get = $_GET['email'];
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $email_get = $_POST['email_input'];
    }
    $flag_email = 0;
    if($email_get == ""){
        // header("Location: index.php");
        $flag_email= 1;
    }
    else{
        $res_dados = $fidelidade->get_email_dados($email_get);
        if($res_dados == 'error'){
            header("Location: index.php");
            exit();
        }
        if($res_dados['flag_registo'] == 1 && $res_dados['presenca_evento'] == 1){
            header("Location: resumo.php?email=".$email_get);
            exit();
        }

        include("classes/mail.class.php");
        $mail = new mail();

        $message = "";
        $url_img = $fidelidade->pathgeral;
        $font_title = "24px";
        $font_items = "14px";
        $font_subtitle = "17px";
        $font_links = "14px";
        $width_img = "715";
		
		
		$message .= '<img src="'.$url_img.'img/newsletters_topo.jpg" width="'.$width_img.'"/>';
		$message .= '<p style="font-family: \'azo\',arial;font-size: '.$font_title.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:20px;margin-top:20px;">Agradecemos a sua resposta e lamentamos que n&atilde;o possa estar presente no Pensar Maior 2017.</p>';
		
        $mail->send_email($email_get,"info@pensarmaior.com","Fidelidade","info@pensarmaior.com",$message,"Registo no evento Pensar Maior 2017",2);

        $data = date("Y-m-d H:s:i");
        $res_update = $fidelidade->query_simple_prepare("UPDATE ".$fidelidade->array_tables[2]." SET flag_registo=1, 	presenca_evento	=0, data_registo=? WHERE email=?",array($data,$email_get),"ss");

    }

    

?>
<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
       
        
        <div class="background"></div>

		<div class="logo_holder">
			<img src="img/logo_novo.png" alt="Pensar Maior" width="100%"/>
		</div>

		<div class="logo_data">
			<img src="img/data_logo_sano.png" alt="Pensar Maior" width="100%"/>
		</div>

        <?php
            if($flag_email == 1){
        ?>
            <div class="login_holder">
                <form action="" method="post" id="login_form" name="login_form">
                    <p class="title red">O SEU EMAIL</p>	
                    <p class="text black">Insira aqui o seu email para aceder ao seu perfil.</p>	
                    <div class="form_holder">
                        <input type="email" name="email_input" id="email_input" value="<?php echo $email_get?>" required placeholder="Email" />
                    </div>
                    <input type="submit" value="CONTINUAR" class="button submit_class"/>
                    
                </form>
            </div>
        <?php
            }
        ?>
        <?php
            if($flag_email == 0){
        ?>
            <div class="container_form" style="margin-top:30px;">
                <p class="verybigtitle red"><span class="bigtitle black" style="font-size: 25px;">Agradecemos a sua resposta</span><br /> <span class="text black" style="margin-top: -13px;display: block;font-size:18px;">e lamentamos que não possa estar presente no</span><br /> <span class="text red" style="margin-top: -43px;display: block;font-size: 20px;"><strong>Pensar Maior 2017.</strong></span></p>
            </div>
        <?php
            }
        ?>


        
        
		

		<div class="logo_footer">
			<!--<img src="img/fidelidade_caixa_branca.png" alt="Fidelidade" class="back"/>-->
			<img src="img/logo_fidelidade_cinza.png" alt="Fidelidade" width="100%"/>
		</div>

		<?php echo $string_error; ?>



        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main_new.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>