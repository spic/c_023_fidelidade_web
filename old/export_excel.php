<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("classes/database.class.php");
$database = new database();

$res = $database->query_simple_prepare("SELECT * FROM ".$database->array_tables[2]." ORDER BY flag_registo DESC, tipo ASC",array(),"");


require 'classes/PHPExcel.php';
$headings = array('Tipo','Nome', 'Email', 'Empresa','Sigla','Telemovel','Cod Acesso','Restrições Alimentares','Transfer','Alojamento','Cartão de Cidadão','Validade CC','Registo Feito','Vai ao evento','Jantar','Observações','Localidade');

$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->setTitle('Listagem de Convidados');
$rowNumber = 1;

$col = 'A';
foreach($headings as $heading) {
	$objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$heading);
	$col++;
}

$rowNumber = 2;
$contador_bebidas = 0;
foreach ($res as $key => $value) {

    switch ($value['tipo']) {
        case '0':{
            $tipo = "Mediador";
            break;
        }
        case '1':{
            $tipo = "Colaborador";
            break;
        }
        case '2':{
            $tipo = "Corretor";
            break;
        }
        case '3':{
            $tipo = "Convidado";
            break;
        }
    };
	$objPHPExcel->getActiveSheet()->setCellValue("A".$rowNumber,$tipo);

	$objPHPExcel->getActiveSheet()->setCellValue("B".$rowNumber,$value['nome']);
	$objPHPExcel->getActiveSheet()->setCellValue("C".$rowNumber,$value['email']);
	$objPHPExcel->getActiveSheet()->setCellValue("D".$rowNumber,$value['empresa']);
    $objPHPExcel->getActiveSheet()->setCellValue("E".$rowNumber,$value['sigla']);
    $objPHPExcel->getActiveSheet()->setCellValue("F".$rowNumber,$value['n_telemovel']);
    $objPHPExcel->getActiveSheet()->setCellValue("G".$rowNumber,$value['cod_acesso']);

    $rest = "";
    if(strpos($value['restricoes_ali'], '0') !== false)
        $rest .= "Sem restrições, ";
    if(strpos($value['restricoes_ali'], '1') !== false)
        $rest .= "Vegetariano, ";
    if(strpos($value['restricoes_ali'], '2') !== false)
        $rest .= "Celíaco, ";
    if(strpos($value['restricoes_ali'], '3') !== false)
        $rest .= "Outros - ".$value['rest_ali_outras'].", ";
    $rest = substr($rest, 0, -2);
    if($value['flag_registo'] == "0") $rest = "";
    if($value['presenca_evento'] == "0") $rest = "";
    $objPHPExcel->getActiveSheet()->setCellValue("H".$rowNumber,$rest);

    switch ($value['transfer']) {
        case '0':
            $transfer = "Não";
            break;
        case '1':
            $transfer = $value['ponto_partida'];
            break;
    } 
    if($value['flag_registo'] == "0") $transfer = "";
    $objPHPExcel->getActiveSheet()->setCellValue("I".$rowNumber,$transfer);
	
    switch ($value['alojamento']) {
        case '0':
            $aloja = "Não";
            break;
        case '1':
            $aloja = "Dia 25";
            break;
        case '2':
            $aloja = "Dia 24 e Dia 25";
            break;
    }
    if($value['flag_registo'] == "0") $aloja = "";
    $objPHPExcel->getActiveSheet()->setCellValue("J".$rowNumber,$aloja);

    $objPHPExcel->getActiveSheet()->setCellValue("K".$rowNumber,$value['cc']);
    $objPHPExcel->getActiveSheet()->setCellValue("L".$rowNumber,$value['cc_validade']);


    switch ($value['flag_registo']) {
        case '0':
            $registo = "Não";
            break;
        case '1':
            $registo = "Sim";
            break;
    }
    $objPHPExcel->getActiveSheet()->setCellValue("M".$rowNumber,$registo);

    switch ($value['presenca_evento']) {
        case '0':
            $presenca = "Não";
            break;
        case '1':
            $presenca = "Sim";
            break;
    }
    if($value['flag_registo'] == "0") $presenca = "";
    $objPHPExcel->getActiveSheet()->setCellValue("N".$rowNumber,$presenca);

    switch ($value['semjantar']) {
        case '0':
            $jantar = "Sim";
            break;
        case '1':
            $jantar = "Não";
            break;
    }
    if($value['flag_registo'] == "0") $jantar = "";
    if($value['presenca_evento'] == "0") $jantar = "";
    $objPHPExcel->getActiveSheet()->setCellValue("O".$rowNumber,$jantar);
    $objPHPExcel->getActiveSheet()->setCellValue("P".$rowNumber,$value['observacoes']);

    $objPHPExcel->getActiveSheet()->setCellValue("Q".$rowNumber,$value['localidade']);


	$rowNumber++;
}

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);

$objPHPExcel->getActiveSheet()->setTitle('Estatisticas');
$rowNumber = 0;
$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+1),"Colaboradores inscritos:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=1 AND flag_registo=1 AND presenca_evento=1",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+1),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+2),"Colaboradores que não vão:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=1 AND flag_registo=1 AND presenca_evento=0",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+2),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+3),"Mediadores inscritos:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=0 AND flag_registo=1 AND presenca_evento=1",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+3),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+4),"Mediadores que não vão:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=0 AND flag_registo=1 AND presenca_evento=0",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+4),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+5),"Corretores inscritos:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=2 AND flag_registo=1 AND presenca_evento=1",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+5),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+6),"Corretores que não vão:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=2 AND flag_registo=1 AND presenca_evento=0",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+6),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+7),"Convidados inscritos:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=3 AND flag_registo=1 AND presenca_evento=1",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+7),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+8),"Convidados que não vão:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE tipo=3 AND flag_registo=1 AND presenca_evento=0",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+8),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+9),"Total inscritos:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE flag_registo=1 AND presenca_evento=1",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+9),count($res));

$objPHPExcel->getActiveSheet()->setCellValue("A".($rowNumber+10),"Total que não vão:");
$res = $database->query_simple_prepare("SELECT id FROM ".$database->array_tables[2]." WHERE flag_registo=1 AND presenca_evento=0",array(),"");
$objPHPExcel->getActiveSheet()->setCellValue("B".($rowNumber+10),count($res));


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: text/html; charset=iso-8859-1');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Convidados.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>