<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="-286.019 -84.285 929 532" width="100%" style="min-height:350px" enable-background="new -286.019 -84.285 929 532" xml:space="preserve">
<g id="mesa10_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 10, cadeira_fechado: array_convidados[10].flag_fechado == 1}">
	<g id="mesa10" class="cadeira_pointer" ng-click="sel_cadeira(10)">
		<polygon fill="#E5E5E5" class="cor1" points="30.192,36.853 33.121,40.892 56.07,48.503 79.178,80.31 121.586,49.499 98.475,17.693 
			98.345,-6.497 95.414,-10.53"/>
		
			<rect x="62.618" y="29.34" transform="matrix(0.809 -0.5878 0.5878 0.809 -11.8355 61.5697)" fill="#F3F3F3" class="cor2" width="52.416" height="39.315"/>
		
			<rect x="23.957" y="12.681" transform="matrix(0.809 -0.5878 0.5878 0.809 3.3553 40.6748)" fill="#F3F3F3" class="cor2" width="80.618" height="4.988"/>
		<polygon fill="#E5E5E5" class="cor1" points="98.345,-6.497 33.121,40.892 56.07,48.503 98.475,17.693 		"/>
		<g id="adicionar10" class="button_cadeira">
			<circle fill="#E02729" cx="89.174" cy="48.826" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="99.457" y1="48.828" x2="78.892" y2="48.828"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="89.173" y1="59.11" x2="89.173" y2="38.546"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M49.893,1.073c0,12.627-10.162,22.789-22.789,22.789h-191.707
				c-12.627,0-22.79-10.162-22.79-22.789s10.163-22.79,22.79-22.79H27.104C39.576-21.717,49.893-11.553,49.893,1.073"/>
			
				<text id="nome_x5F_10" class="text_nome_cadeira" transform="matrix(1 0 0 1 -157.5974 8.6145)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[10].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="30.8" cy="1.073" r="8.161"/>
		</g>
		<g id="erase_x5F_11_2_" class="erase_nome_cadeira" ng-click="delete_convidado(10)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -55.7773 -132.0094)" fill="#E02227" cx="-187.238" cy="1.324" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-193.398" y1="-4.778" x2="-181.08" y2="7.539"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-193.398" y1="7.539" x2="-181.08" y2="-4.778"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa9_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 9, cadeira_fechado: array_convidados[9].flag_fechado == 1}">
	<g id="mesa9" class="cadeira_pointer" ng-click="sel_cadeira(9)">
		<polygon fill="#E5E5E5" class="cor1" points="-22.468,151.717 -17.726,153.26 5.315,145.93 42.705,158.079 58.904,108.227 21.511,96.079 
			7.19,76.586 2.447,75.044 		"/>
		
			<rect x="5.906" y="107.417" transform="matrix(0.309 -0.9511 0.9511 0.309 -98.6658 118.3539)" fill="#F3F3F3" class="cor2" width="52.417" height="39.316"/>
		
			<rect x="-47.954" y="111.668" transform="matrix(0.3091 -0.951 0.951 0.3091 -113.8534 71.6025)" fill="#F3F3F3" class="cor2" width="80.616" height="4.988"/>
		<polygon fill="#E5E5E5" class="cor1" points="7.19,76.586 -17.726,153.26 5.315,145.93 21.511,96.079 		"/>
		<g id="adicionar9" class="button_cadeira">
			<circle fill="#E02729" cx="32.456" cy="126.904" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="42.739" y1="126.908" x2="22.175" y2="126.908"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="32.455" y1="137.188" x2="32.455" y2="116.623"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M-23.196,106.955c0,12.626-10.162,22.789-22.789,22.789
				h-191.707c-12.627,0-22.79-10.163-22.79-22.789c0-12.626,10.163-22.79,22.79-22.79h191.707
				C-33.512,84.165-23.196,94.328-23.196,106.955"/>
			
				<text id="nome_x5F_9_1_" class="text_nome_cadeira" transform="matrix(1 0 0 1 -230.6853 114.4963)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[9].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="-42.289" cy="106.955" r="8.161"/>
		</g>
		<g id="erase_x5F_11_1_" class="erase_nome_cadeira" ng-click="delete_convidado(9)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -152.0543 -152.6789)" fill="#E02227" cx="-260.327" cy="107.206" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-266.487" y1="101.104" x2="-254.169" y2="113.421"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-266.487" y1="113.421" x2="-254.169" y2="101.104"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa8_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 8, cadeira_fechado: array_convidados[8].flag_fechado == 1}">
	<g id="mesa8" class="cadeira_pointer" ng-click="sel_cadeira(8)">
		<polygon fill="#E5E5E5" class="cor1" points="2.436,275.583 7.181,274.044 21.511,254.571 58.903,242.422 42.706,192.569 5.315,204.72 
			-17.731,197.367 -22.473,198.908 		"/>
		
			<rect x="12.463" y="197.362" transform="matrix(0.9511 -0.309 0.309 0.9511 -67.512 20.8668)" fill="#F3F3F3" class="cor2" width="39.317" height="52.417"/>
		
			<rect x="-10.134" y="196.157" transform="matrix(0.9511 -0.309 0.309 0.9511 -73.4404 9.2112)" fill="#F3F3F3" class="cor2" width="4.989" height="80.623"/>
		<polygon fill="#E5E5E5" class="cor1" points="-17.731,197.367 7.181,274.044 21.511,254.571 5.315,204.72 		"/>
		<g id="adicionar8" class="button_cadeira">
			<circle fill="#E02729" cx="32.455" cy="223.394" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="42.737" y1="223.397" x2="22.175" y2="223.397"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="32.454" y1="233.678" x2="32.454" y2="213.113"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M-23.196,243.324c0,12.626-10.162,22.789-22.789,22.789
				h-191.707c-12.627,0-22.79-10.163-22.79-22.789c0-12.627,10.163-22.79,22.79-22.79h191.707
				C-33.512,220.534-23.196,230.697-23.196,243.324"/>
			
				<text id="nome_x5F_8" class="text_nome_cadeira" transform="matrix(1 0 0 1 -230.6853 250.8655)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[8].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="-42.289" cy="243.324" r="8.161"/>
		</g>
		<g id="erase_x5F_11" class="erase_nome_cadeira" ng-click="delete_convidado(8)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -248.4822 -112.7372)" fill="#E02227" cx="-260.327" cy="243.576" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-266.487" y1="237.473" x2="-254.169" y2="249.791"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-266.487" y1="249.791" x2="-254.169" y2="237.473"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa7_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 7, cadeira_fechado: array_convidados[7].flag_fechado == 1}">
	<g id="mesa7" class="cadeira_pointer" ng-click="sel_cadeira(7)">
		<polygon fill="#E5E5E5" class="cor1" points="95.405,361.154 98.339,357.119 98.486,332.941 121.596,301.135 79.189,270.323 56.082,302.13 
			33.114,309.731 30.183,313.763 		"/>
		
			<rect x="69.186" y="275.429" transform="matrix(0.5878 -0.809 0.809 0.5878 -207.4029 196.2052)" fill="#F3F3F3" class="cor2" width="39.315" height="52.415"/>
		
			<rect x="61.764" y="295.132" transform="matrix(0.5878 -0.809 0.809 0.5878 -244.8904 190.2587)" fill="#F3F3F3" class="cor2" width="4.987" height="80.62"/>
		<polygon fill="#E5E5E5" class="cor1" points="33.114,309.731 98.339,357.119 98.486,332.941 56.082,302.13 		"/>
		<g id="adicionar7" class="button_cadeira">
			<circle fill="#E02729" cx="89.184" cy="301.458" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="99.468" y1="301.458" x2="78.904" y2="301.458"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="89.183" y1="311.741" x2="89.183" y2="291.176"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M49.893,358.751c0,12.626-10.162,22.79-22.789,22.79h-191.707
				c-12.627,0-22.79-10.164-22.79-22.79c0-12.627,10.163-22.79,22.79-22.79H27.104C39.576,335.961,49.893,346.124,49.893,358.751"/>
			
				<text id="nome_x5F_7" class="text_nome_cadeira" transform="matrix(1 0 0 1 -157.5974 366.2932)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[7].nome}}</text>
			<circle fill="#4D4D4D"  class="circulo_nome_cadeira" cx="30.8" cy="358.751" r="8.161"/>
		</g>
		<g id="erase_x5F_11_3_" class="erase_nome_cadeira" ng-click="delete_convidado(7)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -308.7024 -27.2452)" fill="#E02227" cx="-187.238" cy="359.003" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-193.398" y1="352.901" x2="-181.08" y2="365.219"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-193.398" y1="365.219" x2="-181.08" y2="352.901"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa6_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 6, cadeira_fechado: array_convidados[6].flag_fechado == 1}">
	<g id="mesa6" class="cadeira_pointer" ng-click="sel_cadeira(6)">
		<polygon fill="#E5E5E5" class="cor1" points="220.915,375.745 220.915,370.76 206.824,351.112 206.824,311.797 154.407,311.797 
			154.407,351.112 140.293,370.76 140.293,375.745 		"/>
		<rect x="154.407" y="311.797" fill="#F3F3F3" class="cor2" width="52.417" height="39.315"/>
		<rect x="140.293" y="370.76" fill="#F3F3F3" class="cor2" width="80.621" height="4.985"/>
		<g id="adicionar6" class="button_cadeira">
			<circle fill="#E02729" cx="180.961" cy="331.278" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="191.243" y1="331.28" x2="170.68" y2="331.28"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="180.961" y1="341.562" x2="180.961" y2="320.999"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M196.289,417.259c0,12.627-10.163,22.79-22.789,22.79H-18.208
				c-12.626,0-22.79-10.163-22.79-22.79c0-12.626,10.164-22.79,22.79-22.79h191.707
				C185.971,394.469,196.289,404.633,196.289,417.259"/>
			
				<text id="nome_x5F_6" class="text_nome_cadeira" transform="matrix(1 0 0 1 -11.2024 424.8019)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[6].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="177.195" cy="417.259" r="8.161"/>
		</g>
		<g id="erase_x5F_11_5_" class="erase_nome_cadeira" ng-click="delete_convidado(6)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -307.1935 93.4111)" fill="#E02227" cx="-40.843" cy="417.511" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-47.002" y1="411.409" x2="-34.684" y2="423.727"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-47.002" y1="423.727" x2="-34.684" y2="411.409"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa5_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 5, cadeira_fechado: array_convidados[5].flag_fechado == 1}">
	<g id="mesa5" class="cadeira_pointer" ng-click="sel_cadeira(5)">
		<polygon fill="#E5E5E5" class="cor1" points="331.016,313.776 328.088,309.742 305.138,302.128 282.03,270.322 239.622,301.132 
			262.733,332.937 262.864,357.13 265.796,361.162 		"/>
		
			<rect x="246.173" y="281.984" transform="matrix(0.809 -0.5878 0.5878 0.809 -125.2845 217.7282)" fill="#F3F3F3" class="cor2" width="52.416" height="39.317"/>
		
			<rect x="256.634" y="332.969" transform="matrix(0.809 -0.5878 0.5878 0.809 -140.4688 238.6108)" fill="#F3F3F3" class="cor2" width="80.618" height="4.984"/>
		<polygon fill="#E5E5E5" class="cor1" points="262.864,357.13 328.088,309.742 305.138,302.128 262.733,332.937 		"/>
		<g id="adicionar5" class="button_cadeira">
			<circle fill="#E02729" cx="272.727" cy="301.455" r="19.931"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="283.009" y1="301.457" x2="262.447" y2="301.457"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="272.726" y1="311.738" x2="272.726" y2="291.173"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M309.516,358.751c0-12.627,10.317-22.79,22.789-22.79h191.707
				c12.627,0,22.79,10.163,22.79,22.79c0,12.626-10.163,22.79-22.79,22.79H332.305C319.678,381.541,309.516,371.377,309.516,358.751
				"/>
			
				<text id="nome_x5F_5" class="text_nome_cadeira" transform="matrix(1 0 0 1 351.7957 366.2932)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[5].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="328.61" cy="358.751" r="8.161"/>
		</g>
		<g id="erase_x5F_11_6_" class="erase_nome_cadeira" ng-click="delete_convidado(5)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -93.7414 491.7013)" fill="#E02227" cx="546.648" cy="359.003" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="540.489" y1="365.219" x2="552.807" y2="352.901"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="540.489" y1="352.901" x2="552.807" y2="365.219"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa4_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 4, cadeira_fechado: array_convidados[4].flag_fechado == 1}">
	<g id="mesa4" class="cadeira_pointer" ng-click="sel_cadeira(4)">
		<polygon fill="#E5E5E5" class="cor1" points="383.676,198.913 378.936,197.371 355.895,204.701 318.503,192.553 302.304,242.404 
			339.697,254.552 354.022,274.045 358.763,275.586 		"/>
		
			<rect x="302.888" y="203.917" transform="matrix(0.309 -0.9511 0.9511 0.309 14.774 467.4855)" fill="#F3F3F3" class="cor2" width="52.419" height="39.318"/>
		
			<rect x="328.535" y="234.011" transform="matrix(0.309 -0.9511 0.9511 0.309 29.9469 514.2233)" fill="#F3F3F3" class="cor2" width="80.622" height="4.985"/>
		<polygon fill="#E5E5E5" class="cor1" points="354.022,274.045 378.936,197.371 355.895,204.701 339.697,254.552 		"/>
		<g id="adicionar4" class="button_cadeira">
			<circle fill="#E02729" cx="329.447" cy="223.377" r="19.931"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="339.729" y1="223.38" x2="319.165" y2="223.38"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="329.446" y1="233.659" x2="329.446" y2="213.096"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M382.605,243.324c0-12.627,10.317-22.79,22.789-22.79h191.707
				c12.627,0,22.79,10.163,22.79,22.79c0,12.626-10.163,22.789-22.79,22.789H405.394
				C392.767,266.113,382.605,255.951,382.605,243.324"/>
			
				<text id="nome_x5F_4" class="text_nome_cadeira" transform="matrix(1 0 0 1 428.1608 250.8655)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[4].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="401.698" cy="243.324" r="8.161"/>
		</g>
		<g id="erase_x5F_11_9_" class="erase_nome_cadeira" ng-click="delete_convidado(4)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 9.2824 509.5616)" fill="#E02227" cx="619.737" cy="243.576" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="613.578" y1="249.791" x2="625.896" y2="237.473"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="613.578" y1="237.473" x2="625.896" y2="249.791"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa3_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 3, cadeira_fechado: array_convidados[3].flag_fechado == 1}">
	<g id="mesa3" class="cadeira_pointer" ng-click="sel_cadeira(3)">
		<polygon fill="#E5E5E5" class="cor1" points="358.772,75.048 354.03,76.587 339.698,96.061 302.304,108.209 318.506,158.062 355.895,145.91 
			378.942,153.262 383.685,151.721 		"/>
		
			<rect x="309.43" y="100.865" transform="matrix(0.9511 -0.309 0.309 0.9511 -23.1609 107.908)" fill="#F3F3F3" class="cor2" width="39.318" height="52.418"/>
		
			<rect x="366.35" y="73.858" transform="matrix(0.9511 -0.309 0.309 0.9511 -17.2281 119.5575)" fill="#F3F3F3" class="cor2" width="4.986" height="80.623"/>
		<polygon fill="#E5E5E5" class="cor1" points="378.942,153.262 354.03,76.587 339.698,96.061 355.895,145.91 		"/>
		<g id="adicionar3" class="button_cadeira">
			<circle fill="#E02729" cx="329.447" cy="126.885" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="339.729" y1="126.889" x2="319.165" y2="126.889"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="329.446" y1="137.17" x2="329.446" y2="116.605"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M382.605,106.955c0-12.626,10.317-22.79,22.789-22.79h191.707
				c12.627,0,22.79,10.164,22.79,22.79c0,12.626-10.163,22.789-22.79,22.789H405.394
				C392.767,129.744,382.605,119.581,382.605,106.955"/>
			
				<text id="nome_x5F_3" class="text_nome_cadeira" transform="matrix(1 0 0 1 428.1608 114.4963)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[3].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="401.698" cy="106.955" r="8.161"/>
		</g>
		<g id="erase_x5F_11_8_" class="erase_nome_cadeira" ng-click="delete_convidado(3)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 105.7103 469.6199)" fill="#E02227" cx="619.737" cy="107.206" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="613.578" y1="113.421" x2="625.896" y2="101.104"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="613.578" y1="101.104" x2="625.896" y2="113.421"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa2_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 2, cadeira_fechado: array_convidados[2].flag_fechado == 1}">
	<g id="mesa2" class="cadeira_pointer" ng-click="sel_cadeira(2)">
		<polygon fill="#E5E5E5" class="cor1" points="265.805,-10.52 262.872,-6.489 262.723,17.69 239.613,49.497 282.021,80.308 305.128,48.5 
			328.095,40.9 331.026,36.868 		"/>
		
			<rect x="252.711" y="22.791" transform="matrix(0.5878 -0.809 0.809 0.5878 72.6285 240.5429)" fill="#F3F3F3" class="cor2" width="39.316" height="52.413"/>
		
			<rect x="294.451" y="-25.118" transform="matrix(0.5878 -0.809 0.809 0.5878 110.1083 246.4892)" fill="#F3F3F3" class="cor2" width="4.985" height="80.618"/>
		<polygon fill="#E5E5E5" class="cor1" points="328.095,40.9 262.872,-6.489 262.723,17.69 305.128,48.5 		"/>
		<g id="adicionar2" class="button_cadeira">
			<circle fill="#E02729" cx="272.716" cy="48.824" r="19.93"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="282.998" y1="48.826" x2="262.435" y2="48.826"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="272.714" y1="59.108" x2="272.714" y2="38.543"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M309.516,1.073c0-12.626,10.317-22.79,22.789-22.79h191.707
				c12.627,0,22.79,10.164,22.79,22.79s-10.163,22.789-22.79,22.789H332.305C319.678,23.862,309.516,13.7,309.516,1.073"/>
			
				<text id="nome_x5F_2" class="text_nome_cadeira" transform="matrix(1 0 0 1 351.7957 8.6145)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[2].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="328.61" cy="1.073" r="8.161"/>
		</g>
		<g id="erase_x5F_11_7_" class="erase_nome_cadeira" ng-click="delete_convidado(2)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 159.1729 386.9262)" fill="#E02227" cx="546.648" cy="1.324" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="540.489" y1="7.539" x2="552.807" y2="-4.778"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="540.489" y1="-4.778" x2="552.807" y2="7.539"/>
			</g>
		</g>
	</g>
</g>
<g id="mesa1_1_" class="cadeira" ng-class="{cadeira_sel: item_sel == 1, cadeira_fechado: array_convidados[1].flag_fechado == 1}">
	<g id="mesa1" class="cadeira_pointer" ng-click="sel_cadeira(1)">
		<polygon fill="#E5E5E5" class="cor1" points="140.297,-25.115 140.293,-20.129 154.386,-0.481 154.385,38.835 206.803,38.835 206.802,-0.48 
			220.916,-20.127 220.916,-25.114 		"/>
		<rect x="154.385" y="-0.48" fill="#F3F3F3" class="cor2" width="52.417" height="39.316"/>
		<rect x="140.296" y="-25.114" fill="#F3F3F3" class="cor2" width="80.619" height="4.987"/>
		<polygon fill="#E5E5E5" class="cor1" points="220.916,-20.127 140.293,-20.129 154.386,-0.481 206.802,-0.48 		"/>
		<g id="adicionar1" class="button_cadeira">
			<circle fill="#E02729" cx="180.95" cy="21.003" r="19.931"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="191.234" y1="21.005" x2="170.67" y2="21.005"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="180.948" y1="31.285" x2="180.948" y2="10.723"/>
			</g>
		</g>
	</g>
	<g class="nome_cadeira">
		<g>
			<path fill="#E6E6E6" class="fundo_nome_cadeira" stroke="#FFFFFF" stroke-miterlimit="10" d="M202.925-59.449c0,12.627-10.162,22.789-22.789,22.789H-11.572
				c-12.627,0-22.79-10.162-22.79-22.789c0-12.626,10.163-22.79,22.79-22.79h191.707C192.607-82.239,202.925-72.076,202.925-59.449"
				/>
			
				<text id="nome_x5F_1" class="text_nome_cadeira" transform="matrix(1 0 0 1 -4.5663 -51.9082)" fill="#4D4D4D" font-family="'AzoSans-Medium'" font-size="24.9763px">{{array_convidados[1].nome}}</text>
			<circle fill="#4D4D4D" class="circulo_nome_cadeira" cx="183.831" cy="-59.449" r="8.161"/>
		</g>
		<!--<g id="erase_x5F_11_4_" class="erase_nome_cadeira" ng-click="delete_convidado(1)">
			
				<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 31.8404 -41.5265)" fill="#E02227" cx="-34.207" cy="-59.198" rx="16.938" ry="16.938"/>
			<g>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-40.366" y1="-65.301" x2="-28.049" y2="-52.983"/>
				
					<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="-40.366" y1="-52.983" x2="-28.049" y2="-65.301"/>
			</g>
		</g>-->
	</g>
</g>
<path fill="#E02528" d="M294.323,176.278c0-62.832-50.988-113.696-113.695-113.696c-62.831,0-113.819,50.864-113.819,113.696
	c0,62.831,50.988,113.695,113.819,113.695C243.335,289.973,294.323,238.985,294.323,176.278z"/>
<text id="_x31_23" transform="matrix(1 0 0 1 136.1128 196.2239)" fill="#FFFFFF" font-family="'AzoSans-Medium'" font-size="49.1811px"><?php echo $mesa_sel?></text>
</svg>