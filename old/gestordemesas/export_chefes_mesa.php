<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/database.class.php");
$database = new database();

// $res = $database->query_simple_prepare("SELECT * FROM ".$database->array_tables[2]." WHERE `rest_ali_outras` IS NOT NULL AND rest_ali_outras !=  '' ORDER BY flag_registo DESC, tipo ASC",array(),"");
$res = $database->query_simple_prepare("SELECT mesas.id as id_mesa, mesas.n_mesa as n_mesa, mesas.n_lotacao as lotacao, mesas.id_responsavel as id, visitantes.nome as nome, visitantes.email as email FROM mesas, visitantes WHERE flag_bloqueado=1 AND mesas.id_responsavel = visitantes.id",array(),"");


require '../classes/PHPExcel.php';
$headings = array('Mesa','Nome Gestor', 'Email Gestor','Convidados da mesa','Total Convidados');

$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->setTitle('Listagem de Convidados por mesa');
$rowNumber = 1;

$col = 'A';
foreach($headings as $heading) {
	$objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$heading);
	$col++;
}

$rowNumber = 2;
$contador_bebidas = 0;
foreach ($res as $key => $value) {

    

	$objPHPExcel->getActiveSheet()->setCellValue("A".$rowNumber,$value['n_mesa']."(".$value['lotacao']." lugares)");
	$objPHPExcel->getActiveSheet()->setCellValue("B".$rowNumber,$value['nome']);
	$objPHPExcel->getActiveSheet()->setCellValue("C".$rowNumber,$value['email']);

	$res_convidados = $database->query_simple_prepare("SELECT mesas_visitantes.id_visitante as id, visitantes.nome as nome, visitantes.email as email FROM mesas_visitantes,visitantes WHERE mesas_visitantes.id_visitante = visitantes.id AND mesas_visitantes.id_mesa = ".$value['id_mesa'],array(),"");

	$string_convidados = "";
	$contador_conv=0;
	foreach($res_convidados as $key2 => $value2){
		$string_convidados .= "- ".$value2['nome']."(".$value2['email']."), \n";
		$contador_conv++;
	}
    $objPHPExcel->getActiveSheet()->setCellValue("D".$rowNumber,$string_convidados);

	$objPHPExcel->getActiveSheet()->setCellValue("E".$rowNumber,$contador_conv);


	$rowNumber++;
}


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: text/html; charset=iso-8859-1');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="mesas_chefes.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>