<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

$new_item = json_decode(file_get_contents("php://input"));
$new_item_convidados = (array) $new_item->lista;
$id_mesa = $new_item->mesa;
$id_chefe = $new_item->id_chefe;

$array_response = array();

$res_mesa_check = $fidelidade->query_simple_prepare("SELECT id,n_lotacao,flag_bloqueado FROM ".$fidelidade->array_tables[7]." WHERE n_mesa = ?",array($id_mesa),"i");

if($res_mesa_check[0]['flag_bloqueado'] == 0){

    
    $flag_error_convidado = 0;
    $contador_convidados = 0;
    foreach ($new_item_convidados as $key => $value) {
        if($value->id){
            $res_check_convidado = $fidelidade->query_simple_prepare("INSERT INTO ".$fidelidade->array_tables[8]."(id_mesa,id_visitante) VALUES(?,?)",array($res_mesa_check[0]['id'],$value->id),"ii");
            if($res_check_convidado == 'error'){
                $array_response['response'] = "errorconvidado";
                $array_response['id_error'] = $value->id;
                $array_response['nome_error'] = $value->nome;
                $array_response['key_error'] = $key;
                $flag_error_convidado = 1;
                break;
            }
            $contador_convidados++;
        }
    }
    
    if($flag_error_convidado ==0 ){
        if($res_mesa_check[0]['n_lotacao'] == $contador_convidados){
            $res_mesa_update = $fidelidade->query_simple_prepare("UPDATE ".$fidelidade->array_tables[7]." SET flag_bloqueado= 1, id_responsavel=? WHERE n_mesa=? AND flag_bloqueado=0 AND id_responsavel IS NULL",array($id_chefe,$id_mesa),"ii");

            if($res_mesa_update != "error"){
                foreach ($new_item_convidados as $key => $value) {
                    $res_check_convidado = $fidelidade->query_simple_prepare("UPDATE ".$fidelidade->array_tables[2]." SET mesa = ?, data_mesa=? WHERE id=?",array($id_mesa,date("Y-m-d H:i:s"),$value->id),"isi");
                }
                $array_response['response'] = "ok";
                $fidelidade->send_resumo_mesas($id_chefe);
            }
            else{
                $res_delete_convidados = $fidelidade->query_simple_prepare("DELETE FROM ".$fidelidade->array_tables[8]." WHERE id_mesa = ?",array($res_mesa_check[0]['id']),"i");
                $array_response['response'] = "errormesa";
            }
        }
        else{
            $res_delete_convidados = $fidelidade->query_simple_prepare("DELETE FROM ".$fidelidade->array_tables[8]." WHERE id_mesa = ?",array($res_mesa_check[0]['id']),"i");
            $array_response['response'] = "errorlotacao";
        }   
        
    }
    else{
        $res_delete_convidados = $fidelidade->query_simple_prepare("DELETE FROM ".$fidelidade->array_tables[8]." WHERE id_mesa = ?",array($res_mesa_check[0]['id']),"i");
    }
     

        
      
}
else{
    $array_response['response'] = "errormesa";
}


echo json_encode($array_response);
?>