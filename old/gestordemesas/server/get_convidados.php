<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

$new_item = json_decode(file_get_contents("php://input"));
$new_item_convidados = (array) $new_item->lista;
$new_item = (array) $new_item->pesquisa;

// var_dump($new_item);
$string_where = "";
$array_vars=array();
$string_vars = "";
if($new_item['nome']){
    $array_nome = explode(" ",$new_item['nome']);
    $string_where .= " AND (";
    foreach($array_nome as $key => $value){
        $string_where .= " nome LIKE ? OR";
        $array_vars[] = "%".$value."%";
        $string_vars .= "s";
    }
    $string_where = substr($string_where,0,-3);
    $string_where .= ")";
}
if($new_item['empresa']){
    $array_nome = explode(" ",$new_item['empresa']);
    $string_where .= " AND (";
    foreach($array_nome as $key => $value){
        $string_where .= " empresa LIKE ? OR";
        $array_vars[] = "%".$value."%";
        $string_vars .= "s";
    }
    $string_where = substr($string_where,0,-3);
    $string_where .= ")";
}
if($new_item['sigla']){
    $array_nome = explode(" ",$new_item['sigla']);
    $string_where .= " AND (";
    foreach($array_nome as $key => $value){
        $string_where .= " sigla LIKE ? OR";
        $array_vars[] = "%".$value."%";
        $string_vars .= "s";
    }
    $string_where = substr($string_where,0,-3);
    $string_where .= ")";
}
if($new_item['estrutura4']){
    $array_nome = explode(" ",$new_item['estrutura4']);
    $string_where .= " AND (";
    foreach($array_nome as $key => $value){
        $string_where .= " estrutura4 LIKE ? OR";
        $array_vars[] = "%".$value."%";
        $string_vars .= "s";
    }
    $string_where = substr($string_where,0,-3);
    $string_where .= ")";
}
if($new_item['estrutura5']){
    $array_nome = explode(" ",$new_item['estrutura5']);
    $string_where .= " AND (";
    foreach($array_nome as $key => $value){
        $string_where .= " estrutura5 LIKE ? OR";
        $array_vars[] = "%".$value."%";
        $string_vars .= "s";
    }
    $string_where = substr($string_where,0,-3);
    $string_where .= ")";
}
if($new_item['nome_cod80']){
    $array_nome = explode(" ",$new_item['nome_cod80']);
    $string_where .= " AND (";
    foreach($array_nome as $key => $value){
        $string_where .= " nome_cod80 LIKE ? OR";
        $array_vars[] = "%".$value."%";
        $string_vars .= "s";
    }
    $string_where = substr($string_where,0,-3);
    $string_where .= ")";
}

if( !($new_item['tipo'] == "")){
    $string_where .= " AND tipo=?";
    $array_vars[] = $new_item['tipo'];
    $string_vars .= "i";
}


$res = $fidelidade->query_simple_prepare("SELECT id,nome,tipo,empresa,sigla,estrutura4,mesa FROM ".$fidelidade->array_tables[2]." WHERE flag_registo=1 AND presenca_evento = 1 AND flag_chefe=0 AND semjantar=0".$string_where, $array_vars,$string_vars);

$array_ids_sel = array();

foreach ($new_item_convidados as $key => $value) {
    $array_ids_sel[] = $value->id;
}



foreach ($res as $key => $value) {
    if(in_array($value['id'], $array_ids_sel)){
        $res[$key]['mesa'] = 1;
    }
}

echo json_encode($res);

?>