<?php


error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

$string_error = "";
$email_post = null;
$pass_post = null;
if($_SERVER['REQUEST_METHOD'] == "POST"){
  include("../classes/fidelidade.class.php");
  $fidelidade = new fidelidade();

  $array_buttons = array();
	$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");


  $email_post = $_POST['email_input'];
  $pass_post = $_POST['pass_input'];

  $res_login = $fidelidade->login_mesas($email_post,$pass_post);
  // var_dump($res_login);
  if($res_login['response'] == $fidelidade->flag_success){
    session_save_path("session");
    session_start();
    $_SESSION['email'] = $email_post;
    $_SESSION['nome'] = $res_login['nome'];
    $_SESSION['id'] = $res_login['id'];
    
    header("Location: home.php");
  }
  else if($res_login['response'] == 1){
    $string_error = $fidelidade->return_popup_error("O email ou a password estão errados","",$array_buttons);
  }
  else if($res_login['response'] == 0){
    $string_error = $fidelidade->return_popup_error("Por favor introduza o seu email e a sua password","",$array_buttons);
  }



}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Gestor de Mesas </title>

    <!-- Bootstrap -->
    <link href="../css/main.css" rel="stylesheet">
    
    
    <!--[if lte IE 9]>
        <p class="browserupgrade" style="color:#000000;position:absolute;top:0px;left:0px;z-index:3">
          Está a usar uma versão antiga deste browser. Para poder desfrutar de todas as funcionalidades desta plataforma aconselhamos a actualizar o browser <a href="http://browsehappy.com/">aqui</a>
          </p>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <div class="background"></div>

    <div class="logo_holder">
      <a href="segundopasso.html">
        <img src="../img/logo_novo.png" alt="Pensar Maior" width="100%"/>
      </a>
    </div>

    <div class="logo_data">
      <img src="../img/data_logo_sano.png" alt="Pensar Maior" width="100%"/>
    </div>
        
    <div class="login_holder">
      <form action="" method="post" id="login_form" name="login_form">
        <p class="title_app black">GESTÃO DE MESAS</p>  
        
        <div class="form_holder">
            <p class="red">O SEU EMAIL</p>
            <input type="email" name="email_input" id="email_input" value="<?php echo $email_post?>"  />
        </div>
        <div class="form_holder">
            <p class="red">SENHA DE ACESSO</p>
            <input type="password" name="pass_input" id="pass_input" value="<?php echo $pass_post?>"  />
        </div>
        <input type="submit" value="CONTINUAR" class="button submit_class"/>
        
      </form>
    </div>

    <div class="logo_footer">
      <!--<img src="img/fidelidade_caixa_branca.png" alt="Fidelidade" class="back"/>-->
      <img src="../img/logo_fidelidade_cinza.png" alt="Fidelidade" width="100%"/>
    </div>

    <?php echo $string_error;?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/main_new.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>