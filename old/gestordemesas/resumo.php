<?php
  include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

$string_error = "";




$res_mesa_check = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE id_responsavel = ?",array($_SESSION['id']),"i");

if(count($res_mesa_check) != 1){
    header("Location: home.php");
}
else{
    $mesa_sel = $res_mesa_check[0]['n_mesa'];
    $res_convidados = $fidelidade->query_simple_prepare("SELECT ".$fidelidade->array_tables[2].".id, ".$fidelidade->array_tables[2].".nome FROM ".$fidelidade->array_tables[2]." WHERE ".$fidelidade->array_tables[2].".id IN (SELECT id_visitante FROM ".$fidelidade->array_tables[8]." WHERE id_mesa=?) AND id != ?",array($res_mesa_check[0]['id'],$_SESSION['id']),"ii");

    
}



?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Gestor de Mesas </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <style>
        .cadeira_fechado .nome_cadeira .erase_nome_cadeira{
            display:none !important;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body ng-app="mesasApp" ng-controller="mesasCtrl" class="ng-cloak">
      <div class="background"></div>
      <div class="col-md-6 col-xs-12 holder_segundo_esquerda" style="width:100%;">
          <div class="col-xs-12 pd0">
              <img src="img/logo_novo.png" alt="Pensar Maior" width="173"/>
          </div>

          <div class="col-xs-12 pd0 holder_segundo_nome">
                <p><?php echo $_SESSION['nome']?></p>
                <p class="segundo_nome_caro">MESA <span><?php echo $mesa_sel;?></span></p>
            </div>

           

          <div class="col-xs-12 col-md-6 pd0 text-center holder_terceiro_imagem">
            <div class="col-xs-12 pd0">
              <?php
                if($res_mesa_check[0]['n_lotacao'] == 12){
                  include("mesa12.php");
                }
                if($res_mesa_check[0]['n_lotacao'] == 10){
                  include("mesa10.php");
                }
              
              ?>
            </div>
          </div>
          <div style="clear:left;"></div> 
          <div style="float: left;position: relative">

                <div class="info_item black" style="width:auto;margin-top:20px;">
                        <p><span class="info_item_title">DUVIDAS</span><br />
                        <a href="mailto:info@pensarmaior.com">info@pensarmaior.com</a>
                        </p>
                    </div>
            </div>
          
      </div>


      
      
    

      
      
    
    

    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/angular.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/main.js"></script>-->
    <script>
      var mesasApp = angular.module('mesasApp',[]);


      mesasApp.controller('mesasCtrl', ['$scope','$http', function mesasCtrl($scope,$http){
        var nome_admin_mesa = "<?php echo $_SESSION['nome']?>";
        nome_admin_mesa = nome_admin_mesa.substring(0, 14);
        $scope.array_convidados = [];
        $scope.array_convidados[1] = {id: <?php echo $_SESSION['id']?>,nome: nome_admin_mesa, flag_fechado: 1};
        $scope.array_convidados[2] = {id: <?php if($res_convidados[0]['id']) echo $res_convidados[0]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[0]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[3] = {id: <?php if($res_convidados[1]['id']) echo $res_convidados[1]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[1]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[4] = {id: <?php if($res_convidados[2]['id']) echo $res_convidados[2]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[2]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[5] = {id: <?php if($res_convidados[3]['id']) echo $res_convidados[3]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[3]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[6] = {id: <?php if($res_convidados[4]['id']) echo $res_convidados[4]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[4]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[7] = {id: <?php if($res_convidados[5]['id']) echo $res_convidados[5]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[5]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[8] = {id: <?php if($res_convidados[6]['id']) echo $res_convidados[6]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[6]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[9] = {id: <?php if($res_convidados[7]['id']) echo $res_convidados[7]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[7]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[10] = {id: <?php if($res_convidados[8]['id']) echo $res_convidados[8]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[8]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[11] = {id: <?php if($res_convidados[9]['id']) echo $res_convidados[9]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[9]['nome'],0,14);?>",flag_fechado: 1};
        $scope.array_convidados[12] = {id: <?php if($res_convidados[10]['id']) echo $res_convidados[10]['id']; else echo "null";?>,nome: "<?php echo substr($res_convidados[10]['nome'],0,14);?>",flag_fechado: 1};
        
        
      }]);

      


    
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>