<?php
  include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

$string_error = "";

$res_mesa_check_user = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE id_responsavel = ?",array($_SESSION['id']),"i");

if(count($res_mesa_check_user) == 1){
    header("Location: resumo.php");
}

if (isset($_COOKIE['mesa_sel'])) {
  $mesa_sel = $_COOKIE['mesa_sel'];
}
else{
  header("Location: home.php");
}


$res_mesa_check = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE n_mesa = ?",array($mesa_sel),"i");

if($res_mesa_check[0]['flag_bloqueado'] == 1){
  header("Location: mesa.php?nmesa=".$mesa_sel);
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Gestor de Mesas </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!--[if lte IE 9]>
        <p class="browserupgrade" style="color:#000000;position:absolute;top:0px;left:0px;z-index:3">
          Está a usar uma versão antiga deste browser. Para poder desfrutar de todas as funcionalidades desta plataforma aconselhamos a actualizar o browser <a href="http://browsehappy.com/">aqui</a>
          </p>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body ng-app="mesasApp" ng-controller="mesasCtrl" class="ng-cloak">
      <div class="background"></div>
      <div class="col-md-6 col-xs-12 holder_segundo_esquerda">
          <div class="col-xs-12 pd0">
              <img src="img/logo_novo.png" alt="Pensar Maior" width="173"/>
          </div>

           <div class="col-xs-12 pd0 holder_quarto_nome">
              <p class="segundo_nome_caro">MESA <span><?php echo $mesa_sel;?></span></p>
              <p>SELECIONE OS CONVIDADOS <br> PARA A SUA MESA</p>
          </div>

          <div class="col-xs-12 pd0 text-center holder_terceiro_imagem">
            <div class="col-xs-12 pd0" >
              <?php
                if($res_mesa_check[0]['n_lotacao'] == 12){
                  include("mesa12.php");
                }
                if($res_mesa_check[0]['n_lotacao'] == 10){
                  include("mesa10.php");
                }
              
              ?>
            </div>
          </div>

          
          
      </div>


      <div class="col-xs-6 pd0 holder_popup_pessoa" ng-show="flag_prev==1">
        <div class="col-xs-12 pd0 text-center texto_acidionar_pessoa">
          <p>Tem a certeza que quer adicionar o <span class="red">{{array_convidados[item_sel].nome}}</span> a este lugar?</p>
        </div>
        <div class="col-xs-12 pd0 botoes_adicionar_pessoa">
          <div id="sim_mesa" ng-click="aceita_convidado()"><p class="button submit_class">SIM</p></div>
          <div id="nao_mesa" ng-click="recusa_convidado()"><p class="button submit_class">NÃO</p></div>
        </div>
      </div>

      <div class="col-xs-6 pd0 holder_popup_pessoa holder_submeter_mesa" ng-show="total_mesa == lotacao_mesa">
        <div class="col-xs-12 pd0 botoes_adicionar_pessoa" style="position:absolute;top:50%;margin-top:-25px;">
          <p class="button submit_class" ng-click="submit_mesa()">SUBMETER MESA</p>
        </div>
      </div>
      
    

      
      
    
    <div class=" col-md-6 col-sm-12 col-xs-12 pd0 holder_segundo_direita">
      <div class="col-sm-12 col-xs-12 pd0" style="background-color: #e6e6e6">
          <div class="col-sm-12 col-xs-12 pesquisa_quartopasso">
            PESQUISA DE CONVIDADOS
          </div>
          <div class="col-xs-12 pd0 holder_pesquisa">
            
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p><br />Tipo</p>
                    <select name="tipo_input" id="tipo_input" ng-model="pesquisa.tipo">
                      <option value="">Tipo</option>
                      <option value="0">Mediador</option>
                      <option value="1">Colaborador</option>
                      <option value="2">Consultor</option>
                      <option value="3">Convidado</option>
                    </select>
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p><br />Empresa</p>
                    <input type="text" name="empresa_input" id="empresa_input" value="" placeholder="" ng-model="pesquisa.empresa" />
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p>Direção/<br />sigla</p>
                    <input type="text" name="sigla_input" id="sigla_input" ng-model="pesquisa.sigla" value="" placeholder="" />
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p>DAM/AC/<br />DEPARTAMENTO</p>
                    <input type="text" name="est4_input" id="est4_input" ng-model="pesquisa.estrutura4" value="" placeholder="" />
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p>GESTOR DE<br />MEDIADOR</p>
                    <input type="text" name="est5_input" id="est5_input" ng-model="pesquisa.estrutura5" value="" placeholder="" />
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p>NOME<br />MEDIADOR 80</p>
                    <input type="text" name="ncod80_input" id="ncod80_input" ng-model="pesquisa.nome_cod80" value="" placeholder="" />
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                    <p>NOME<br />PARTICIPANTE</p>
                    <input type="text" name="nome_input" id="nome_input" ng-model="pesquisa.nome" value="" placeholder="" />
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-lg-3 col-md-4">
                <div class="form_holder">
                  <p>&nbsp;<br />&nbsp;</p>
                  <input type="submit" ng-click="pesquisa_func()" value="PESQUISAR" style="margin-top:0px;" class="button submit_class" id="pesquisar_button" placeholder="" />
                </div>
                  
                  <!--<div class="botao_pesquisar">
                    <p class="button submit_class">pesquisar</p>
                  </div>  -->
              </div>


              </div>
      <div class="col-xs-12 pd0 ng-cloak">
        <div class="table-responsive ng-cloak hidden_load" ng-class="{hidden_load_o: convidados}">
          <table class="table" style="margin-bottom:0px;">
           <tr style="background-color: #e6e6e6" class="titulo_tabela">
              <th>NOME</th>
              <th>TIPO</th> 
              <th>EMPRESA</th>
              <th>DIREÇÃO/SIGLA</th>
              <th>DAM/AC/DEPARTAMENTO</th>
              <th></th>
          </tr>
            <tr ng-repeat="convidado in convidados" class="item_convidado" ng-click="sel_convidado(convidado)" ng-class="{item_odd: $odd, item_even: $even}">
              <td class="nome_tabela">{{convidado.nome}}</td>
              <td class="email_tabela">
                <span ng-if="convidado.tipo == 0">Mediador</span>
                <span ng-if="convidado.tipo == 1">Colaborador</span>
                <span ng-if="convidado.tipo == 2">Consultor</span>
                <span ng-if="convidado.tipo == 3">Convidado</span>
              </td> 
              <td class="empresa_tabela">{{convidado.empresa}}</td>
              <td class="empresa_tabela">{{convidado.sigla}}</td>
              <td class="empresa_tabela">{{convidado.estrutura4}}</td>
              <td ng-class="{pessoa_adicionada: convidado.mesa, pessoa_tabela: !convidado.mesa}"><img ng-if="!convidado.mesa" src="img/adicionar.svg" alt="" width="35px"></td>
            </tr>
            <!--<tr >
              <td class="nome_tabela">Celso</td>
              <td class="email_tabela">celso@spic.pt</td> 
              <td class="empresa_tabela">Spic</td>
              <td class="empresa_tabela">Loulé</td>
              <td class="pessoa_tabela"><img src="img/adicionar.svg" alt="" width="35px"></td>
            </tr>-->
             
          </table>
        </div>
      </div>
    </div>

    <div class='popup_error' ng-class="{visible: flag_popup==1}">
      <div class='popup_back'></div>
      <div class='popup_holder'>
        <p class='error_msg red'>{{msg_red}}</p>
        <div class='button_holder'>
          <div class='button' id='voltar' ng-click="flag_popup=0"><p>VOLTAR</p></div>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/angular.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/main.js"></script>-->
    <script>
      var mesasApp = angular.module('mesasApp',[]);


      mesasApp.controller('mesasCtrl', ['$scope','$http', function mesasCtrl($scope,$http){
        var nome_admin_mesa = "<?php echo $_SESSION['nome']?>";
        nome_admin_mesa = nome_admin_mesa.substring(0, 14);
        $scope.array_convidados = [];
        $scope.array_convidados[1] = {id: <?php echo $_SESSION['id']?>,nome: nome_admin_mesa, flag_fechado: 1};
        $scope.array_convidados[2] = {id: null,nome: "..."};
        $scope.array_convidados[3] = {id: null,nome: "..."};
        $scope.array_convidados[4] = {id: null,nome: "..."};
        $scope.array_convidados[5] = {id: null,nome: "..."};
        $scope.array_convidados[6] = {id: null,nome: "..."};
        $scope.array_convidados[7] = {id: null,nome: "..."};
        $scope.array_convidados[8] = {id: null,nome: "..."};
        $scope.array_convidados[9] = {id: null,nome: "..."};
        $scope.array_convidados[10] = {id: null,nome: "..."};
        $scope.array_convidados[11] = {id: null,nome: "..."};
        $scope.array_convidados[12] = {id: null,nome: "..."};
        $scope.item_sel = null;
        $scope.sel_cadeira = function(index){
          if( $scope.array_convidados[index].flag_fechado != 1){

            if($scope.item_sel){
              if($scope.array_convidados[$scope.item_sel].id && $scope.array_convidados[$scope.item_sel].fechado != 1){
                $scope.total_mesa++;
                $scope.delete_convidado($scope.item_sel);
              }
            }
            

            $scope.item_sel = index;
            $scope.flag_prev =0;
          }
          
        }

        $scope.pesquisa = {nome: "",tipo: "", empresa: "",sigla: "",estrutura4: "",estrutura5:"",nome_cod80: ""};

        $scope.convidados = [];

        $scope.lotacao_mesa = <?php echo $res_mesa_check[0]['n_lotacao'];?>;
        $scope.total_mesa = 1;
        
        $scope.flag_prev = 0;

        $scope.mesa_sel = <?php echo $mesa_sel;?>;
        
        $scope.pesquisa_func = function(){
          $("#pesquisar_button").val("Carregando...");
          $http.post('server/get_convidados.php?data='+(Math.random()),{"pesquisa": $scope.pesquisa,"lista":$scope.array_convidados},{ cache: false}).then(function(data) {
            console.log(data);
            $scope.convidados = data.data;
            $("#pesquisar_button").val("PESQUISAR");
          },function errorCallback(response) {
            console.log("error");
            $("#pesquisar_button").val("PESQUISAR");
          });
        }

        $scope.id_sel = null;
        $scope.sel_convidado = function(convidado){
          if($scope.item_sel){
            if(!convidado.mesa){
              $scope.array_convidados[$scope.item_sel].id = convidado.id;
              $scope.array_convidados[$scope.item_sel].nome = convidado.nome.substring(0, 14);
              $scope.array_convidados[$scope.item_sel].index_sel = $scope.convidados.indexOf(convidado);
              $scope.flag_prev = 1;
              $scope.id_sel = $scope.convidados.indexOf(convidado);
            }
            else{
              $scope.msg_red = "Lamentamos, mas este convidado já foi seleccionado. Por favor, escolha uma alternativa.";
              $scope.flag_popup = 1;
            }
          }
          else{
              $scope.msg_red = "Por favor selecione uma cadeira primeiro.";
              $scope.flag_popup = 1;
            }
        }

        $scope.aceita_convidado = function(){
          $scope.array_convidados[$scope.item_sel].flag_fechado = 1;
          $scope.flag_prev = 0;
          $scope.item_sel = null;
          $scope.convidados[$scope.id_sel].mesa = $scope.mesa_sel;
          $scope.total_mesa++;
        }

        $scope.recusa_convidado = function(){
          $scope.flag_prev = 0;
          $scope.array_convidados[$scope.item_sel].id = null;
          $scope.array_convidados[$scope.item_sel].nome = "...";
        }

        $scope.delete_convidado = function(index){
            $scope.array_convidados[index].id = null;
            $scope.array_convidados[index].nome = "...";
            $scope.array_convidados[index].flag_fechado = null;
            $scope.array_convidados[index].mesa = $scope.mesa_sel;
            $scope.convidados[$scope.array_convidados[index].index_sel].mesa = null;
            $scope.total_mesa--;
        }

        $scope.flag_popup = 0;
        $scope.submit_mesa = function(){
          $http.post('server/save_mesa.php?data='+(Math.random()),{"lista":$scope.array_convidados,"mesa":$scope.mesa_sel,"id_chefe":<?php echo $_SESSION['id']; ?>},{ cache: false}).then(function(data) {
            console.log(data);
            var response = data.data.response;
            if(response == "errorlotacao"){
              $scope.msg_red = "A mesa só pode ser submetida após estar completa";
              $scope.flag_popup = 1;
            }
            else if(response == "errormesa"){
              window.location.assign("<?php echo $fidelidade->pathgeral?>mesa.php?nmesa=<?php echo $mesa_sel; ?>");
            }
            else if(response == "errorconvidado"){
              $scope.msg_red = "Lamentamos, mas o convidado "+data.data.nome_error+" já foi seleccionado. Por favor, escolha uma alternativa.";
              $scope.flag_popup = 1;
              $scope.delete_convidado(data.data.key_error);
            }
            else if(response == "ok"){
              window.location.assign("<?php echo $fidelidade->pathgeral?>resumo.php");
            }
            else{
              $scope.msg_red = "Lamentamos, mas ocurreu um erro. Tente de novo.";
              $scope.flag_popup = 1;
            }
            


          },function errorCallback(response) {
            console.log("error");
            $scope.msg_red = "Lamentamos, mas ocurreu um erro. Tente de novo.";
            $scope.flag_popup = 1;
          });
        }
        
      }]);

      


    
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>