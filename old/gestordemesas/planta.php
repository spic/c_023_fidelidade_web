<?php
//   include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

// $res_mesa_check_user = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE id_responsavel = ?",array($_SESSION['id']),"i");

// if(count($res_mesa_check_user) == 1){
//     header("Location: resumo.php");
// }

?>
<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
<div class="holder_planta">
    
    <div class="holder_slots" style="float:left">
        <?php
            $res_mesas = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7],array(),"");

            foreach ($res_mesas as $key => $value) {
                if($value['n_mesa'] == 1){
                    echo "<div class='coluna_slots coluna_slot_tipo1'>";
                }
                if($value['n_mesa'] == 109){
                    echo "<div class='coluna_slots coluna_slot_tipo2'>";
                }

                if($value['n_mesa'] == 169){
                    echo "<div class='coluna_slots coluna_slot_tipo3'>";
                }

                if($value['n_mesa'] == 1){
                    echo "<div id='slot1' class='holder_mesas'>";
                    echo "<p class='title_holder_mesas'>01-36</p>";
                } 
                if($value['n_mesa'] == 37){
                  echo "<div id='slot2' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>37-72</p>";
                } 

                if($value['n_mesa'] == 73){
                  echo "<div id='slot3' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>73-108</p>";
                } 

                if($value['n_mesa'] == 109){
                  echo "<div id='slot4' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>109-136</p>";
                } 

                if($value['n_mesa'] == 139){
                  echo "<div id='slot5' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>139-168</p>";
                } 

                if($value['n_mesa'] == 169){
                  echo "<div id='slot6' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>169-204</p>";
                } 

                if($value['n_mesa'] == 205){
                  echo "<div id='slot7' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>205-241</p>";
                } 

                if($value['n_mesa'] == 242){
                  echo "<div id='slot8' class='holder_mesas'>";
                  echo "<p class='title_holder_mesas'>242-277</p>";
                } 

                $string_check_sel = "";

                if($value['flag_bloqueado'] == 1){
                    $string_sel = "mesa_holder_sel";
                    $link = "";
                    $link2 = "";
                }
                 else{
                    $string_sel = "";
                    $link = "<a href='mesa.php?nmesa=".$value['n_mesa']."'>";
                    $link2 = "</a>";

                    if($mesa_sel == $value['n_mesa'])
                        $string_check_sel = "mesa_holder_check_sel";
                 }   

                 
                    

                if(($value['n_mesa'] > 241 && $value['n_mesa'] <= 277)){
                    if(($value['n_mesa']-2) %6 == 0){
                        echo "<div class='holder_coluna_mesas'>";
                    }
                    echo $link."<div class='mesa_holder red_table ".$string_sel." ".$string_check_sel."' id='".$value['n_mesa']."'>
                        <p class='number_table'>".sprintf("%02d", $value['n_mesa'])."</p>
                        <div class='mesa_taken'></div>
                    </div>".$link2;
                    if(($value['n_mesa']-1) %6 == 0){
                        echo "</div>";
                    }
                }
                
                if($value['n_mesa'] <= 36 || ($value['n_mesa'] > 72 && $value['n_mesa'] <= 108) || ($value['n_mesa'] > 168 && $value['n_mesa'] <= 204)){
                    if(($value['n_mesa']-1) %6 == 0){
                        echo "<div class='holder_coluna_mesas'>";
                    }
                    echo $link."<div class='mesa_holder red_table ".$string_sel." ".$string_check_sel."' id='".$value['n_mesa']."'>
                        <p class='number_table'>".sprintf("%02d", $value['n_mesa'])."</p>
                        <div class='mesa_taken'></div>
                    </div>".$link2;
                    if($value['n_mesa'] %6 == 0){
                        echo "</div>";
                    }
                }

                if(($value['n_mesa'] > 36 && $value['n_mesa'] <= 72) || ($value['n_mesa'] > 204 && $value['n_mesa'] <= 241)){
                    if(($value['n_mesa']-1) %6 == 0){
                        echo "<div class='holder_coluna_mesas'>";
                    }
                    echo $link."<div class='mesa_holder white_table ".$string_sel." ".$string_check_sel."' id='".$value['n_mesa']."'>
                        <p class='number_table'>".sprintf("%02d", $value['n_mesa'])."</p>
                        <div class='mesa_taken'></div>
                    </div>".$link2;
                    if($value['n_mesa'] %6 == 0){
                        echo "</div>";
                    }
                    if($value['n_mesa'] == 241){
                        echo "</div>";
                    }
                }

                if(($value['n_mesa'] > 108 && $value['n_mesa'] <= 138) || ($value['n_mesa'] > 138 && $value['n_mesa'] <= 168) ){
                    if(($value['n_mesa']+1) %5 == 0){
                        echo "<div class='holder_coluna_mesas'>";
                    }
                    echo $link."<div class='mesa_holder white_table ".$string_sel." ".$string_check_sel."' id='".$value['n_mesa']."'>
                        <p class='number_table'>".sprintf("%02d", $value['n_mesa'])."</p>
                        <div class='mesa_taken'></div>
                    </div>".$link2;
                    if(($value['n_mesa']+2) %5 == 0){
                        echo "</div>";
                    }
                }
                

                if($value['n_mesa'] == 36 || $value['n_mesa'] == 72 || $value['n_mesa'] == 108 || $value['n_mesa'] == 138 || $value['n_mesa'] == 168 || $value['n_mesa'] == 204 || $value['n_mesa'] == 241 || $value['n_mesa'] == 277 ){
                  echo "</div>";
                }

                if($value['n_mesa'] == 138 ){
                    echo "<div class='holder_mesas holder_mesas_img '><img src='img/palco.jpg' /></div>";
                }
                // if($value['n_mesa'] == 42 ){
                //     echo "</div>";
                // }

                if($value['n_mesa'] == 108 || $value['n_mesa'] == 168 || $value['n_mesa'] == 277){
                    echo "</div>";
                }
            }

        ?>

    </div>
    <div class="holder_cocktail" style="float:left;max-width:200px">
        <img src="img/cocktail.svg" />
    </div>
</div>