<?php
  include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

$res_mesa_check_user = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE id_responsavel = ?",array($_SESSION['id']),"i");

if(count($res_mesa_check_user) == 1){
    header("Location: resumo.php");
}

$string_error = "";

$mesa_sel = $_GET['nmesa'];

$res_mesa_check = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE n_mesa = ?",array($mesa_sel),"i");

if($res_mesa_check[0]['flag_bloqueado'] == 1){
  $array_buttons = array();
	$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");
  $string_error = $fidelidade->return_popup_error("Lamentamos, mas esta mesa já foi seleccionada.<br />Por favor, escolha uma alternativa.","",$array_buttons);
  $mesa_sel = null;
}
else{
  unset($_COOKIE['mesa_sel']);
  $cookie_name = "mesa_sel";
  $cookie_value = $mesa_sel;
  setcookie($cookie_name, $cookie_value, time()+((3600 * 24)*30), "/");  
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Gestor de Mesas </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!--[if lte IE 9]>
        <p class="browserupgrade" style="color:#000000;position:absolute;top:0px;left:0px;z-index:3">
          Está a usar uma versão antiga deste browser. Para poder desfrutar de todas as funcionalidades desta plataforma aconselhamos a actualizar o browser <a href="http://browsehappy.com/">aqui</a>
          </p>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <div class="background"></div>

    <div class="col-md-6 col-xs-12 holder_segundo_esquerda">
      <div class="col-xs-12 pd0">
          <img src="img/logo_novo.png" alt="Pensar Maior" width="173"/>
      </div>

      <div class="col-xs-12 pd0 holder_segundo_nome">
          <p><?php echo $_SESSION['nome']?></p>
      </div>

      <div class="col-xs-12 pd0 holder_terceiro_nome">
          <p class="terceiro_nome_caro">SELECIONOU A</p>
          <p class="terceiro_mesa"> MESA <span class="red"><?php echo $mesa_sel?></span></p>
          <?php if($mesa_sel){ ?>
          <p class="terceiro_capacidade red">Capacidade para <?php echo $res_mesa_check[0]['n_lotacao']?> pessoas</p>
          <?php } ?>
      </div>

      <div class="col-xs-12 pd0 text-center holder_terceiro_imagem">
          <div class="col-xs-12 pd0">
            <?php if($res_mesa_check[0]['n_lotacao'] == 12){?>
              <img src="img/mesa.svg" alt="" width="50%">
            <?}?>
            <?php if($res_mesa_check[0]['n_lotacao'] == 10){?>
              <img src="img/mesa10.svg" alt="" width="50%">
            <?}?>
          </div>
          <div class="col-xs-12 pd0 numero_mesa_terceiro">
            <?php echo $mesa_sel?>
          </div>
        
      </div>  
      
    </div>

    <?php if($mesa_sel){ ?>
      <div class="col-xs-6 pd0 holder_popup_pessoa">
        <div class="col-xs-12 pd0 text-center texto_acidionar_pessoa">
          <p>Tem a certeza que quer Selecionar esta mesa?</p>
          <p>Esta opção é irreversivel</p>
        </div>
        <div class="col-xs-12 pd0 botoes_adicionar_pessoa">
          <div id="sim_mesa"><a href="convidados.php"><p class="button submit_class">SIM</p></a></div>
          <div id="nao_mesa"><p class="button submit_class">NÃO</p></div>
        </div>
      </div>
      <?php } ?>
    
    <!--<div class="col-sm-6 col-xs-12 pd0 holder_segundo_esquerda">
      <div class="logo_holder">
        <a href="quartopasso.html">
          <img src="img/logo_novo.png" alt="Pensar Maior" width="75%"/>
        </a>
      </div>

      <div class="col-xs-12 pd0 holder_segundo_nome">
          <p class="segundo_nome_caro">Caro</p>
          <p>João Palhinha</p>
      </div>-->

      
      

    <!--</div>-->
    <div class="col-md-6 col-xs-12 pd0 holder_segundo_direita">
      <?php include "planta.php";?>
    </div>

    <?php echo $string_error?>    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="../js/main_new.js"></script>

    <script>
      $(document).ready(function(){
        $("#nao_mesa").click(function(){
          $(".holder_popup_pessoa").css("display","none");
        });
      });
    </script>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
    <!--<script src="js/main.js"></script>-->
  </body>
</html>