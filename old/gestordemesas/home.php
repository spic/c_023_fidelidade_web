<?php
  include "session_control.php";

  error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("../classes/fidelidade.class.php");
$fidelidade = new fidelidade();

$res_mesa_check_user = $fidelidade->query_simple_prepare("SELECT * FROM ".$fidelidade->array_tables[7]." WHERE id_responsavel = ?",array($_SESSION['id']),"i");

if(count($res_mesa_check_user) == 1){
    header("Location: resumo.php");
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pensar Maior Fidelidade - Gestor de Mesas </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!--[if lte IE 9]>
        <p class="browserupgrade" style="color:#000000;position:absolute;top:0px;left:0px;z-index:3">
          Está a usar uma versão antiga deste browser. Para poder desfrutar de todas as funcionalidades desta plataforma aconselhamos a actualizar o browser <a href="http://browsehappy.com/">aqui</a>
          </p>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <div class="background"></div>
    
    <div class="col-md-6 col-xs-12 holder_segundo_esquerda">
      <div class="col-xs-12 pd0">
          <img src="img/logo_novo.png" alt="Pensar Maior" width="173"/>
      </div>
      <div class="col-xs-12 pd0 holder_segundo_nome">
          <p><?php echo $_SESSION['nome']?></p>
      </div>
      
        <p class="titulo_segundo_mesa">
          Faça da sua, a melhor mesa:<br /><br />
          <span class="red">1º Passo - </span> Selecione a sua mesa<br />
          <span class="red">2º Passo - </span> Selecione os convidados<br />
          <span class="red">3º Passo - </span> Submeta a sua mesa

          <br /><br />A mesa e convidados selecionados só ficarão confirmados após carregar no botão “SUBMETER”.
          Para submeter a sua mesa terá de selecionar o nº máximo de convidados da mesa escolhida.

          <br /><br />
          <span class="frase_baixo_segundo">
            Tem que submeter a sua mesa
          até ao dia <span class="red">DIA  23 DE FEVEREIRO, ÀS 13h00.</span>
          </span>
        </p>
        

        <p class="button submit_class" id="inicio" style="margin-top:50px;">SELECIONAR MESA</p>
      
      <div class="col-xs-12 logo_baixo_segundo">
        <img src="img/logo_grande_fidelidade.png" alt="" width="200px">
      </div>
    </div>

    <div class="col-md-6 col-xs-12 pd0 holder_segundo_direita">
      
      <?php include "planta.php";?>

      
      
    </div>
    <div class="fundocinza"></div>

    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script>
      $(document).ready(function(){
        $("#inicio").click(function(){
          $(".fundocinza").css("display","none");
          $("#inicio").css("display","none");
        });
      });
    </script>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
    
  </body>
</html>