<?php
    header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");

    error_reporting(E_ALL|E_STRICT);
    ini_set("display_errors","off");
    ini_set('error_log','my_file.log');

    require("classes/fidelidade.class.php");
	$fidelidade = new fidelidade();


    $string_error = "";

    $email_get = $_GET['email'];
    if($email_get == ""){
        header("Location: index.php");
    }
    else{
        $res_dados = $fidelidade->get_email_dados($email_get);
        $fidelidade->error_report($res_dados);
        $fidelidade->error_report("dados");
        if($res_dados == 'error'){
            header("Location: index.php");
        }
        if($res_dados['flag_registo'] == 1 && $res_dados['presenca_evento'] == 1){
            header("Location: resumo.php?email=".$email_get);
        }
        
    }

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $res_dados = $fidelidade->check_post_data($_POST,$res_dados);
        

        $array_response = $fidelidade->registo($res_dados);
        $fidelidade->error_report($array_response);
        if($array_response['state'] == "error" ){
            $string_error = $array_response['text'];
        }
        else if($array_response['state'] == "ok"){
            header("Location: resumo.php?email=".$res_dados['email']);
        }
    }
    

?>
<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 9]>
			<script>
				window.location.assign("<?php echo $fidelidade->pathgeral?>simple");
			</script>
        <![endif]-->
        <div class="background"></div>

		<div class="logo_holder">
			<img src="img/logo_novo.png" alt="Pensar Maior" width="100%"/>
		</div>

		<div class="logo_data">
			<img src="img/data_logo_sano.png" alt="Pensar Maior" width="100%"/>
		</div>

        <div class="container_form">
            <form action="" method="post" id="registo">
                <p class="bigtitle black"><?php echo $res_dados['nome']?></p>
              
                <p class="text black">No dia <strong>25 de Fevereiro</strong> esperamos por si para o encontro <strong>Pensar Maior.</strong><br />
              9h30 – 19h30 – Reunião Pensar Maior – Meo Arena<br />
              19h30 – 24h00 – Cocktail, jantar e espetáculo – Pavilhão 1 FIL</p>

                <p class="title red" style="margin-top:56px">CONFIRME A SUA PRESENÇA</p>	
                    
                <div class="form_holder">
                    <p>NOME</p>
                    <p style="font-family:'azo_sanslight';margin-bottom:0px;text-transform:none"><?php echo $res_dados['nome']?></p>
                </div>
                <div class="form_holder">
                    <p>E-MAIL <?php if($res_dados['tipo'] == 0 || $res_dados['tipo'] == 2 || $res_dados['tipo'] == 3) echo "<span style='text-transform:none'>(Atualize se necessário)</span>" ?></p>
                    <?php
                        if($res_dados['tipo'] == 0 || $res_dados['tipo'] == 2 || $res_dados['tipo'] == 3){
                    ?>
                    <input type="text" name="email_input" id="email_input" value="<?php echo $res_dados['email']?>" required placeholder="Email" />
                    <?php
                        }
                    ?>
                    <?php
                        if($res_dados['tipo'] == 1){
                    ?>
                    <p style="font-family:'azo_sanslight';margin-bottom:0px;text-transform:none"><?php echo $res_dados['email']?></p>
                    <?php
                        }
                    ?>
                </div>

                <div style="clear:left;"></div>
                
                <p class="text black" style="margin-top:50px;">O nº de telemóvel será o seu código de entrada no evento,</p>
                <p class="title red">INSIRA AQUI O SEU Nº DE TELEMÓVEL:</p>	
                <div class="form_holder" style="margin-top:5px;">
                    <input type="text" name="tel_input" id="tel_input" maxlength="9"  value="<?php echo $res_dados['n_telemovel']?>" placeholder="Telemovel" />
                    <input type="hidden" id="flag_sem_tel" name="flag_sem_tel" value="<?php echo $res_dados['flag_sem_tel']?>" />
                </div>

                <div style="clear:left;"></div>

                <p class="title red" style="margin-top:50px;">RESTRIÇÕES ALIMENTARES:</p>	
                <div class="form_holder lista" style="margin-top:5px;">
                    <ul>
                        <li>
                            <input type="checkbox" id="sem" value="0" name="restricoes[]" <?php if(strpos($res_dados['restricoes_ali'], '0') !== false) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="sem" class="black">SEM RESTRIÇÕES</label>   
                        </li>
                        <li>
                            <input type="checkbox" id="veg" value="1" name="restricoes[]" <?php if(strpos($res_dados['restricoes_ali'], '1') !== false) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="veg" class="black">VEGETARIANO</label>   
                        </li>
                        <li>
                            <input type="checkbox" id="cel" value="2" name="restricoes[]" <?php if(strpos($res_dados['restricoes_ali'], '2') !== false) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="cel" class="black">CELÍACO</label>   
                        </li>
                        <li>
                            <input type="checkbox" id="outras" value="3" name="restricoes[]" <?php if(strpos($res_dados['restricoes_ali'], '3') !== false) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="outras" class="black">OUTRAS</label>   
                            <input type="text" name="outras_rest" id="outras_rest" <?php if(strpos($res_dados['restricoes_ali'], '3') === false) echo "class='hidden'"?> value="<?php echo $res_dados['rest_ali_outras']?>"  placeholder="OUTRAS" />
                        </li>
                    </ul>
                </div>

                <div style="clear:left;"></div>
                
                <?php
                    if($res_dados['transfer_flag'] == 1){
                ?>
                <p class="title red" style="margin-top:50px;">PRETENDE TRANSPORTE ATÉ LISBOA (Meo Arena)?</p>	
                <div class="form_holder lista" style="margin-top:5px;">
                    <ul>
                        <li>
                            <input type="radio" id="sem_transfer" value="0" name="transfer[]" <?php if($res_dados['transfer'] == 0 || !$res_dados['transfer']) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="sem_transfer" class="black">NÃO</label>   
                        </li>
                        <li>
                            <input type="radio" id="com_transfer" value="1" name="transfer[]" <?php if($res_dados['transfer'] == 1) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="com_transfer" class="black">SIM</label>   
                        </li>
                    </ul>
                </div>

                <div class="form_holder hidden" id="ponto_partida_holder">
                    <p>PONTO DE PARTIDA</p>
                    <select id="ponto_partida" name="ponto_partida">
                        <option value="">PONTO DE PARTIDA</option>
                        <?php
                            
                            $array_select = array();
                            foreach ($fidelidade->array_transfers['24'] as $key => $value) {
                                $array_select[$fidelidade->generateFriendlyName($value,200)] = $value;
                            }
                            foreach ($fidelidade->array_transfers['25'] as $key => $value) {
                                $array_select[$fidelidade->generateFriendlyName($value,200)] = $value;
                            }
                            
                            ksort($array_select);

                            foreach ($array_select as $key => $value) {
                                echo '<option value="'.$value.'"'; 
                                if($res_dados['ponto_partida'] == $value) echo 'selected="selected"';
                                echo '>'.$value.'</option>';
                            }

                        
                        ?>
                    </select>
                </div>
                <input type="hidden" id="flag_transfer" name="flag_transfer" value="<?php echo $res_dados['flag_transfer']?>" />
                <?php
                    if(($res_dados['aloja_24'] == 0 && $res_dados['aloja_25'] == 0)){
                ?>
                <div style="clear:left;"></div>
                <p class="text black" style="margin-top:25px;">Caso tenha selecionado transporte até Lisboa (Meo Arena) brevemente irá receber mais informações para o email indicado acima</p>
                
                <?php
                    }
                ?>
                <?php
                    }
                ?>
                <?php
                    if(!($res_dados['aloja_24'] == 0 && $res_dados['aloja_25'] == 0)){
                ?>
                <div style="clear:left;"></div>

                <p class="title red" style="margin-top:50px;">ALOJAMENTO</p>	
                <div class="form_holder lista" style="margin-top:5px;">
                    <p>PRETENDE ALOJAMENTO?</p>
                    <ul>
                        <li>
                            <input type="radio" id="sem_aloj" value="0" name="alojamento[]" <?php if($res_dados['alojamento'] == 0 || !$res_dados['alojamento']) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="sem_aloj" class="black">NÃO</label>   
                        </li>
                        <li>
                            <input type="radio" id="uma_noite" value="1" name="alojamento[]" <?php if($res_dados['alojamento'] == 1) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="uma_noite" class="black">SIM, DIA 25</label>   
                        </li>
                        <?php
                            if($res_dados['aloja_24'] == 1 && $res_dados['aloja_25'] == 1){
                        ?>
                        <li>
                            <input type="radio" id="duas_noite" value="2" name="alojamento[]" <?php if($res_dados['alojamento'] == 2) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="duas_noite" class="black">SIM, DIA 24 E DIA 25</label>   
                        </li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
                <div style="clear: left;"></div>
                <!--<p class="text black" style="margin-top:5px;">Mais tarde irá receber no email indicado em cima informações sobre o hotel que lhe foi atribuído e sobre o transfer.</p>-->
                <p class="text black" style="margin-top:5px;">Caso tenha selecionado alojamento ou transporte até Lisboa (Meo Arena) brevemente irá receber mais informações para o email indicado acima</p>

                

                <div style="clear:left;"></div>

                <div class="form_holder hidden" id="cc_form">
                    <p>CARTÃO DE CIDADÃO</p>
                    <input type="text" name="cc_input" maxlength="8" id="cc_input" toaki value="<?php echo $res_dados['cc']?>" placeholder="Cartão de cidadão" />
                </div>

                <div style="clear:left;"></div>

                <div class="form_holder hidden" id="cc_validade_form">
                    <p>VALIDADE CARTÃO DE CIDADÃO (dd/mm/aaaa)</p>
                    <input type="date" name="validade_cc_input" id="validade_cc_input" value="<?php echo $res_dados['cc_validade']?>" placeholder="dd/mm/aaaa" />
                </div>
                <?php
                    }
                ?>

                <div style="clear:left;"></div>

                <div class="form_holder lista" style="margin-top:25px;">
                    <ul>
                        <li>
                            <input type="checkbox" id="sem_jantar" value="1" name="sem_jantar[]" <?php if($res_dados['jantar'] == 1) echo 'checked="checked"' ?> />
                            <div class="check"></div>
                            <label for="sem_jantar" class="text red">Caso não tenha possibilidade de ir ao 
jantar e ao espetáculo, por favor clique aqui</label>   
                        </li>
                    </ul>
                </div>
                
                <div style="clear:left"></div>

                <input type="submit" value="CONFIRMAR PRESENÇA" id="submit_registo" class="button submit_class"/>
            </form>
        </div>
        
		

		<div class="logo_footer">
			<!--<img src="img/fidelidade_caixa_branca.png" alt="Fidelidade" class="back"/>-->
			<img src="img/logo_fidelidade_cinza.png" alt="Fidelidade" width="100%"/>
		</div>

		<?php echo $string_error; ?>


        <?php echo $fidelidade->return_popup_error("","",array( 0 =>array("id" => "voltar", "text"=>"VOLTAR")),"transfer",1)?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main_new.js"></script>
        <script src="js/form_new.js"></script>

        <script>
            var array_transfer = <?php echo json_encode($fidelidade->array_transfers);?>;
        </script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>