<?php
	error_reporting(E_ALL|E_STRICT);
	ini_set("display_errors","off");
	ini_set('error_log','my_file.log');

	require("classes/fidelidade.class.php");
	$fidelidade = new fidelidade();
	
	

	$string_error = "";
	$email_get = $_GET['email'];
	if($email_get != ""){
		if($fidelidade->check_email($email_get) == 1){
			header("Location: form.php?email=".$email_get);
		}
		
	}

	if($_SERVER['REQUEST_METHOD'] == "POST"){
		$email_get = $_POST['email_input'];

		$array_buttons = array();
		$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");

		if($email_get != ""){
			if($fidelidade->check_email($email_get) == 1){
				header("Location: form.php?email=".$email_get);
			}
			else{
				$string_error = $fidelidade->return_popup_error("O email inserido não consta na base de dados","",$array_buttons);
			}
		}
		else{
			$string_error = $fidelidade->return_popup_error("Por favor preencha o email","",$array_buttons);
		}
	}

?>

<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 9]>
			<script>
				window.location.assign("<?php echo $fidelidade->pathgeral?>simple?email=<?php echo $email_get?>");
			</script>
        <![endif]-->
		<div class="background"></div>

		<div class="logo_holder">
			<img src="img/logo_novo.png" alt="Pensar Maior" width="100%"/>
		</div>

		<div class="logo_data">
			<img src="img/data_logo_sano.png" alt="Pensar Maior" width="100%"/>
		</div>
        
		<div class="login_holder">
			<form action="" method="post" id="login_form" name="login_form">
				<p class="title red">O SEU EMAIL</p>	
				<p class="text black">Insira aqui o seu email para aceder ao seu perfil.</p>	
				<div class="form_holder">
					<input type="email" name="email_input" id="email_input" value="<?php echo $email_get?>" required placeholder="Email" />
				</div>
				<input type="submit" value="CONTINUAR" class="button submit_class"/>
				
			</form>
		</div>

		<div class="logo_footer">
			<!--<img src="img/fidelidade_caixa_branca.png" alt="Fidelidade" class="back"/>-->
			<img src="img/logo_fidelidade_cinza.png" alt="Fidelidade" width="100%"/>
		</div>

		<?php echo $string_error; ?>



        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main_new.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>