<?php
$name='AzoSans-Regular';
$type='TTF';
$desc=array (
  'CapHeight' => 354.0,
  'XHeight' => 240.0,
  'FontBBox' => '[-44 -243 1001 1011]',
  'Flags' => 4,
  'Ascent' => 1011.0,
  'Descent' => -243.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 365.0,
);
$unitsPerEm=2048;
$up=-344;
$ut=50;
$strp=295;
$strs=50;
$ttffile='/Applications/MAMP/htdocs/spic/fidelidade/mpdf60/ttfonts/rui_abreu_-_azosans-regular-webfont.ttf';
$TTCfontID='0';
$originalsize=50212;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='azo';
$panose=' 0 0 2 0 0 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 772, -228, 0
// usWinAscent/usWinDescent = 1011, -292
// hhea Ascent/Descent/LineGap = 1011, -292, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>