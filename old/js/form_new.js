$(document).ready(function(){

    $("#com_transfer").change(function(){
        if($("#com_transfer").prop("checked")){
            $("#ponto_partida_holder").removeClass('hidden');
        }
        else{
            $("#ponto_partida_holder").addClass('hidden');
        }
    });

    $("#sem_transfer").change(function(){
        if($("#sem_transfer").prop("checked")){
            $("#ponto_partida_holder").addClass('hidden');
        }
    });

    $("#uma_noite").change(function(){
        if($(this).prop("checked")){
            $("#cc_form").removeClass("hidden");
            $("#cc_validade_form").removeClass("hidden");
        }
    });
    $("#duas_noite").change(function(){
        if($(this).prop("checked")){
            $("#cc_form").removeClass("hidden");
            $("#cc_validade_form").removeClass("hidden");
        }
    });
    $("#sem_aloj").change(function(){
        if($(this).prop("checked")){
            $("#cc_form").addClass("hidden");
            $("#cc_validade_form").addClass("hidden");
        }
    });

    if($("#duas_noite").prop("checked") || $("#uma_noite").prop("checked")){
        $("#cc_form").removeClass("hidden");
        $("#cc_validade_form").removeClass("hidden");
    }

    if($("#com_transfer").prop("checked")){
        $("#ponto_partida_holder").removeClass('hidden');
    }

    $("#submit_form").click(function(){
        $("#flag_sem_tel").val(1);
        $("#submit_registo").click();
    });

    $("#submit_form_transfer").click(function(){
        $("#flag_transfer").val(1);
        $("#submit_registo").click();
    });

    $("#ponto_partida").change(function(){
        var local_transfer = $("#ponto_partida").val();
        console.log(array_transfer[24].indexOf(local_transfer));
        if(local_transfer != ""){
            if(array_transfer[24].indexOf(local_transfer) != -1){
                $("#transfer .error_msg.red").html("Para o ponto de partida selecionado o transfer será dia 24 ao final do dia.");
            }
            if(array_transfer[25].indexOf(local_transfer) != -1){
                $("#transfer .error_msg.red").html("Para o ponto de partida selecionado o transfer será dia 25.");
            }
            $("#transfer").addClass("visible");        
        }
        

        
    });

    $("#outras").change(function(){
        
        if($("#outras").is(':checked')){
            $("#outras_rest").removeClass('hidden');
        }
        else{
            $("#outras_rest").addClass('hidden');
        }
    });

});