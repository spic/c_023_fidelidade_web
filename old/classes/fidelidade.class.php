<?php

spl_autoload_register(function ($class) {
    include $class . '.class.php';
});

class fidelidade extends database{

	
	
	public $array_transfers;

    /*CONTRUCTOR*/
	function __construct() {
        parent::__construct();

        /*definir a zona horaria*/
        date_default_timezone_set('GMT');

		$this->array_transfers = array();
		$this->array_transfers['24'] = array("Amarante","Braga","Bragança","Guimarães","Lamego","Maia","Mirandela","Paredes","V.N. Famalicão","Vila Real");
		$this->array_transfers['25'] = array("Abrantes","Albufeira","Aveiro","Beja","Coimbra","Covilhã","Évora","Grândola","Leiria","Oliveira Hospital","Porto","Sta. Maria Feira","Viseu");
   }


    function check_email($email)
	{
		$string_sql = "SELECT id FROM ".$this->array_tables[2]." WHERE email = ?";
		$res_email = $this->query_simple_prepare($string_sql,array($email),"s");
		if($res_email != 'error')
			return count($res_email);
		else 
			return 0;
	}

    function get_email_dados($email)
	{
		$string_sql = "SELECT * FROM ".$this->array_tables[2]." WHERE email = ?";
		$res_email = $this->query_simple_prepare($string_sql,array($email),"s");
		if($res_email != 'error' && count($res_email) == 1)
            return $res_email[0];
		else 
			return 'error';
	}

	function check_post_data($post_data,$bd_dados){
		$array_return = array();
		$array_return['nome'] = $post_data['nome_input'];
		if(!$array_return['nome']) $array_return['nome'] = $bd_dados['nome'];
		$array_return['email'] = $post_data['email_input'];
		if(!$array_return['email']) $array_return['email'] = $bd_dados['email'];
		$array_return['n_telemovel'] = $post_data['tel_input'];
		$array_return['flag_sem_tel'] = $post_data['flag_sem_tel'];
		$array_return['restricoes_ali'] = "";
		foreach ($post_data['restricoes'] as $key => $value) {
			$array_return['restricoes_ali'] .= $value;
		}
		if($array_return['restricoes_ali'] == "") $array_return['restricoes_ali'] = 0;
		$array_return['rest_ali_outras'] = $post_data['outras_rest'];
		if(strpos($array_return['restricoes_ali'], '3') === false) $array_return['rest_ali_outras'] = "";
		$array_return['transfer'] = $post_data['transfer'][0];
		if(!$array_return['transfer']) $array_return['transfer'] = 0;
		$array_return['flag_transfer'] = $post_data['flag_transfer'];
		$array_return['ponto_partida'] = $post_data['ponto_partida'];
		if($array_return['transfer'] == 0) $array_return['ponto_partida'] = "";
		$array_return['alojamento'] = $post_data['alojamento'][0];
		if(!$array_return['alojamento']) $array_return['alojamento'] = 0;
		$array_return['cc'] = $post_data['cc_input'];
		$array_return['cc_validade'] = $post_data['validade_cc_input'];
		$array_return['jantar'] = $post_data['sem_jantar'][0];
		if(!$array_return['jantar']) $array_return['jantar'] = 0;
		$array_return['aloja_24'] = $bd_dados['aloja_24'];
		$array_return['aloja_25'] = $bd_dados['aloja_25'];
		$array_return['transfer_flag'] = $bd_dados['transfer_flag'];
		$array_return['id'] = $bd_dados['id'];
		return $array_return;
	}

	function registo($dados)
	{	
		$array_ccvalidade_2 = explode("-",$dados['cc_validade']);

		if(count($array_ccvalidade_2) == 3 ){
			$dados['cc_validade'] = $array_ccvalidade_2[2]."/".$array_ccvalidade_2[1]."/".$array_ccvalidade_2[0];
		}

		$array_ccvalidade = explode("/",$dados['cc_validade']);

		$array_return = array();
		if($dados['jantar'] == 1 && ($dados['alojamento'] != 0 || $dados['transfer'] != 0)){
			$array_return['state'] = "error";
			$title = "Lamentamos informar, mas opções de alojamento e transfer estão apenas disponíveis para convidados que participam no jantar.";
			$subtitle = "Se pretende ativar estas opções, por favor, 
altere a sua resposta. Obrigado";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);

		}
		else if(!$dados['n_telemovel'] && $dados['flag_sem_tel'] == 0){
			$array_return['state'] = "error";
			$title = "Por favor, introduza o número do seu telemóvel.<br />
              Esta informação destina-se apenas a facilitar a entrada e envio de informações referentes a este evento";
			$subtitle = "O acesso ao evento é possível sem indicação deste dado, mas será previsivelmente mais demorado";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"Inserir nº de telemóvel");
			$array_buttons[1] = array("id" => "submit_form", "text"=>"Avançar sem nº de telemóvel");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if( !(is_numeric($dados['n_telemovel']) && strlen($dados['n_telemovel']) == 9 && substr($dados['n_telemovel'],0,1) == 9) && $dados['n_telemovel'] != "" ){
			$array_return['state'] = "error";
			$title = "O formato do número de telemóvel está errado.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if($dados['alojamento'] != 0 && (!$dados['cc'] || !$dados['cc_validade'])){
			$array_return['state'] = "error";
			$title = "Se pretende alojamento, tem de indicar o numero de cartão de cidadão e a sua validade.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if($dados['transfer'] == 1 && $dados['ponto_partida'] == ""){
			$array_return['state'] = "error";
			$title = "Por favor seleccione um ponto de partida para o transfer.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"Voltar");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if( in_array($dados['ponto_partida'], $this->array_transfers['24']) && $dados['alojamento'] == 1 && $dados['flag_transfer'] == 0){ 
			$array_return['state'] = "error";
			$title = "A data de saída do transfer selecionado é incongruente com a(s) data(s) de alojamento. Verifique por favor as suas opções.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"Voltar");
			$array_buttons[1] = array("id" => "submit_form_transfer", "text"=>"Avançar mesmo assim");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if( in_array($dados['ponto_partida'], $this->array_transfers['25']) && $dados['alojamento'] == 2  && $dados['flag_transfer'] == 0){ 
			$array_return['state'] = "error";
			$title = "A data de saída do transfer selecionado é incongruente com a(s) data(s) de alojamento. Verifique por favor as suas opções.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"Voltar");
			$array_buttons[1] = array("id" => "submit_form_transfer", "text"=>"Avançar mesmo assim");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if (!(is_numeric($dados['cc']) && strlen($dados['cc']) == 8 ) && $dados['alojamento'] != 0) {
			$array_return['state'] = "error";
			$title = "O Número de Cartão de Cidadão deve conter 8 dígitos.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"Voltar");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else if(  !(count($array_ccvalidade) == 3 && strlen($array_ccvalidade[2])==4 && strlen($array_ccvalidade[1])==2 && strlen($array_ccvalidade[0])==2 && is_numeric($array_ccvalidade[2]) && is_numeric($array_ccvalidade[1]) && is_numeric($array_ccvalidade[0]) && $array_ccvalidade[1] <= 12 && $array_ccvalidade[1] >= 1 && $array_ccvalidade[0] <= 31 && $array_ccvalidade[0] >= 1 && ($array_ccvalidade[2]>2017 || ($array_ccvalidade[2]==2017 && $array_ccvalidade[1]>2 ) || ($array_ccvalidade[2]==2017 && $array_ccvalidade[1]==2 && $array_ccvalidade[0]>=25  )  )) && $dados['alojamento'] != 0 ){
			$array_return['state'] = "error";
			$title = "A validade do seu Cartão de Cidadão está incorrecta ou expira antes do dia do evento.";
			$subtitle = "";
			$array_buttons = array();
			$array_buttons[0] = array("id" => "voltar", "text"=>"Voltar");
			$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
		}
		else{
			$code = null;
			if($dados['n_telemovel'] == ""){
				$code = $this->generate_random_code(9);
				$dados['n_telemovel'] = null;
			}
			

			$string_update = "UPDATE ".$this->array_tables[2]." SET nome=?, email=?, n_telemovel=?, cod_acesso=?, restricoes_ali=?, rest_ali_outras=?, transfer=?, ponto_partida=?, alojamento=?,cc=?,cc_validade=?, semjantar=?, presenca_evento=1, flag_registo=1, data_registo=? WHERE id=?";
			$array_vars = array($dados['nome'],$dados['email'],$dados['n_telemovel'],$code,$dados['restricoes_ali'],$dados['rest_ali_outras'],$dados['transfer'],$dados['ponto_partida'],$dados['alojamento'],$dados['cc'],$dados['cc_validade'], $dados['jantar'], date("Y-m-d H:i:s"), $dados['id']);
			$string_var = "ssssisisissisi";
			$res_update = $this->query_simple_prepare_report($string_update,$array_vars,$string_var);
			if($res_update['status'] != "error"){
				if($code) $codigo = $code;
				else if($dados['n_telemovel']) $codigo = $dados['n_telemovel'];
				$res_save_code = $this->query_simple_prepare("INSERT INTO ".$this->array_tables[3]."(code) VALUES(?)",array($codigo),"s");
				
				$array_return['state'] = "ok";

				$mail_class = new mail();
				$mail_class->send_email($dados['email'],"info@pensarmaior.com",'Fidelidade',"info@pensarmaior.com",utf8_decode($this->return_body($dados,1)),"Registo no Evento Pensar Maior 2017 ",2);
					
			}
			else{
				$array_return['state'] = "error";
				

				if (strpos($res_update['error'], 'Duplicate entry') !== false && strpos($res_update['error'], 'key \'email\'') !== false) {
					$title = "O email que introduziu já existe na base de dados.";
				}
				else if(strpos($res_update['error'], 'Duplicate entry') !== false && strpos($res_update['error'], 'key \'n_telemovel\'') !== false){
					$title = "O telemovel que introduziu já existe na base de dados.";
				}
				else{
					$title = "Ocurreu um erro ao guardar o seu registo. Tente de novo.";
					$this->error_report("AQUI UM ERRO DE INSERAO NA BD. FEEDBACK AO CLIENTE DE ERRO PARA O EMAIL:".$dados['email']);
				}
				
				$array_buttons = array();
				$array_buttons[0] = array("id" => "voltar", "text"=>"VOLTAR");
				$array_return['text'] = $this->return_popup_error($title,$subtitle,$array_buttons);
				
			}
		}
		return $array_return;
	}

	function generate_random_code($length)
	{
		$res_code = $this->query_simple_prepare("SELECT code FROM ".$this->array_tables[3]);

		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		if (in_array($randomString,$res_code, TRUE)){
			$randomString = $this->generate_random_code(9);	
		}
		
		return $randomString;
	}

	function generate_random_pass($length)
	{
		

		$characters = '0123456789abcdefghijklmnopqrstuvxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		
		
		return $randomString;
	}

	

	function return_popup_error($title, $subtitle = null, $array_buttons, $id_popup = null, $invisible = null )
	{
		if($id_popup){
			$string_id = 'id="'.$id_popup.'"';
		}
		else{
			$string_id = '';
		}

		if($invisible){
			$string_invisible = "";
		}
		else{
			$string_invisible = "visible";
		}

		$string_popup = "";
		$string_popup .= "<div class='popup_error ".$string_invisible."' ".$string_id.">";
		$string_popup .= "<div class='popup_back'></div>";
		$string_popup .= "<div class='popup_holder'>";
		$string_popup .= "<p class='error_msg red'>".$title."</p>";
		if($subtitle != null && $subtitle != ""){
			$string_popup .= "<p class='error_msg black'>".$subtitle."</p>";
		}

		foreach ($array_buttons as $key => $value) {
			$string_popup .= "<div class='button_holder'>";
			$string_popup .= "<div class='button' id='".$value['id']."'>";
			$string_popup .= "<p>".$value['text']."</p>";
			$string_popup .= "</div>";
			$string_popup .= "</div>";
		}
		
		$string_popup .= "</div>";
		$string_popup .= "</div>";

		return $string_popup;
	}

	function return_body($dados,$flag = 0)
	{
		// VARS EMAIL
		if($flag == 1){
			$url_img = $this->pathgeral;
			$font_title = "24px";
			$font_items = "14px";
			$font_subtitle = "17px";
			$font_links = "14px";
			$width_img = "715";
			$width_img_metade = "357";
		}
		else{
			$url_img = "";
			$font_title = "24px";
			$font_items = "11px";
			$font_subtitle = "18px";
			$font_links = "10px";
			$width_img = "100%";
			$width_img_metade = "50%";
		}

		$string_html = "";
		$string_html .= '<img src="'.$url_img.'img/newsletters_topo.jpg" width="'.$width_img.'"/>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_title.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:20px;margin-top:20px;">Registo efetuado com sucesso.</p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Nome: &nbsp;&nbsp;<span style="font-weight:normal">'.$dados['nome'].'</span></p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Email: &nbsp;&nbsp;<span style="font-weight:normal">'.$dados['email'].'</span></p>';

		if($dados['n_telemovel']){
			$cod = $dados['n_telemovel'];
		}
		else{
			$cod = $dados['cod_acesso'];
		}
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Telemóvel / Código de acesso: &nbsp;&nbsp;<span style="font-weight:normal">'.$cod.'</span></p>';

		$rest = "";
		if(strpos($dados['restricoes_ali'], '0') !== false)
			$rest .= "Sem restrições, ";
		if(strpos($dados['restricoes_ali'], '1') !== false)
			$rest .= "Vegetariano, ";
		if(strpos($dados['restricoes_ali'], '2') !== false)
			$rest .= "Celíaco, ";
		if(strpos($dados['restricoes_ali'], '3') !== false)
			$rest .= "Outros - ".$dados['rest_ali_outras'].", ";
		$rest = substr($rest, 0, -2);

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Restrições alimentares: &nbsp;&nbsp;<span style="font-weight:normal">'.$rest.'</span></p>';

		 switch ($dados['transfer']) {
			case '0':
				$transfer = "Não";
				break;
			case '1':
				$transfer = $dados['ponto_partida'];
				break;
		}        

		if( in_array($dados['ponto_partida'], $this->array_transfers['25']) ){
			$transfer .= " - Dia 25 de Fevereiro";
		}
		if( in_array($dados['ponto_partida'], $this->array_transfers['24']) ){
			$transfer .= " - Dia 24 de Fevereiro";
		}      
		
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Transporte até Lisboa (Meo Arena): &nbsp;&nbsp;<span style="font-weight:normal">'.$transfer.'</span></p>';

		switch ($dados['alojamento']) {
			case '0':
				$aloja = "Não";
				break;
			case '1':
				$aloja = "Dia 25";
				break;
			case '2':
				$aloja = "Dia 24 e Dia 25";
				break;
		}

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Alojamento: &nbsp;&nbsp;<span style="font-weight:normal">'.$aloja.'</span></p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_subtitle.';font-weight: bold;color: #000000;margin:0px;margin-bottom:20px;margin-top:40px;">Dia 25 de Fevereiro às 09h30 no Meo Arena</p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:10px;margin-top:0px;"><a href="https://goo.gl/maps/YwrXvQPfuXL2" target="_blank" style="display:inline;text-decoration:none;">Ver localização do Evento</a></p>';

		if($flag == 1){
			$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:10px;margin-top:0px;"><a href="http://www.pensarmaior.com/resume_pdf.php?email='.$dados['email'].'"  target="_blank" style="display:inline;text-decoration:none;">Download PDF</a></p>';
		}

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">Dúvidas:</p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;"><a href="mailto:info@pensarmaior.com" target="_blank" style="display:inline;text-decoration:none;">info@pensarmaior.com</a></p>';
		
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;text-align:left">
			<img style="float:left;" src="'.$url_img.'img/newsletters_footer_app_r1_c1.jpg" width="'.$width_img.'"/>
			<div style="clear:left;"></div>
			<a href="https://itunes.apple.com/us/app/pensar-maior-fidelidade/id1203139703?l=pt&ls=1&mt=8" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c1.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			<a href="https://play.google.com/store/apps/details?id=com.spic.fidelidade&hl=pt_PT" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c2.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			</p>';

		
		


		return $string_html;
	}


	function login_mesas($email, $password){
		if($email != "" && $password != ""){
			$result = array();
			$password_encript = md5($email.$password);
			$res = $this->query_simple_prepare("SELECT id, nome FROM ".$this->array_tables[2]." WHERE password_chefe=? AND email = ? AND flag_chefe = 1 AND flag_registo=1",array($password_encript,$email),"ss");
			if(count($res) == 1 && $res != 'error'){
				$result['response'] = $this->flag_success;
				$result['id'] = $res[0]['id'];
				$result['nome'] = $res[0]['nome'];
			}
			else{
				$result['response'] = 1;
			}
		}
		else{
			$result['response'] = 0;
		}

		return $result;
	}

	function send_resumo_mesas($id_chefe){
		
		$url_img = "http://pensarmaior.com/";
		$font_title = "24px";
		$font_items = "14px";
		$font_subtitle = "17px";
		$font_links = "14px";
		$width_img = "715";
		$width_img_metade = "357";
		
		$res_mesa_check = $this->query_simple_prepare("SELECT * FROM ".$this->array_tables[7]." WHERE id_responsavel = ?",array($id_chefe),"i");

		$res_convidados = $this->query_simple_prepare("SELECT ".$this->array_tables[2].".id, ".$this->array_tables[2].".nome, ".$this->array_tables[2].".email FROM ".$this->array_tables[2]." WHERE ".$this->array_tables[2].".id IN (SELECT id_visitante FROM ".$this->array_tables[8]." WHERE id_mesa=?)",array($res_mesa_check[0]['id']),"i");

		$string_html = "";
		$string_html .= '<img src="'.$url_img.'img/newsletters_topo.jpg" width="'.$width_img.'"/>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_title.';font-weight: bold;color: #000000;margin:0px;margin-bottom:20px;margin-top:20px;"><span style="color:#E02428">Mesa '.$res_mesa_check[0]['n_mesa'].'</span> submetida com sucesso.</p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Convidados associados a esta mesa:</p>';

		$email = "";
		foreach ($res_convidados as $key => $value) {
			if($value['id'] == $id_chefe)
				$email = $value['email'];
			$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">'.$value['nome'].': &nbsp;&nbsp;<span style="font-weight:normal">'.$value['email'].'</span></p>';	
		}
		
		

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_subtitle.';font-weight: bold;color: #000000;margin:0px;margin-bottom:20px;margin-top:40px;">Dia 25 de Fevereiro às 09h30 no Meo Arena</p>';


		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">Para mais informações por favor contacte:</p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;"><a href="mailto:info@pensarmaior.com" target="_blank" style="display:inline;text-decoration:none;">info@pensarmaior.com</a></p>';
		
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;text-align:left">
			<img style="float:left;" src="'.$url_img.'img/newsletters_footer_app_r1_c1.jpg" width="'.$width_img.'"/>
			<div style="clear:left;"></div>
			<a href="https://itunes.apple.com/us/app/pensar-maior-fidelidade/id1203139703?l=pt&ls=1&mt=8" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c1.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			<a href="https://play.google.com/store/apps/details?id=com.spic.fidelidade&hl=pt_PT" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c2.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			</p>';

		
		
			$mail_class = new mail();
			$mail_class->send_email($email,"info@pensarmaior.com",'Fidelidade',"info@pensarmaior.com",utf8_decode($string_html),"Resumo de mesa para o Evento Pensar Maior 2017",2);

		

		// return $result;
	}

	function send_convite_chefe($email,$password){
		
		$url_img = "http://pensarmaior.com/";
		$font_title = "24px";
		$font_items = "14px";
		$font_subtitle = "17px";
		$font_links = "14px";
		$width_img = "715";
		$width_img_metade = "357";
		
		

		$string_html = "";
		$string_html .= '<img src="'.$url_img.'img/newsletters_topo.jpg" width="'.$width_img.'"/>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_title.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:20px;margin-top:20px;"><span style="color:#E02428">FAÇA A SUA MESA</p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Foi selecionada/o para ser Gestor de Mesa no jantar do Pensar Maior.<br />Como tal, até dia 23 de Fevereiro, às 13h00, deverá escolher a mesa e as pessoas que a/o vão acompanhar durante o jantar e espetáculo do evento.</p>';
		

		$string_html .= '<br /><p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">O desafio está lançado. Quanto mais rápido for, melhor localizada será a sua mesa.</p>';

		$string_html .= '<br /><p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">O processo é simples.<br /><span style="color:#E02428">Clique no link e faça login com os dados de acesso indicados em baixo, depois:</span></p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:0px">1º Passo: &nbsp;&nbsp;<span style="font-weight:normal;color:#000000">Selecione a sua mesa. Há mesas de 10 e de 12 lugares.</span></p>';	
		
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:0px">2º Passo: &nbsp;&nbsp;<span style="font-weight:normal;color:#000000">Selecione os convidados.<br />Se um convidado que selecionou entretanto deixar de estar disponivel é porque outro Gestor de mesa foi mais rápido e o convidou;<br />A mesa só pode ser submetida depois de totalmente preenchida.</span></p>';	

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:10px">3º Passo: &nbsp;&nbsp;<span style="font-weight:normal;color:#000000">Submeta a sua mesa.<br />Vai receber no seu e-mail um resumo da sua mesa.</span></p>';	
		
		$string_html .= '<br /><p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Se no dia 23, às 13h00, a mesa não tiver sido feita, então os convidados ainda disponiveis serão distribuidos de forma aleatória na sua mesa.</p>';


		$string_html .= '<br /><p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:30px">Obrigado pela sua colaboração e por contribuir para que este evento seja um sucesso!</p>';
		

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_subtitle.';font-weight: bold;color: #000000;margin:0px;margin-bottom:20px;margin-top:40px;">Dia 25 de Fevereiro às 09h30 no Meo Arena</p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:0px">Faça aqui a sua mesa:</p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;"><a href="http://www.pensarmaior.com/gestordemesas" target="_blank" style="display:inline;text-decoration:none;">http://www.pensarmaior.com/gestordemesas</a></p>';

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: bold;color: #E02428;margin:0px;margin-bottom:0px">Dados de acesso:</p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Email: &nbsp;&nbsp;<span style="font-weight:normal">'.$email.'</span></p>';	
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px">Password: &nbsp;&nbsp;<span style="font-weight:normal">'.$password.'</span></p>';	

		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">Para mais informações por favor contacte:</p>';
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;"><a href="mailto:info@pensarmaior.com" target="_blank" style="display:inline;text-decoration:none;">info@pensarmaior.com</a></p>';
		
		$string_html .= '<p style="font-family: \'azo\',arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;text-align:left">
			<img style="float:left;" src="'.$url_img.'img/newsletters_footer_app_r1_c1.jpg" width="'.$width_img.'"/>
			<div style="clear:left;"></div>
			<a href="https://itunes.apple.com/us/app/pensar-maior-fidelidade/id1203139703?l=pt&ls=1&mt=8" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c1.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			<a href="https://play.google.com/store/apps/details?id=com.spic.fidelidade&hl=pt_PT" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c2.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			</p>';

		
		
			$mail_class = new mail();
			$mail_class->send_email($email,"info@pensarmaior.com",'Fidelidade',"info@pensarmaior.com",utf8_decode($string_html),"Faça a sua Mesa",2);
			$mail_class->send_email("joao@spic.pt","info@pensarmaior.com",'Fidelidade',"info@pensarmaior.com",utf8_decode($string_html),"Faça a sua Mesa",2);

		

		// return $result;
	}
	
	function send_mesa_convidado($email,$mesa,$gestor){
		
		$url_img = "http://pensarmaior.com/";
		$font_title = "20px";
		$font_items = "14px";
		$font_subtitle = "17px";
		$font_links = "14px";
		$width_img = "715";
		$width_img_metade = "357";
		
		

		$string_html = "";
		$string_html .= '<img src="'.$url_img.'img/newsletters_topo.jpg" width="'.$width_img.'"/>';
		$string_html .= '<p style="font-family: arial;font-size: '.$font_title.';font-weight: bold;color: #000000;margin:0px;margin-bottom:20px;margin-top:20px;">No dia 25 de fevereiro, após um dia de partilha de conhecimento, vamos ter o nosso jantar Pensar Maior, seguido de espetáculo.<br /><br />
		<span style="font-weight: normal;font-size: '.$font_items.'">Este momento será de convívio e descontração para todos, mas sobretudo uma oportunidade para reforçar laços, num ambiente mais informal.</span></p>';

		$string_html .= '<p style="font-family: arial;font-size: '.$font_subtitle.';font-weight: bold;color: #000000;margin:0px;margin-bottom:10px"><span style="color:#E02428">'.$gestor.'</span> convidou-o para ficar na sua mesa que terá o número <span style="color:#E02428">'.$mesa.'.</span></p>';
		

		$string_html .= '<br /><p style="font-family: arial;font-size: '.$font_items.';font-weight: normal;color: #000000;margin:0px;margin-bottom:0px">A logística de um jantar sentado, para cerca de 3200 pessoas, nunca é fácil, mas quisemos aproveitar ao máximo esta possibilidade.<br />Para isso <span style="font-weight:bold;">selecionámos Gestores de Mesa</span>, entre os Responsáveis do Grupo Fidelidade e os Representantes da Área Comercial que constituíram as suas mesas, considerando o  objetivo maior deste jantar: partilha de ideias, reforço de laços, motivação.</p>';

		$string_html .= '<br /><p style="font-family: arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">Esperamos que aproveite da melhor forma este momento do Pensar Maior.<br />Se todos nos empenharmos e contribuirmos, este jantar Pensar Maior será um sucesso!</p>';
		
		$string_html .= '<br /><p style="font-family: arial;font-size: '.$font_items.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">Aceda aqui à planta da sala de jantar: <a href="http://www.pensarmaior.com/planta.jpg" target="_blank" style="display:inline;text-decoration:none;">http://www.pensarmaior.com/planta.jpg</a></p>';

		

		$string_html .= '<p style="font-family: arial;font-size: '.$font_subtitle.';font-weight: bold;color: #000000;margin:0px;margin-bottom:20px;margin-top:40px;">Dia 25 de Fevereiro às 09h30 no Meo Arena</p>';


		$string_html .= '<p style="font-family: arial;font-size: '.$font_links.';font-weight: bold;color: #000000;margin:0px;margin-bottom:0px">Para mais informações por favor contacte:</p>';
		$string_html .= '<p style="font-family: arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;"><a href="mailto:info@pensarmaior.com" target="_blank" style="display:inline;text-decoration:none;">info@pensarmaior.com</a></p>';
		
		$string_html .= '<p style="font-family: arial;font-size: '.$font_links.';font-weight: normal;color: #4E47FF;margin:0px;margin-bottom:30px;margin-top:0px;text-align:left">
			<img style="float:left;" src="'.$url_img.'img/newsletters_footer_app_r1_c1.jpg" width="'.$width_img.'"/>
			<div style="clear:left;"></div>
			<a href="https://itunes.apple.com/us/app/pensar-maior-fidelidade/id1203139703?l=pt&ls=1&mt=8" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c1.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			<a href="https://play.google.com/store/apps/details?id=com.spic.fidelidade&hl=pt_PT" target="_blank"><img src="'.$url_img.'img/newsletters_footer_app_r2_c2.jpg" style="float:left;" width="'.($width_img_metade).'"/></a>
			</p>';

		
		
			$mail_class = new mail();
			$mail_class->send_email($email,"info@pensarmaior.com",'Fidelidade',"info@pensarmaior.com",utf8_decode($string_html),"Mesa para Jantar no Pensar Maior 2017",2);
			$mail_class->send_email("joao@spic.pt","info@pensarmaior.com",'Fidelidade',"info@pensarmaior.com",utf8_decode($string_html),"Mesa para Jantar no Pensar Maior 2017",2);

		

		// return $result;
	}
	
}
?>