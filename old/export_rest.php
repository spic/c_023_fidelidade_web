<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

include("classes/database.class.php");
$database = new database();

// $res = $database->query_simple_prepare("SELECT * FROM ".$database->array_tables[2]." WHERE `rest_ali_outras` IS NOT NULL AND rest_ali_outras !=  '' ORDER BY flag_registo DESC, tipo ASC",array(),"");
$res = $database->query_simple_prepare("SELECT nome, email, mesa FROM ".$database->array_tables[2]." WHERE mesa IS NOT NULL AND flag_registo = 1 AND semjantar = 1 ",array(),"");


require 'classes/PHPExcel.php';
$headings = array('Nome', 'Email','Mesa');

$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->setTitle('Listagem de Convidados');
$rowNumber = 1;

$col = 'A';
foreach($headings as $heading) {
	$objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$heading);
	$col++;
}

$rowNumber = 2;
$contador_bebidas = 0;
foreach ($res as $key => $value) {

    

	$objPHPExcel->getActiveSheet()->setCellValue("A".$rowNumber,$value['nome']);
	$objPHPExcel->getActiveSheet()->setCellValue("B".$rowNumber,$value['email']);
    $objPHPExcel->getActiveSheet()->setCellValue("C".$rowNumber,$value['mesa']);
	

	$rowNumber++;
}


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: text/html; charset=iso-8859-1');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Convidados.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>