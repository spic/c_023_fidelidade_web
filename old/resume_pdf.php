<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');


    require("classes/fidelidade.class.php");
	$fidelidade = new fidelidade();


    $string_error = "";

    $email_get = $_GET['email'];
    if($email_get == ""){
        header("Location: index.php");
    }
    else{
        $res_dados = $fidelidade->get_email_dados($email_get);
        if($res_dados == 'error'){
            header("Location: index.php");
        }
        if($res_dados['flag_registo'] == 0){
            header("Location: form.php?email=".$email_get);
        }
    }

$html = $fidelidade->return_body($res_dados);



//==============================================================
//==============================================================
//==============================================================

include("mpdf60/mpdf.php");

$mpdfObj = new mPDF('', '', 'azo');
$mpdfObj->SetFont('azo');
// $mpdf=new mPDF('c'); 
// $mpdf->AddFont('azo_sansbold', '', 'azo_bold.php');

$mpdfObj->SetTitle( "Resumo Registo - ".$res_dados['nome'] );
$mpdfObj->WriteHTML($html,0);
$mpdfObj->Output('registo_pensar_maior.pdf','I');
exit;

//==============================================================
//==============================================================
//==============================================================


?>