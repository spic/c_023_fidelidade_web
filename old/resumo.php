<?php

    error_reporting(E_ALL|E_STRICT);
    ini_set("display_errors","off");
    ini_set('error_log','my_file.log');

    require("classes/fidelidade.class.php");
	$fidelidade = new fidelidade();


    $string_error = "";

    $email_get = $_GET['email'];
    if($email_get == ""){
        header("Location: index.php");
    }
    else{
        $res_dados = $fidelidade->get_email_dados($email_get);
        if($res_dados == 'error'){
            header("Location: index.php");
        }
        if($res_dados['flag_registo'] == 0){
            header("Location: form.php?email=".$email_get);
        }
        if($res_dados['flag_registo'] == 1 && $res_dados['presenca_evento'] == 0){
            header("Location: naovai.php?email=".$email_get);
        }
    }

    

?>
<!doctype html>
<html class="no-js" lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fidelidade - Pensar Maior</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 9]>
			<script>
				window.location.assign("<?php echo $fidelidade->pathgeral?>simple");
			</script>
        <![endif]-->
        
        <div class="background"></div>

		<div class="logo_holder">
			<img src="img/logo_novo.png" alt="Pensar Maior" width="100%"/>
		</div>

		<div class="logo_data">
			<img src="img/data_logo_sano.png" alt="Pensar Maior" width="100%"/>
		</div>

        <div class="container_form">
            <p class="verybigtitle red">Registo efetuado com sucesso.</p>
            <p class="text black">Vai receber este resumo no seu email.</p>

            <div class="holder_dados">
                <div class="holder_coluna holder_coluna_esq">
                    <div class="info_item black">
                        <p><span class="info_item_title">NOME</span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $res_dados['nome']?></p>
                    </div>
                    <div class="info_item black">
                        <p><span class="info_item_title">EMAIL</span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $res_dados['email']?></p>
                    </div>
                    <div class="info_item black">
                        <p><span class="info_item_title">TELEMOVEL/</span></p>
                        <p><span class="info_item_title">CÓDIGO DE ACESSO</span>&nbsp;&nbsp;&nbsp;&nbsp;<?php if($res_dados['n_telemovel']) echo $res_dados['n_telemovel']; else echo $res_dados['cod_acesso'];?></p>
                    </div>
                </div>

                <div class="holder_coluna holder_coluna_dir">
                    <div class="info_item black">
                        <p><span class="info_item_title">RESTRIÇÕES ALIMENTARES</span><br />
                            <?php 
                                $rest = "";
                                if(strpos($res_dados['restricoes_ali'], '0') !== false)
                                    $rest .= "Sem restrições, ";
                                if(strpos($res_dados['restricoes_ali'], '1') !== false)
                                    $rest .= "Vegeteriano, ";
                                if(strpos($res_dados['restricoes_ali'], '2') !== false)
                                    $rest .= "Celíaco, ";
                                if(strpos($res_dados['restricoes_ali'], '3') !== false)
                                    $rest .= "Outros - ".$res_dados['rest_ali_outras'].", ";
                                $rest = substr($rest, 0, -2);
                                echo $rest;
                            ?>
                        </p>
                    </div>
                    <div class="info_item black">
                        <p><span class="info_item_title">TRANSPORTE ATÉ LISBOA (Meo Arena)</span><br />
                            <?php 
                                switch ($res_dados['transfer']) {
                                    case '0':
                                        echo "Não";
                                        break;
                                    case '1':{
                                        echo $res_dados['ponto_partida'];
                                        if( in_array($res_dados['ponto_partida'], $fidelidade->array_transfers['25']) ){
                                            echo " - Dia 25 de Fevereiro";
                                        }
                                        if( in_array($res_dados['ponto_partida'], $fidelidade->array_transfers['24']) ){
                                            echo " - Dia 24 de Fevereiro";
                                        }
                                        break;
                                    }
                                        
                                }
                            ?>
                        </p>
                    </div>
                    <div class="info_item black">
                        <p><span class="info_item_title">ALOJAMENTO</span><br />
                            <?php 
                                switch ($res_dados['alojamento']) {
                                    case '0':
                                        echo "Não";
                                        break;
                                    case '1':
                                        echo "Dia 25";
                                        break;
                                    case '2':
                                        echo "Dia 24 e Dia 25";
                                        break;
                                }
                            ?>
                        </p>
                    </div>
                </div>
            </div>

            <div style="clear:left;"></div>

            <p class="verybigtitleunderline black">Dia 25 de Fevereiro às 09h30 no Meo Arena </p>
            
            <div style="clear:left;"></div>

            <div style="float: left;position: relative">
                <a href="resume_pdf.php?email=<?php echo $res_dados['email']?>" target="_blank"><div class="pdf_button"></div></a>
                <a href="https://goo.gl/maps/YwrXvQPfuXL2" target="_blank"><div class="mapa_button"></div></a>

                <div class="info_item black" style="width:auto;margin-top:20px;">
                        <p><span class="info_item_title">DUVIDAS</span><br />
                        <a href="mailto:info@pensarmaior.com">info@pensarmaior.com</a>
                        </p>
                    </div>
            </div>

            <div style="clear:left;"></div>

            <div class="holder_phone">
                <div class="texto_left_holder">
                    <p class="title_app red">APP PENSAR MAIOR FIDELIDADE</p>
                    <p class="text_app black"> DESCARREGUE JÁ A NOSSA APP E ENTRE
NO PENSAR MAIOR PELA PORTA
EXPRESS CHECK-IN COM MAIOR RAPIDEZ!</p>
                </div>
                <div class="phone_holder">
                    <img src="img/phone.png" alt="App Pensar Maior Fidelidade" width="100%"/>
                </div>
                <div class="texto_right_holder">
                    <p class="title_app black">EXPRESS CHECK-IN</p>
                    <p class="subtitle_app red">FAÇA JÁ O DOWNLOAD</p>
                    <hr class="divider_footer" />
                    <p class="subtitle_app red" >
                        <a href="https://itunes.apple.com/us/app/pensar-maior-fidelidade/id1203139703?l=pt&ls=1&mt=8" style="text-decoration: none;" target="_blank">
                            <img src="img/appstrore.svg" alt="" width="40%">
                        </a>
                       &nbsp; &nbsp; &nbsp; &nbsp;
                        <a href="https://play.google.com/store/apps/details?id=com.spic.fidelidade&hl=pt_PT#details-reviews" style="text-decoration: none;" target="_blank">
                            <img src="img/googleplay.svg" alt="" width="40%">
                        </a>
                    </p>
                </div>
            </div>
        </div>
        
		

		<div class="logo_footer">
			<!--<img src="img/fidelidade_caixa_branca.png" alt="Fidelidade" class="back"/>-->
			<img src="img/logo_fidelidade_cinza.png" alt="Fidelidade" width="100%"/>
		</div>

		<?php echo $string_error; ?>



        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main_new.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>