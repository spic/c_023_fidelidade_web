<?php 


  # Abre a diretoria da galeria
  $galleryDir = opendir("imagenspadrao");

  # Diz o que não pode abrir

  $restricted = array(".", "..", "thumbs" , "index.html", "Thumbs.db", ".DS_Store");


  # Faz loop a todos o files
  $i = -1;

  while ( false !== ($filename = readdir($galleryDir))) {

    if(!in_array($filename, $restricted)){
      $i++;
       
      // echo $filename."<br>";
      $files_array[] = $filename;

    }  
  }

  // var_dump($files_array);

  sort($files_array);

  // echo "<br />";

  // var_dump($files_array);
  #Paginação 

  $count = 0 ;
  $limit = 10;
  $total_pages = ceil(count($files_array) / $limit );

  $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

  if ($page < 1 || $page > $total_pages) $page = 1;

  $start = ($page - 1) * $limit;



?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Galeria</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="css/maingaleria.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div class="col-xs-12 titulo_galeria">
    Galeria
  </div>

  
    <?php for ($i=$start; $i < count($files_array); $i++) { $count ++; ?>    
      <a href="viewphoto.php?nome=<?php echo $i ?>" >
        <div class="col-xs-6 padding_fotos">
          <img src="return_img.php?width=500&height=300&img=imagenspadrao/<?php echo $files_array[$i];  ?>" alt="" width="100%">
        </div>  
      </a>
    <?php if($limit == $count)  break; } ?>

      <div class="col-xs-12 holder_botoes_paginacao" >
        <div class="col-xs-6 pd0 "> 
          <?php if($page > 1){?>
           <a href="?page=<?php echo ($page - 1); ?>">
            <div class="col-xs-12 pdo anterior">
                <div><img src="img/anterior.svg" alt="" width="30"></div>
                <div>ANTERIOR</div>
            </div>
           </a>
           <?php } ?>

         </div>
         <div class="col-xs-6 pd0 ">
            <?php if($total_pages > $page){?>
           <a href="?page=<?php echo ($page + 1); ?>"> 

            <div class="col-xs-12 pdo seguinte">
                <div>SEGUINTE</div>
                <div><img src="img/seguinte.svg" alt="" width="30"></div>
               </div>  
           </a>
           <?php } ?>
         </div>
      </div>
     
  
 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>