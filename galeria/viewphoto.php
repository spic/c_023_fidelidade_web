<?php 

  $name = $_GET['nome'];


  
 
  # Abre a diretoria da galeria
  $galleryDir = opendir("imagenspadrao");

  # Diz o que não pode abrir

  $restricted = array(".", "..", "thumbs" , "index.html", "Thumbs.db", ".DS_Store");


  # Faz loop a todos o files
  $i = -1;

  while ( false !== ($filename = readdir($galleryDir))) {

    if(!in_array($filename, $restricted)){
      $i++;
       #echo $filename."<br>";
      $files_array[] = $filename;

    }  
  }

  sort($files_array);

  if($name < 0){
    $name = count($files_array)-1;
  }

  if($name > count($files_array)-1){
    $name = 0;
  }
  //  $total_pages = ceil(count($files_array) / $limit );

  // if ($name < 0 || $name > $total_pages) $name = 0;
 
  // echo $name;

  // var_dump($files_array); 

  if($name >= 0 && $name <= 9){
    $page = 1;
  }
  if($name >= 10 && $name <= 19){
    $page = 2;
  }
  if($name >= 20 && $name <= 29){
    $page = 3;
  }

 ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Galeria</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="css/maingaleria.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <a href="index.php?page=<?php echo $page ?>">
      <div class="col-xs-12 holder_voltar">
          <div><img src="../agenda/img/setavoltar.svg" alt="" width="25"></div>
          <div >VOLTAR</div>
      </div>
  </a>
    
  <div class="col-xs-12 titulo_galeria">
    Galeria
  </div>

  <div class="col-xs-12 holder_foto_unica">
    <?php 

      echo " <img src=imagenspadrao/".$files_array[$name]." alt='fotounica' width='100%'> ";

     ?>
  </div>

   <div class="col-xs-12 holder_botoes_paginacao" >
        <div class="col-xs-6 pd0 "> 
         
           <a href="?nome=<?php echo $name - 1 ?>">
            <div class="col-xs-12 pdo anterior">
                <div><img src="img/anterior.svg" alt="" width="30"></div>
                <div>ANTERIOR</div>
            </div>
           </a>
        

         </div>
         <div class="col-xs-6 pd0 ">
           
           <a  href="?nome=<?php echo $name + 1 ?>"> 

            <div class="col-xs-12 pdo seguinte">
                <div>SEGUINTE</div>
                <div><img src="img/seguinte.svg" alt="" width="30"></div>
               </div>  
           </a>
          
         </div>
    </div>

    <div class="col-xs-12 botao_download">

      <a target="_blank" href="download.php?nome=<?php echo $files_array[$name] ?>">
        <div class="button submit_class">DOWNLOAD</div>
      </a>
    </div> 

     
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
