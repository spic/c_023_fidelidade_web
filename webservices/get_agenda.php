<?php

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors","off");
ini_set('error_log','my_file.log');

require("../old/classes/database.class.php");
$database = new database();
// select apresentacoes.id, group_concat(apresentacoes_oradores.nome separator ', ') from apresentacoes, apresentacoes_oradores where apresentacoes.id = apresentacoes_oradores.id_apresentacao group by 'all'
$res_agenda = $database->query_simple_prepare("SELECT ".$database->array_tables[4].".id,".$database->array_tables[4].".flag_passado,".$database->array_tables[4].".flag_activo,".$database->array_tables[4].".titulo, group_concat(".$database->array_tables[6].".nome separator ', ') as oradores FROM ".$database->array_tables[4].", ".$database->array_tables[6]." WHERE ".$database->array_tables[4].".id = ".$database->array_tables[6].".id_apresentacao GROUP BY ".$database->array_tables[4].".id ORDER BY ".$database->array_tables[4].".ordem ASC");


$array_response = array();

if($res_agenda != 'error'){
    $array_response['response'] = "ok";
    $array_response['agenda'] = $res_agenda;    
}
else{
    $array_response['response'] = "error";
}
echo json_encode($array_response);
?>