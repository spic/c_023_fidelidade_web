<?php
error_reporting(E_ALL | E_STRICT);
ini_set("display_errors", "off");

$lang = 'en';


/*URL PARA BASE DO ANGULARJS*/
$url_base = $_SERVER['PHP_SELF'];
$url_base_array = explode('/', $url_base);
$url_base = "";
for ($i = 0; $i < count($url_base_array) - 1; $i++) {
    $url_base .= $url_base_array[$i] . "/";
}

require('classes/database.class.php');
$database = new database();
$area = $_GET['area'];
if ($area != 'blog') {
    $id_lang = $database->return_id_lang($lang);
    if (count($id_lang) > 1) {
        $lang = $id_lang[2];
        $id_lang = $id_lang[1];
    }
    $lang = 'en';

    $string_sql = "SELECT 
  " . $database->array_tables[3] . ".id AS id, 
  " . $database->array_tables[3] . ".data as data, 
  " . $database->array_tables[3] . ".url_post as url_post, 
  " . $database->array_tables[4] . ".titulo as titulo,
  " . $database->array_tables[4] . ".subtitulo as subtitulo, 
  " . $database->array_tables[4] . ".texto1 as texto1,
  " . $database->array_tables[4] . ".texto2 as texto2
  FROM " . $database->array_tables[3] . "," . $database->array_tables[4] . "
  WHERE 
  activo =1 AND " . $database->array_tables[3] . ".url_post = ? AND 
  " . $database->array_tables[3] . ".id = " . $database->array_tables[4] . ".id_post AND
  " . $database->array_tables[4] . ".id_idioma = ?";

    $array_arguments = array($area, $id_lang);
    $string_args = 'si';

    $res = $database->query_simple_prepare($string_sql, $array_arguments, $string_args);

    if (count($res) > 0) {
        $titulo_page = " - " . $res[0]['titulo'];
        $desc_page = substr($res[0]['texto1'] . $res[0]['texto2'], 0, 160);

        $res_media = $database->query_simple_prepare("SELECT id_tipo, media FROM " . $database->array_tables[9] . " WHERE id_post=? AND id_tipo=2", array($res[0]['id']), 'i');
        if (count($res_media) > 0) {
            $imagem_final = "http://" . $_SERVER['HTTP_HOST'] . $url_base . "posts/" . $res[0]['id'] . "/" . $res_media[0]['media'];
        } else {
            $imagem_final = "http://" . $_SERVER['HTTP_HOST'] . $url_base . "img/logo_novo.png";
        }
    } else {
        $titulo_page = "";
        $imagem_final = "http://" . $_SERVER['HTTP_HOST'] . $url_base . "img/logo_novo.png";
        $url_final = "http://" . $_SERVER['HTTP_HOST'] . $url_base . "/" . $area;
    }


} else {
    $titulo_page = "";

    $imagem_final = "http://" . $_SERVER['HTTP_HOST'] . $url_base . "img/logo_novo.png";
    $url_final = "http://" . $_SERVER['HTTP_HOST'] . $url_base . "/" . $area;
}


?>
<!doctype html>
<html class="no-js" ng-app="milesApp" ng-cloak>
<head>
    <base href="<?php echo $url_base ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title ng-bind="titlePage">Pensar Maior 2017</title>
    <meta name="keywords"
          content=""/>


    <!-- for Facebook -->
    <meta property="og:title" content="Pensar Maior 2017"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="<?php echo $imagem_final ?>"/>
    <meta property="og:url" content="<?php echo $url_final ?>"/>
    <meta property="og:description" content="Pensar Maior 2017"/>

    <!-- for Google -->
    <meta name="description" content="Pensar Maior 2017"/>
    <!-- <meta name="keywords" content="<?php echo $keywords_final ?>" /> -->

    <meta name="author" content="Pensar Maior 2017"/>
    <meta name="copyright" content="Fidelidade"/>
    <meta name="application-name" content="Pensar Maior 2017"/>



    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

    <link rel="stylesheet" href="css/bootstrap.min.css">
   
    
     <link rel="stylesheet" href="css/main.css">
     <link rel="stylesheet" href="css/main_blog_new.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<div class="col-xs-12 pd0 holder_password" ng-class="{afirmativo: verifica == 1}">
  <div class="col-xs-12 pd0 holder_password_fundo">
    
  </div>
  <div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 pd0 holder_password_password" >
      <div class="password_frase">
        PARA VISUALISAR OS CONTEÚDOS <br>POR FAVOR INSIRA A PASSWORD
      </div>
      <div>
        <form id="pass_form" name="pass_form" >
          <input type="password" class="input_password" ng-model="password.pass" style=" background: #F2F2F2;
    border-radius: 20px;
    border: none;
    width: 100%;
    height: 40px;
    padding-left: 10px;
    padding-right: 10px;
    font-family: 'azo_sansregular';
    font-size: 16px;">
          <input type="submit" value="Submeter" ng-click="verificar(password.pass)" class="button submit_class" style="margin-top: 10px;">
        </form>
      </div>
      <div class="passerror" ng-class="{errorpass: verificaerror == 1}">Password Incorreta</div>
  </div>
</div>
<div class="background"></div>
<!-- MENU -->
<menu></menu>



<!-- CONTAINER -->
<div class="container_website col-xs-12" ng-view></div>

<!-- FOOTER -->
<footer></footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>


<script src="js/plugins.js"></script>
<?php
$mydir = "js/user_scripts/";
$d = dir($mydir);

unlink("js/main.js");
$myfile = fopen("js/main.js", "w") or die("Unable to open file!");

$content_file = file_get_contents("js/user_scripts/lang/" . $lang . ".js");
fwrite($myfile, $content_file);

$content_file = file_get_contents($mydir . "app.js");
fwrite($myfile, $content_file);
$content_file = file_get_contents($mydir . "services.js");
fwrite($myfile, $content_file);
$content_file = file_get_contents($mydir . "directives.js");
fwrite($myfile, $content_file);

while ($entry = $d->read()) {
    if ($entry != "." && $entry != ".DS_Store" && $entry != ".." && $entry != "app.js" && $entry != "services.js" && $entry != "directives.js" && $entry != "lang" && $entry != "minify") {
        // echo $entry;
        $array_nome_file = explode(".", $entry);
        // if ($array_nome_file[count($array_nome_file)-2] == 'min') {
        $content_file = file_get_contents($mydir . $entry);
        fwrite($myfile, $content_file);
        //}
    }
}
$d->close();

fclose($myfile);

?>
<script src="js/main.js"></script>

<script>
    var lang = '<?php echo $lang?>';
    var pathgeral = '<?php echo "http://" . $_SERVER['HTTP_HOST'] . $url_base ?>';
</script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12913211-65', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
