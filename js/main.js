var array_lang = {
	tudo_palavra: "All",
	pergunta_palavra: "Ask Anything",
	assets_palavra: "Assets",
	servicos_palavra: "Services",
	social_responsibility_palavra: "Social Responsibility",
	redessociais_palavra: "Social Media",
	morada_frase: "Av. Eng. Duarte Pacheco, <br />Amoreiras, Torre 2, 14º D <br />1070-102 Lisboa Portugal<br /><br />CBC Towers, 4th floor, <br />Olubunmi owa street, Lekki phase 1, <br />Lagos Nigeria.",
	telefone_frase: "Phone: +351 211 221 864",
	email_frase: "Email: info@fivethousandmiles.com",
	vermais_palavra: "Ver mais",
	carregando_palavra: "Loading",
	texo_ask: "We are obsessed about delivering value. How can we help you?",
	comoajudar_frase: "How can we help you?",
	option1form_frase: "I'm in Africa and want a job at a multinational company",
	option2form_frase: "I’m not in Africa but want a job in Africa",
	option3form_frase: "I'm an African company / businessman looking for an international partner",
	option4form_frase: "I’m a company looking to expand business to Africa",
	askanything_frase: "Ask us anything",
	email_palavra: "Email",
	telefone_palavra: "Phone",
	nome_palavra: "Name",
	camposobrigatorios_frase: "Fill in all required fields (*)",
	formenviado_frase: "Form submitted successfully!",
	formerro_frase: "Form not sent, try later!",
	frase_footer1: "LOREM IPSUM IS SIMPLY DUMMY TEXT.",
	texo_social: "Get direct access to 5TM's management and follow our business development adventures.",
	saibamais_palavra: "Know more",
	audio_palavra: "Audio File",
	download_palavra: "Download File",
	frase_newsletter: "Get Africa insights directly into your inbox",
	subscrevanewsletter_frase: "SUBSCRIBE TO OUR NEWSLETTER",
	descricao_page:"5 Thousand Miles - The Gateway to Africa",
	option1form: "Career - Apply for a job",
	option2form: "Social responsibility - Evolution Program",
	option3form: "Consultation - Insights program",
	option4form: "Advisory - Knowledge Program",
	option5form: "Sales - Kickstart Program",
	option6form: "Sales - Discovery Program",
	option7form: "Sales - Talent Program",
	option8form: "Trading - Import goods",
	option9form: "Other - None of the above ",
	quemevoce_frase: "Who are you?",
	option1formwho: "I represent a company",
	option2formwho: "I am an individual",
	deonde_frase: "Where are you from?",
	budget_frase: "What’s your budget?",
	company_palavra: "Company",
	professional_frase: "Professional title",




	
};'use strict';

/* App Module */

var milesApp = angular.module('milesApp', [
    'ngRoute',
    'ngSanitize',
    'services'
    // 'ngAnimate'
]);

milesApp.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        $routeProvider.when(
            '/blog', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl',
                resolve: {
                    items: ['$rootScope', 'services', function ($rootScope, services) {
                        $rootScope.area_sel = 'blog';
                        $rootScope.menu_mobile_open = false;
                        $rootScope.array_tags = [];
                        $rootScope.social = false;
                        var promise = services.get_posts($rootScope.array_tags, 0, 8, lang);
                        return promise;
                    }]
                }
            })
            .when(
            '/fotos', {
                templateUrl: 'views/galeria.html',
                controller: 'galeriaCtrl',
                resolve: {
                    items: ['$rootScope', 'services', function ($rootScope, services) {
                        $rootScope.area_sel = 'galeria';
                        $rootScope.menu_mobile_open = false;
                        
                        return 1;
                    }]
                }
            })
            .when('/:url_post', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl',
                resolve: {
                    items: ['$rootScope', 'services', '$route', function ($rootScope, services, $route) {
                        $rootScope.area_sel = 'all';
                        $rootScope.menu_mobile_open = false;
                        var promise = services.get_post($route.current.params.url_post, lang);
                        return promise;
                        $rootScope.social = false;
                    }]
                }
            }).otherwise({
            redirectTo: '/blog'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: true
        });

    }]);


milesApp.run(['$rootScope', 'services', function ($rootScope, services) {
    $rootScope.select_menu = function () {
        var position_select = angular.element('.menu_item_sel').position().left;
        angular.element('.menu_item_sel_bar').css('left', position_select);
    }

    $rootScope.submitted_news = false;
    $rootScope.form_invalid_news = false;
    $rootScope.form_sucess_news = false;
    $rootScope.form_error_news = false;
    $rootScope.form_news = {};
    $rootScope.submit_form_news = function (form) {
        $rootScope.submitted_news = true;
        if (form.$invalid) {
            $rootScope.form_sucess_news = false;
            $rootScope.form_error_news = false;
            $rootScope.form_invalid_news = true;
            return;
        }
        else {
            $rootScope.form_invalid_news = false;
            var promise = services.submit_form_news($rootScope.form_news, $rootScope.lang);
            promise.then(
                function (response) {
                    console.log(response);
                    if (response == 0) {
                        $rootScope.form_sucess_news = true;
                        $rootScope.form_error_news = false;
                        $rootScope.form_news = {};
                    }
                    else {
                        $rootScope.form_sucess_news = false;
                        $rootScope.form_error_news = true;
                    }
                }
                , function (reason) {
                    console.log('Failed: ' + reason);
                }
            );
        }
    }

    /*GET LANG*/
    $rootScope.lang = lang;
    $rootScope.lang_array = array_lang;
    $rootScope.social = false;

    $rootScope.pathgeral = pathgeral;

    $rootScope.verifica = 0;
    $rootScope.verificaerror = 0;

   $rootScope.verificar = function(password){
        if(password == "Pensarmaior2017"){
            $rootScope.verifica = 1;
        }
        else{
            $rootScope.verificaerror = 1;
        }
        console.log(password);
   }

}]);




'use strict';


var services = angular.module('services', []);

services.service('services',['$http','$q','$location','$window', function ($http,$q,$location,$window) {

    this.get_posts = function(array_tags,limit_ini,limit_final,lang){
        var deferred = $q.defer();
        $http.post('server/get_posts.php?data='+(Math.random()),{'array_tags': array_tags,'limit_ini':limit_ini,'limit_final':limit_final},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
    this.get_photos = function(page){
        var deferred = $q.defer();
        $http.get('server/get_photos.php?page='+page+'&data='+(Math.random()),{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.get_post = function(url_post,lang){
    	var deferred = $q.defer();
		$http.post('server/get_post.php?data='+(Math.random()),{'url_post': url_post,'lang':lang},{ cache: false}).success(function(data, status) {
		   deferred.resolve(data);
		}).error(function(data, status) {
		   deferred.reject(data);
		});

		return deferred.promise;
    }
    

    this.submit_form = function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_form.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
    this.submit_formz = function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_formz.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
    this.submit_evoform= function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_evoform.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.submit_form_news = function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_form_news.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
           deferred.resolve(data);
        }).error(function(data, status) {
           deferred.reject(data);
        });

        return deferred.promise;
    }
    this.get_paises = function(){
        var deferred = $q.defer();
        $http.get('server/get_paises.php?data='+(Math.random()),{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
}]);

services.service('PageTitle',['$rootScope','$rootElement', function($rootScope,$rootElement) {
  return {
    setTitle: function(newTitle) {
      $rootScope.titlePage = newTitle; 
      angular.element($rootElement.find('meta[name=\'twitter:title\']')[0]).attr('content',newTitle);
      angular.element($rootElement.find('meta[property=\'og:title\']')[0]).attr('content',newTitle);
      
    },
    setDesc: function(newDesc) {
      angular.element($rootElement.find('meta[name=description]')[0]).attr('content',newDesc);
      angular.element($rootElement.find('meta[property=\'og:description\']')[0]).attr('content',newDesc);
      angular.element($rootElement.find('meta[name=\'twitter:description\']')[0]).attr('content',newDesc);
    }
  };
}]);milesApp.directive('menu', [function () {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    link: function (scope, elem, attrs) {
     
      
    },
    templateUrl: 'templates/menu.html'
  };
}]);

milesApp.directive('footer', [function () {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    link: function (scope, elem, attrs) {
     
    },
    templateUrl: 'templates/footer.html'
  };
}]);

'use strict';


milesApp.controller('africaCtrl', ['$scope', '$rootScope','$window','$timeout','items','services','PageTitle','$location', function africaCtrl($scope, $rootScope, $window,$timeout,items, services,PageTitle,$location){

    /*CLICKS*/
    $scope.$on('$viewContentLoaded', function(event) {
        $window.ga('send', 'pageview', { page: $location.url() });
    });

    $scope.test = "asd";
    $scope.submitted = false;
    $scope.form_invalid = false;
    $scope.form_sucess = false;
    $scope.form_error = false;


    $(document).ready(function() {
        $("#menu_items").hide();
    });
    $scope.submit_form = function(form){


        $scope.submitted = true;
        if (form.$invalid) {
            alert("Preencher campos obrigatórios");
            $scope.form_sucess = false;
            $scope.form_error = false;
            $scope.form_invalid = true;
            return;
        }
        else{
            $scope.form_invalid = false;
            var promise = services.submit_formz($scope.form,$rootScope.lang);
            promise.then(
                function(response){
                    if (response == 0) {
                        $scope.form_sucess = true;
                        $scope.form_error = false;
                        $scope.form = {};
                    }
                    else{
                        $scope.form_sucess = false;
                        $scope.form_error = true;
                    }
                }
                ,function(reason){console.log('Failed: ' + reason);}
            );
        }
    }





}]);'use strict';


milesApp.controller('askCtrl', ['$scope', '$rootScope','$window','$timeout','items','services','PageTitle','$location', function askCtrl($scope, $rootScope, $window,$timeout,items, services,PageTitle,$location){

	/*CLICKS*/
	$scope.$on('$viewContentLoaded', function(event) {
	 $window.ga('send', 'pageview', { page: $location.url() });  
	});



	/*SET TITLE*/
	PageTitle.setTitle('Five Thousand Miles');
	PageTitle.setDesc($rootScope.lang_array.descricao_page);	
  	
  	$scope.paises = items;

  	console.log(items);

	$scope.submitted = false;
	$scope.form_invalid = false;
	$scope.form_sucess = false;
	$scope.form_error = false;
	$scope.form = {};
	$scope.submit_form = function(form){
		$scope.submitted = true;
		if (form.$invalid) {
			$scope.form_sucess = false;
         $scope.form_error = false;	
			$scope.form_invalid = true;
			return;
		}
		else{
			$scope.form_invalid = false;
			var promise = services.submit_form($scope.form,$rootScope.lang);
			promise.then(
            function(response){
            	if (response == 0) {
            		$scope.form_sucess = true;
            		$scope.form_error = false;
            		$scope.form = {};
            	}
            	else{
            		$scope.form_sucess = false;
            		$scope.form_error = true;	
            	}
            }
           ,function(reason){console.log('Failed: ' + reason);}
         );  
		}
	}  



  

}]);'use strict';


milesApp.controller('galeriaCtrl', ['$scope', '$rootScope','$window','$timeout','$sce','items','services','$routeParams','PageTitle','$location', function galeriaCtrl($scope, $rootScope, $window,$timeout,$sce,items, services,$routeParams,PageTitle,$location){

$scope.page = 1;
  
  	var promise = services.get_photos($scope.page);  
		promise.then(
		function(response){
			$scope.fotos = response.fotos;
			console.log($scope.fotos);
			console.log(response.totalfotos);
			console.log(response.totalpages);
			$scope.comprimento = $scope.fotos.length;
			$scope.totalfotos = response.totalfotos;
			$scope.totalpages = response.totalpages;
			
	
		});
	
		
		
		

		$scope.seguintepagina = function(){
			if($scope.page < $scope.totalpages){
				$scope.page++;
		}
			
			var promise = services.get_photos($scope.page);  
				promise.then(
				function(response){
						$scope.fotos = response.fotos;
					console.log($scope.fotos);
					console.log(response.totalfotos);
					$scope.comprimento = $scope.fotos.length;
					$scope.totalfotos = response.totalfotos;
					$scope.totalpages = response.totalpages;
					
				});
		}

		$scope.anteriorpagina = function(){
			if($scope.page > 1){
				$scope.page--;
		}

				var promise = services.get_photos($scope.page);  
				promise.then(
				function(response){
				$scope.fotos = response.fotos;
				console.log($scope.fotos);
				console.log(response.totalfotos);
				$scope.comprimento = $scope.fotos.length;
				$scope.totalfotos = response.totalfotos;
				$scope.totalpages = response.totalpages;
					
				});
		}
		

        $scope.pop = 0;

        $scope.abrirpopup = function(index){
			console.log(index);
			$scope.pop = 1;
			$scope.indexpop = index;
		}

		$scope.seguinte = function(index){
			if(index < $scope.comprimento - 1){
				$scope.indexpop = index + 1;
			}
			else{
				$scope.indexpop = 0;
			}	
		}

		$scope.anterior = function(index){
			if(index < 1){
				$scope.indexpop = $scope.comprimento - 1;
				console.log(index);
			}
			else{
				$scope.indexpop = index - 1;
			}
		}




}]);
'use strict';


milesApp.controller('homeCtrl', ['$scope', '$rootScope','$window','$timeout','$sce','items','services','$routeParams','PageTitle','$location', function homeCtrl($scope, $rootScope, $window,$timeout,$sce,items, services,$routeParams,PageTitle,$location){


  /*GET POSTS*/
  console.log(items);
  $scope.posts = items.posts;
  $scope.total_posts = items.total_posts;
  $scope.total_posts_act = $scope.posts.length;
  if ($scope.total_posts_act == 0 && $routeParams.url_post) {
  	// $window.location.href = $rootScope.lang+"/all";
  }

  /*CLICKS*/
  $scope.$on('$viewContentLoaded', function(event) {
    $window.ga('send', 'pageview', { page: $location.url() });  
  });

  if ($routeParams.url_post) {
		/*SET TITLE*/
	  	PageTitle.setTitle('Pensar Maior 2017 -'+$scope.posts[0].titulo);
	  	PageTitle.setDesc( $scope.posts[0].texto1.substring(0,160));	
  }
  else{
  		/*SET TITLE*/
	  	PageTitle.setTitle('Pensar Maior 2017');
	  	PageTitle.setDesc($rootScope.lang_array.descricao_page);	
  }
  
  
  	
  /*CONFIAR NO EMBED DO VIDEO*/
  angular.forEach($scope.posts, function(post,key){
  	angular.forEach(post.media, function(item_media, key2){
  		if (item_media.id_tipo == 1) {
  			item_media.media = $sce.trustAsHtml('<iframe width="100%" height="100%" src="'+item_media.media+'" frameborder="0" allowfullscreen></iframe>');
  		}
  		if (item_media.id_tipo == 4) {
  			item_media.media = $sce.trustAsHtml($rootScope.pathgeral+'posts/'+post.id+'/'+item_media.media);
  		}
  	});
  });

  	/*FILTRO MEDIA - VIDEO E IMAGEM*/
  	$scope.vidorimg = function(item){
	    return item.id_tipo == 1 || item.id_tipo == 2;
	};
	$scope.audiofile = function(item){
	    return item.id_tipo == 3 || item.id_tipo == 4;
	};

	/*PLAY AUDIO*/
	$scope.play_audio = function(item,file){
		if (!item.playing) {
			item.audio = new Audio(file);
			item.audio.play();
			item.playing = true;
		}
		else{
			item.audio.pause();
			item.playing = false;
		}
	}

	/*RESIZE WINDOW*/
	var width_original_media = 1080;
	var height_original_media = 610;
	var w = angular.element($window);
	function getWindowDimensions() {
	 return {
	    'h': w.height(),
	    'w': w.width()
	 };
	};
	var window_width = 0;
	var window_height = 0;
	$scope.resize_timeout;

	function resize_func(){
		window_width = w.width();
		window_height = w.height();

		var height_video = (angular.element(".holder_conteudo").width()*height_original_media)/width_original_media;

		$timeout.cancel($scope.resize_timeout);
		$scope.resize_timeout = $timeout(function(){
			angular.element(".video_holder").height(height_video);
		},250);


	}

	$scope.$watch(getWindowDimensions, function (newValue, oldValue) {
	  resize_func();
	}, true);

	w.bind('resize', function () {
		$scope.$apply();
	});

	/*GET MORE POSTS*/
	$scope.get_more_posts = function(){
		angular.element(".get_more_button").addClass('get_more_button_loading');
		var promise = services.get_posts($rootScope.array_tags,$scope.total_posts_act,8,$scope.lang);  
		promise.then(
         function(response){
			  angular.forEach(response.posts, function(post,key){
				angular.forEach(post.media, function(item_media, key2){
					if (item_media.id_tipo == 1) {
						item_media.media = $sce.trustAsHtml('<iframe width="100%" height="100%" src="'+item_media.media+'" frameborder="0" allowfullscreen></iframe>');
					}
					if (item_media.id_tipo == 4) {
						item_media.media = $sce.trustAsHtml($rootScope.pathgeral+'posts/'+post.id+'/'+item_media.media);
					}
				});
			});
         	angular.forEach(response.posts, function(item, key) {
         		$scope.posts.push(item);
         	});
         	$scope.total_posts_act = $scope.posts.length;
			  
			resize_func();
         	angular.element(".get_more_button").removeClass('get_more_button_loading');
         }
         ,function(reason){console.log('Failed: ' + reason);}
      );  
	}
    //form
    $scope.submitted = false;
    $scope.form_invalid = false;
    $scope.form_sucess = false;
    $scope.form_error = false;
    $scope.form = {};
    $scope.submit_form = function(form){
        $scope.submitted = true;
        if (form.$invalid) {
            $scope.form_sucess = false;
            $scope.form_error = false;
            $scope.form_invalid = true;
            return;
        }
        else{
            $scope.form_invalid = false;
            var promise = services.submit_evoform($scope.form,$rootScope.lang);
            promise.then(
                function(response){
                    if (response == 0) {
                        $scope.form_sucess = true;
                        $scope.form_error = false;
                        $scope.form = {};
                    }
                    else{
                        $scope.form_sucess = false;
                        $scope.form_error = true;
                    }
                }
                ,function(reason){console.log('Failed: ' + reason);}
            );
        }
    }

}]);'use strict';


milesApp.controller('socialCtrl', ['$scope', '$rootScope','$window','$timeout','items','services','PageTitle','$location', function socialCtrl($scope, $rootScope, $window,$timeout,items, services,PageTitle,$location){

	/*CLICKS*/
	$scope.$on('$viewContentLoaded', function(event) {
	 $window.ga('send', 'pageview', { page: $location.url() });  
	});


	/*SET TITLE*/
	PageTitle.setTitle('Five Thousand Miles');
	PageTitle.setDesc($rootScope.lang_array.descricao_page);	
  


}]);