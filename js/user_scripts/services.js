'use strict';


var services = angular.module('services', []);

services.service('services',['$http','$q','$location','$window', function ($http,$q,$location,$window) {

    this.get_posts = function(array_tags,limit_ini,limit_final,lang){
        var deferred = $q.defer();
        $http.post('server/get_posts.php?data='+(Math.random()),{'array_tags': array_tags,'limit_ini':limit_ini,'limit_final':limit_final},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
    this.get_photos = function(page){
        var deferred = $q.defer();
        $http.get('server/get_photos.php?page='+page+'&data='+(Math.random()),{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.get_post = function(url_post,lang){
    	var deferred = $q.defer();
		$http.post('server/get_post.php?data='+(Math.random()),{'url_post': url_post,'lang':lang},{ cache: false}).success(function(data, status) {
		   deferred.resolve(data);
		}).error(function(data, status) {
		   deferred.reject(data);
		});

		return deferred.promise;
    }
    

    this.submit_form = function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_form.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
    this.submit_formz = function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_formz.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
    this.submit_evoform= function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_evoform.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    this.submit_form_news = function(form_item,lang){
        var deferred = $q.defer();
        $http.post('server/send_form_news.php?data='+(Math.random()),{'form_item': form_item,'lang':lang},{ cache: false}).success(function(data, status) {
           deferred.resolve(data);
        }).error(function(data, status) {
           deferred.reject(data);
        });

        return deferred.promise;
    }
    this.get_paises = function(){
        var deferred = $q.defer();
        $http.get('server/get_paises.php?data='+(Math.random()),{ cache: false}).success(function(data, status) {
            deferred.resolve(data);
        }).error(function(data, status) {
            deferred.reject(data);
        });

        return deferred.promise;
    }
}]);

services.service('PageTitle',['$rootScope','$rootElement', function($rootScope,$rootElement) {
  return {
    setTitle: function(newTitle) {
      $rootScope.titlePage = newTitle; 
      angular.element($rootElement.find('meta[name=\'twitter:title\']')[0]).attr('content',newTitle);
      angular.element($rootElement.find('meta[property=\'og:title\']')[0]).attr('content',newTitle);
      
    },
    setDesc: function(newDesc) {
      angular.element($rootElement.find('meta[name=description]')[0]).attr('content',newDesc);
      angular.element($rootElement.find('meta[property=\'og:description\']')[0]).attr('content',newDesc);
      angular.element($rootElement.find('meta[name=\'twitter:description\']')[0]).attr('content',newDesc);
    }
  };
}]);