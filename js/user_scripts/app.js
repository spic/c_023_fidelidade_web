'use strict';

/* App Module */

var milesApp = angular.module('milesApp', [
    'ngRoute',
    'ngSanitize',
    'services'
    // 'ngAnimate'
]);

milesApp.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        $routeProvider.when(
            '/blog', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl',
                resolve: {
                    items: ['$rootScope', 'services', function ($rootScope, services) {
                        $rootScope.area_sel = 'blog';
                        $rootScope.menu_mobile_open = false;
                        $rootScope.array_tags = [];
                        $rootScope.social = false;
                        var promise = services.get_posts($rootScope.array_tags, 0, 8, lang);
                        return promise;
                    }]
                }
            })
            .when(
            '/fotos', {
                templateUrl: 'views/galeria.html',
                controller: 'galeriaCtrl',
                resolve: {
                    items: ['$rootScope', 'services', function ($rootScope, services) {
                        $rootScope.area_sel = 'galeria';
                        $rootScope.menu_mobile_open = false;
                        
                        return 1;
                    }]
                }
            })
            .when('/:url_post', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl',
                resolve: {
                    items: ['$rootScope', 'services', '$route', function ($rootScope, services, $route) {
                        $rootScope.area_sel = 'all';
                        $rootScope.menu_mobile_open = false;
                        var promise = services.get_post($route.current.params.url_post, lang);
                        return promise;
                        $rootScope.social = false;
                    }]
                }
            }).otherwise({
            redirectTo: '/blog'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: true
        });

    }]);


milesApp.run(['$rootScope', 'services', function ($rootScope, services) {
    $rootScope.select_menu = function () {
        var position_select = angular.element('.menu_item_sel').position().left;
        angular.element('.menu_item_sel_bar').css('left', position_select);
    }

    $rootScope.submitted_news = false;
    $rootScope.form_invalid_news = false;
    $rootScope.form_sucess_news = false;
    $rootScope.form_error_news = false;
    $rootScope.form_news = {};
    $rootScope.submit_form_news = function (form) {
        $rootScope.submitted_news = true;
        if (form.$invalid) {
            $rootScope.form_sucess_news = false;
            $rootScope.form_error_news = false;
            $rootScope.form_invalid_news = true;
            return;
        }
        else {
            $rootScope.form_invalid_news = false;
            var promise = services.submit_form_news($rootScope.form_news, $rootScope.lang);
            promise.then(
                function (response) {
                    console.log(response);
                    if (response == 0) {
                        $rootScope.form_sucess_news = true;
                        $rootScope.form_error_news = false;
                        $rootScope.form_news = {};
                    }
                    else {
                        $rootScope.form_sucess_news = false;
                        $rootScope.form_error_news = true;
                    }
                }
                , function (reason) {
                    console.log('Failed: ' + reason);
                }
            );
        }
    }

    /*GET LANG*/
    $rootScope.lang = lang;
    $rootScope.lang_array = array_lang;
    $rootScope.social = false;

    $rootScope.pathgeral = pathgeral;

    $rootScope.verifica = 0;
    $rootScope.verificaerror = 0;

   $rootScope.verificar = function(password){
        if(password == "Pensarmaior2017"){
            $rootScope.verifica = 1;
        }
        else{
            $rootScope.verificaerror = 1;
        }
        console.log(password);
   }

}]);




