'use strict';


milesApp.controller('askCtrl', ['$scope', '$rootScope','$window','$timeout','items','services','PageTitle','$location', function askCtrl($scope, $rootScope, $window,$timeout,items, services,PageTitle,$location){

	/*CLICKS*/
	$scope.$on('$viewContentLoaded', function(event) {
	 $window.ga('send', 'pageview', { page: $location.url() });  
	});



	/*SET TITLE*/
	PageTitle.setTitle('Five Thousand Miles');
	PageTitle.setDesc($rootScope.lang_array.descricao_page);	
  	
  	$scope.paises = items;

  	console.log(items);

	$scope.submitted = false;
	$scope.form_invalid = false;
	$scope.form_sucess = false;
	$scope.form_error = false;
	$scope.form = {};
	$scope.submit_form = function(form){
		$scope.submitted = true;
		if (form.$invalid) {
			$scope.form_sucess = false;
         $scope.form_error = false;	
			$scope.form_invalid = true;
			return;
		}
		else{
			$scope.form_invalid = false;
			var promise = services.submit_form($scope.form,$rootScope.lang);
			promise.then(
            function(response){
            	if (response == 0) {
            		$scope.form_sucess = true;
            		$scope.form_error = false;
            		$scope.form = {};
            	}
            	else{
            		$scope.form_sucess = false;
            		$scope.form_error = true;	
            	}
            }
           ,function(reason){console.log('Failed: ' + reason);}
         );  
		}
	}  



  

}]);