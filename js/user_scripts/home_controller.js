'use strict';


milesApp.controller('homeCtrl', ['$scope', '$rootScope','$window','$timeout','$sce','items','services','$routeParams','PageTitle','$location', function homeCtrl($scope, $rootScope, $window,$timeout,$sce,items, services,$routeParams,PageTitle,$location){


  /*GET POSTS*/
  console.log(items);
  $scope.posts = items.posts;
  $scope.total_posts = items.total_posts;
  $scope.total_posts_act = $scope.posts.length;
  if ($scope.total_posts_act == 0 && $routeParams.url_post) {
  	// $window.location.href = $rootScope.lang+"/all";
  }

  /*CLICKS*/
  $scope.$on('$viewContentLoaded', function(event) {
    $window.ga('send', 'pageview', { page: $location.url() });  
  });

  if ($routeParams.url_post) {
		/*SET TITLE*/
	  	PageTitle.setTitle('Pensar Maior 2017 -'+$scope.posts[0].titulo);
	  	PageTitle.setDesc( $scope.posts[0].texto1.substring(0,160));	
  }
  else{
  		/*SET TITLE*/
	  	PageTitle.setTitle('Pensar Maior 2017');
	  	PageTitle.setDesc($rootScope.lang_array.descricao_page);	
  }
  
  
  	
  /*CONFIAR NO EMBED DO VIDEO*/
  angular.forEach($scope.posts, function(post,key){
  	angular.forEach(post.media, function(item_media, key2){
  		if (item_media.id_tipo == 1) {
  			item_media.media = $sce.trustAsHtml('<iframe width="100%" height="100%" src="'+item_media.media+'" frameborder="0" allowfullscreen></iframe>');
  		}
  		if (item_media.id_tipo == 4) {
  			item_media.media = $sce.trustAsHtml($rootScope.pathgeral+'posts/'+post.id+'/'+item_media.media);
  		}
  	});
  });

  	/*FILTRO MEDIA - VIDEO E IMAGEM*/
  	$scope.vidorimg = function(item){
	    return item.id_tipo == 1 || item.id_tipo == 2;
	};
	$scope.audiofile = function(item){
	    return item.id_tipo == 3 || item.id_tipo == 4;
	};

	/*PLAY AUDIO*/
	$scope.play_audio = function(item,file){
		if (!item.playing) {
			item.audio = new Audio(file);
			item.audio.play();
			item.playing = true;
		}
		else{
			item.audio.pause();
			item.playing = false;
		}
	}

	/*RESIZE WINDOW*/
	var width_original_media = 1080;
	var height_original_media = 610;
	var w = angular.element($window);
	function getWindowDimensions() {
	 return {
	    'h': w.height(),
	    'w': w.width()
	 };
	};
	var window_width = 0;
	var window_height = 0;
	$scope.resize_timeout;

	function resize_func(){
		window_width = w.width();
		window_height = w.height();

		var height_video = (angular.element(".holder_conteudo").width()*height_original_media)/width_original_media;

		$timeout.cancel($scope.resize_timeout);
		$scope.resize_timeout = $timeout(function(){
			angular.element(".video_holder").height(height_video);
		},250);


	}

	$scope.$watch(getWindowDimensions, function (newValue, oldValue) {
	  resize_func();
	}, true);

	w.bind('resize', function () {
		$scope.$apply();
	});

	/*GET MORE POSTS*/
	$scope.get_more_posts = function(){
		angular.element(".get_more_button").addClass('get_more_button_loading');
		var promise = services.get_posts($rootScope.array_tags,$scope.total_posts_act,8,$scope.lang);  
		promise.then(
         function(response){
			  angular.forEach(response.posts, function(post,key){
				angular.forEach(post.media, function(item_media, key2){
					if (item_media.id_tipo == 1) {
						item_media.media = $sce.trustAsHtml('<iframe width="100%" height="100%" src="'+item_media.media+'" frameborder="0" allowfullscreen></iframe>');
					}
					if (item_media.id_tipo == 4) {
						item_media.media = $sce.trustAsHtml($rootScope.pathgeral+'posts/'+post.id+'/'+item_media.media);
					}
				});
			});
         	angular.forEach(response.posts, function(item, key) {
         		$scope.posts.push(item);
         	});
         	$scope.total_posts_act = $scope.posts.length;
			  
			resize_func();
         	angular.element(".get_more_button").removeClass('get_more_button_loading');
         }
         ,function(reason){console.log('Failed: ' + reason);}
      );  
	}
    //form
    $scope.submitted = false;
    $scope.form_invalid = false;
    $scope.form_sucess = false;
    $scope.form_error = false;
    $scope.form = {};
    $scope.submit_form = function(form){
        $scope.submitted = true;
        if (form.$invalid) {
            $scope.form_sucess = false;
            $scope.form_error = false;
            $scope.form_invalid = true;
            return;
        }
        else{
            $scope.form_invalid = false;
            var promise = services.submit_evoform($scope.form,$rootScope.lang);
            promise.then(
                function(response){
                    if (response == 0) {
                        $scope.form_sucess = true;
                        $scope.form_error = false;
                        $scope.form = {};
                    }
                    else{
                        $scope.form_sucess = false;
                        $scope.form_error = true;
                    }
                }
                ,function(reason){console.log('Failed: ' + reason);}
            );
        }
    }

}]);