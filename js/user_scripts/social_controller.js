'use strict';


milesApp.controller('socialCtrl', ['$scope', '$rootScope','$window','$timeout','items','services','PageTitle','$location', function socialCtrl($scope, $rootScope, $window,$timeout,items, services,PageTitle,$location){

	/*CLICKS*/
	$scope.$on('$viewContentLoaded', function(event) {
	 $window.ga('send', 'pageview', { page: $location.url() });  
	});


	/*SET TITLE*/
	PageTitle.setTitle('Five Thousand Miles');
	PageTitle.setDesc($rootScope.lang_array.descricao_page);	
  


}]);