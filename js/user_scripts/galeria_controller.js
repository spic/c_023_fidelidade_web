'use strict';


milesApp.controller('galeriaCtrl', ['$scope', '$rootScope','$window','$timeout','$sce','items','services','$routeParams','PageTitle','$location', function galeriaCtrl($scope, $rootScope, $window,$timeout,$sce,items, services,$routeParams,PageTitle,$location){

$scope.page = 1;
  
  	var promise = services.get_photos($scope.page);  
		promise.then(
		function(response){
			$scope.fotos = response.fotos;
			console.log($scope.fotos);
			console.log(response.totalfotos);
			console.log(response.totalpages);
			$scope.comprimento = $scope.fotos.length;
			$scope.totalfotos = response.totalfotos;
			$scope.totalpages = response.totalpages;
			
	
		});
	
		
		
		

		$scope.seguintepagina = function(){
			if($scope.page < $scope.totalpages){
				$scope.page++;
		}
			
			var promise = services.get_photos($scope.page);  
				promise.then(
				function(response){
						$scope.fotos = response.fotos;
					console.log($scope.fotos);
					console.log(response.totalfotos);
					$scope.comprimento = $scope.fotos.length;
					$scope.totalfotos = response.totalfotos;
					$scope.totalpages = response.totalpages;
					
				});
		}

		$scope.anteriorpagina = function(){
			if($scope.page > 1){
				$scope.page--;
		}

				var promise = services.get_photos($scope.page);  
				promise.then(
				function(response){
				$scope.fotos = response.fotos;
				console.log($scope.fotos);
				console.log(response.totalfotos);
				$scope.comprimento = $scope.fotos.length;
				$scope.totalfotos = response.totalfotos;
				$scope.totalpages = response.totalpages;
					
				});
		}
		

        $scope.pop = 0;

        $scope.abrirpopup = function(index){
			console.log(index);
			$scope.pop = 1;
			$scope.indexpop = index;
		}

		$scope.seguinte = function(index){
			if(index < $scope.comprimento - 1){
				$scope.indexpop = index + 1;
			}
			else{
				$scope.indexpop = 0;
			}	
		}

		$scope.anterior = function(index){
			if(index < 1){
				$scope.indexpop = $scope.comprimento - 1;
				console.log(index);
			}
			else{
				$scope.indexpop = index - 1;
			}
		}




}]);
