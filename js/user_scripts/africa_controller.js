'use strict';


milesApp.controller('africaCtrl', ['$scope', '$rootScope','$window','$timeout','items','services','PageTitle','$location', function africaCtrl($scope, $rootScope, $window,$timeout,items, services,PageTitle,$location){

    /*CLICKS*/
    $scope.$on('$viewContentLoaded', function(event) {
        $window.ga('send', 'pageview', { page: $location.url() });
    });

    $scope.test = "asd";
    $scope.submitted = false;
    $scope.form_invalid = false;
    $scope.form_sucess = false;
    $scope.form_error = false;


    $(document).ready(function() {
        $("#menu_items").hide();
    });
    $scope.submit_form = function(form){


        $scope.submitted = true;
        if (form.$invalid) {
            alert("Preencher campos obrigatórios");
            $scope.form_sucess = false;
            $scope.form_error = false;
            $scope.form_invalid = true;
            return;
        }
        else{
            $scope.form_invalid = false;
            var promise = services.submit_formz($scope.form,$rootScope.lang);
            promise.then(
                function(response){
                    if (response == 0) {
                        $scope.form_sucess = true;
                        $scope.form_error = false;
                        $scope.form = {};
                    }
                    else{
                        $scope.form_sucess = false;
                        $scope.form_error = true;
                    }
                }
                ,function(reason){console.log('Failed: ' + reason);}
            );
        }
    }





}]);