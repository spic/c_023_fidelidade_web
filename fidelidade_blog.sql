-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: 09-Mar-2017 às 18:58
-- Versão do servidor: 5.6.34
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fidelidade_blog`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contactos_news`
--

CREATE TABLE `contactos_news` (
  `id` int(11) NOT NULL,
  `email` varchar(200) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `contactos_news`
--

INSERT INTO `contactos_news` (`id`, `email`) VALUES
(1, 'joao@spic.pt'),
(2, 'joao@spic.pt'),
(3, 'joao@spic.pt'),
(4, 'joao@spic.pt'),
(5, 'joao@spic.pt'),
(6, 'joao@spic.pt'),
(7, 'joao@spic.pt');

-- --------------------------------------------------------

--
-- Estrutura da tabela `idiomas`
--

CREATE TABLE `idiomas` (
  `id` int(1) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `principal` tinyint(1) NOT NULL,
  `idioma` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `sigla` varchar(5) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `idiomas`
--

INSERT INTO `idiomas` (`id`, `activo`, `principal`, `idioma`, `sigla`) VALUES
(1, 1, 1, 'Português', 'pt');

-- --------------------------------------------------------

--
-- Estrutura da tabela `media_posts`
--

CREATE TABLE `media_posts` (
  `id` int(111) NOT NULL,
  `id_post` int(111) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `media` varchar(500) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE `posts` (
  `id` int(111) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `data` datetime NOT NULL,
  `url_post` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `template` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `activo`, `data`, `url_post`, `template`) VALUES
(19, 1, '2016-02-19 10:25:30', '19-teste-de-opportunities-in-africa', 1),
(20, 1, '2016-02-19 03:12:46', '20-dsadsa', 4),
(21, 1, '2016-03-31 10:27:40', '21-dsa', 1),
(22, 1, '2016-04-03 11:44:09', '22-dsadsa', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts_idiomas`
--

CREATE TABLE `posts_idiomas` (
  `id` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `titulo` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `subtitulo` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `texto1` text COLLATE latin1_general_ci NOT NULL,
  `texto2` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `area` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tags`
--

INSERT INTO `tags` (`id`, `activo`, `area`) VALUES
(1, 0, 0),
(2, 0, 0),
(3, 0, 0),
(4, 0, 0),
(5, 0, 0),
(6, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(10, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tags_idiomas`
--

CREATE TABLE `tags_idiomas` (
  `id` int(11) NOT NULL,
  `id_idioma` int(1) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `tag` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tags_post`
--

CREATE TABLE `tags_post` (
  `id` int(111) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_post` int(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_media`
--

CREATE TABLE `tipo_media` (
  `id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `tipo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_cms`
--

CREATE TABLE `users_cms` (
  `id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `nivel` tinyint(4) NOT NULL,
  `nome` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `user` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `users_cms`
--

INSERT INTO `users_cms` (`id`, `activo`, `nivel`, `nome`, `user`, `password`, `email`, `last_login`) VALUES
(1, 1, 0, 'Administrador', 'admin', '08421c3209ec6eecb0c234f67e7bc6b0', 'joao@spic.pt', '2017-03-09 17:51:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactos_news`
--
ALTER TABLE `contactos_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_posts`
--
ALTER TABLE `media_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_post` (`id_post`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts_idiomas`
--
ALTER TABLE `posts_idiomas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`id_idioma`) USING BTREE,
  ADD KEY `id_post` (`id_post`) USING BTREE;

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_idiomas`
--
ALTER TABLE `tags_idiomas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_idioma` (`id_idioma`),
  ADD KEY `id_tags` (`id_tag`);

--
-- Indexes for table `tags_post`
--
ALTER TABLE `tags_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`id_tag`,`id_post`),
  ADD KEY `id_tag` (`id_tag`),
  ADD KEY `id_post` (`id_post`);

--
-- Indexes for table `tipo_media`
--
ALTER TABLE `tipo_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_cms`
--
ALTER TABLE `users_cms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contactos_news`
--
ALTER TABLE `contactos_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `idiomas`
--
ALTER TABLE `idiomas`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `media_posts`
--
ALTER TABLE `media_posts`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `posts_idiomas`
--
ALTER TABLE `posts_idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tags_idiomas`
--
ALTER TABLE `tags_idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags_post`
--
ALTER TABLE `tags_post`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipo_media`
--
ALTER TABLE `tipo_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_cms`
--
ALTER TABLE `users_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `media_posts`
--
ALTER TABLE `media_posts`
  ADD CONSTRAINT `media_posts_ibfk_2` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `posts_idiomas`
--
ALTER TABLE `posts_idiomas`
  ADD CONSTRAINT `posts_idiomas_ibfk_1` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_idiomas_ibfk_2` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tags_idiomas`
--
ALTER TABLE `tags_idiomas`
  ADD CONSTRAINT `tags_idiomas_ibfk_1` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tags_idiomas_ibfk_2` FOREIGN KEY (`id_tag`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tags_post`
--
ALTER TABLE `tags_post`
  ADD CONSTRAINT `tags_post_ibfk_1` FOREIGN KEY (`id_tag`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tags_post_ibfk_2` FOREIGN KEY (`id_post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
