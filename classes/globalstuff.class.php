<?php
spl_autoload_register(function ($class) {
	include $class . '.class.php';
});
 
class globalstuff{

	public $flag_error = "error";
	public $flag_success = "ok";

	private $desenvolvimento_flag;
	public $pathgeral;

	function __construct(){
		include "config/global_config.php";

		$this->desenvolvimento_flag = $desenvolvimento_flag;
		$this->pathgeral = $pathgeral;
		$this->pathgeral_base = $pathgeral_base;

		if ($array_areas) {
			$this->array_areas = $array_areas;			
		}



		/*definir a zona horaria*/
 	   date_default_timezone_set('GMT');

	}

	function get_areas_conta($id_conta){
		$database = new database();
		$mysql_query = "SELECT id,ordem,nome,tabela,area_parent_id, tabela_multilang FROM ".$database->array_tables[3]." WHERE id_conta=? AND activo=1";	
   	$result = $database->query_simple_prepare($mysql_query,array($id_conta),'i');

   	$array_return = array();
   	foreach ($result as $key => $value) {
   		$array_return_item = array();
   		$array_return_item['nome'] = $value['nome'];
   		$array_return_item['url'] =  $this->generateFriendlyName($value['nome'],50);
   		$array_return_item['id_area'] = $value['id'];

   		$array_return_item['dependecias_listar'] = array();
   		$array_return_item['dependecias_listar'][] = $value['tabela'];

   		$array_return_item['dependecias_inserir'] = array();
   		$array_return_item['dependecias_inserir'][] = $value['tabela'];

   		$array_return_item['dependecias_editar'] = array();
   		$array_return_item['dependecias_editar'][] = $value['tabela'];
   		

   		$mysql_query = "SELECT id,tabela_varios,tabela_externa FROM ".$database->array_tables[4]." WHERE id_area=? AND activo=1";	
   		$result_campos = $database->query_simple_prepare($mysql_query,array($value['id']),'i');

   		foreach ($result_campos as $key_campo => $value_campo) {
   			if ($value_campo['tabela_varios'] != '') {
   				$array_return_item['dependecias_listar'][] = $value_campo['tabela_varios'];
   				$array_return_item['dependecias_inserir'][] = $value_campo['tabela_varios'];
   				$array_return_item['dependecias_editar'][] = $value_campo['tabela_varios'];
   			}

   			if ($value_campo['tabela_externa'] != '') {
   				$array_return_item['dependecias_listar'][] = $value_campo['tabela_externa'];
   				$array_return_item['dependecias_inserir'][] = $value_campo['tabela_externa'];
   				$array_return_item['dependecias_editar'][] = $value_campo['tabela_externa'];
   			}
   		}

   		$array_return_item['area_parent'] =  $value['area_parent_id'];

   		
   		$array_return[] = $array_return_item;
   			
		}

   	return $array_return;
	}

	function return_id_area($area){
		if ($_SESSION['id_conta'] != '') {
			$this->array_areas = $this->get_areas_conta($_SESSION['id_conta']);	
		}
		$id_area = '';
		foreach ($this->array_areas as $key => $value) {
			if ($value['url'] == $area) {
				$id_area = $value['id_area'];
			}
		}
		return $id_area;
	}

	function return_dependecies_editar($url){
		if ($_SESSION['id_conta'] != '') {
			$this->array_areas = $this->get_areas_conta($_SESSION['id_conta']);	
		}
		$array_dependecies = array();
		foreach ($this->array_areas as $key => $value) {
			if ($value['url'] == $url) {
				$array_dependecies = $value['dependecias_editar'];
			}
		}
		return $array_dependecies;
	}
	function return_dependecies_inserir($url){
		if ($_SESSION['id_conta'] != '') {
			$this->array_areas = $this->get_areas_conta($_SESSION['id_conta']);	
		}
		$array_dependecies = array();
		foreach ($this->array_areas as $key => $value) {
			if ($value['url'] == $url) {
				$array_dependecies = $value['dependecias_inserir'];
			}
		}
		return $array_dependecies;
	}
	function return_dependecies_listar($url){
		if ($_SESSION['id_conta'] != '') {
			$this->array_areas = $this->get_areas_conta($_SESSION['id_conta']);	
		}

		$array_dependecies = array();
		foreach ($this->array_areas as $key => $value) {
			if ($value['url'] == $url) {
				$array_dependecies = $value['dependecias_listar'];
			}
		}
		return $array_dependecies;
	}

	function return_subareas($url){
		if ($_SESSION['id_conta'] != '') {
			$this->array_areas = $this->get_areas_conta($_SESSION['id_conta']);	
		}
		$array_subareas = array();
		foreach ($this->array_areas as $key => $value) {
			if ($value['area_parent'] == $url) {
				$array_subareas[] = $value;
			}
		}
		return $array_subareas;
	}

	function return_parent($url){
		if ($_SESSION['id_conta'] != '') {
			$this->array_areas = $this->get_areas_conta($_SESSION['id_conta']);	
		}
		$parent = '';
		foreach ($this->array_areas as $key => $value) {
			if ($value['url'] == $url) {
				$parent = $value['area_parent'];
			}
		}
		return $parent;
	}
	

	function error_report($error_string){
		if ($this->desenvolvimento_flag == 1) {
			echo $error_string;	
		}
		else{
			$mail_class = new mail();
			$mail_class->send_email($mail_class->email_developer,$mail_class->email_smtp,'admin',$mail_class->email_smtp,$error_string,"Erro ".$mail_class->clienturl,2);
		}
	}

	function generateFriendlyName($phrase, $maxLength)
	{
		$result = preg_replace('/ã/', 'a', $phrase);
		$result = preg_replace('/Ã/', 'a', $result);
		$result = preg_replace('/á/', 'a', $result);
		$result = preg_replace('/Á/', 'a', $result);
		$result = preg_replace('/à/', 'a', $result);
		$result = preg_replace('/À/', 'a', $result);
		$result = preg_replace('/Â/', 'a', $result);
		$result = preg_replace('/â/', 'a', $result);
		
		$result = preg_replace('/Ç/', 'c', $result);
		$result = preg_replace('/ç/', 'c', $result);
		
		$result = preg_replace('/õ/', 'o', $result);
		$result = preg_replace('/Õ/', 'o', $result);
		$result = preg_replace('/ó/','o',$result);
		$result = preg_replace('/Ó/','o',$result);

		$result = preg_replace('/é/','e',$result);
		$result = preg_replace('/É/','e',$result);
		$result = preg_replace('/ê/','e',$result);
		$result = preg_replace('/Ê/','e',$result);



		$result = preg_replace('/í/','i',$result);
		$result = preg_replace('/Í/','i',$result);		
		
		$result = preg_replace('/ú/','u',$result);
		$result = preg_replace('/Ú/','u',$result);


		$result = strtolower($result);

		$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
		$result = trim(preg_replace("/[\s-]+/", " ", $result));
		$result = trim(substr($result, 0, $maxLength));
		$result = preg_replace("/\s/", "-", $result);

		return $result;
	}

	/*PESQUISA NUM ARRAY MULTIDIMENSIONAL POR KEY E VALUE*/
	function search($array, $key, $value)
	{
	    $results = array();

	    if (is_array($array)) {
	        if (isset($array[$key]) && $array[$key] == $value) {
	            $results[] = $array;
	        }

	        foreach ($array as $subarray) {
	            $results = array_merge($results, $this->search($subarray, $key, $value));
	        }
	    }

	    return $results;
	}

	/*DIVIDE TEXTO EM DUAS PARTES*/
	function divide_text($text){
		$array_text = explode(' ',$text);
		$text1 = "";
		$text2 = "";
		for ($i=0; $i < round(count($array_text)/2); $i++) { 
			$text1 .= $array_text[$i]." ";
		}
		for ($i=round(count($array_text)/2); $i < count($array_text); $i++) { 
			$text2 .= $array_text[$i]." ";
		}
		return array($text1, $text2);
	}

	public function gera_links($texto){
       $texto_array = explode("http://",$texto);
       $string_new_text = "";
       $i=0;
       foreach ($texto_array as $key => $value) {
         if ($i == 0) {
           $string_new_text .= $value;
         }
         else{
           $array_new_text = explode(" ",$value);
           $string_new_text .= "<a href='http://".$array_new_text[0]."' target='_blank' class='texto_post orange'><span class='texto_post orange'>".$array_new_text[0]."</span></a> ";
           $j = 0;
           foreach ($array_new_text as $key2 => $value2) {
             if (!$j == 0) {
               $string_new_text .=  $value2." ";
             }
             $j++;
             
           }
         }
         // echo $value."<br />";
         $i++;
       }
       $texto_array = explode("https://",$string_new_text);
       $string_new_text = "";
       $i=0;
       foreach ($texto_array as $key => $value) {
         if ($i == 0) {
           $string_new_text .= $value;
         }
         else{
           $array_new_text = explode(" ",$value);
           $string_new_text .= "<a href='https://".$array_new_text[0]."' target='_blank' class='texto_post orange'><span class='texto_post orange'>".$array_new_text[0]."</span></a> ";
           $j = 0;
           foreach ($array_new_text as $key2 => $value2) {
             if (!$j == 0) {
               $string_new_text .=  $value2." ";
             }
             $j++;
             
           }
         }
         // echo $value."<br />";
         $i++;
       }
       return $string_new_text;
     }
}
?>